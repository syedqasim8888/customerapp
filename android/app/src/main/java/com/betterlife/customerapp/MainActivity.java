package com.betterlife.customerapp;

import com.facebook.react.ReactActivity;
 
import android.os.Bundle; // here
import com.facebook.react.ReactActivity;
// react-native-splash-screen >= 0.3.1

// react-native-splash-screen < 0.3.1
import org.devio.rn.splashscreen.SplashScreen;
import com.facebook.react.modules.i18nmanager.I18nUtil;
public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
      @Override
    protected void onCreate(Bundle savedInstanceState) {
          //   I18nUtil sharedI18nUtilInstance = I18nUtil.getInstance();
    // sharedI18nUtilInstance.allowRTL(context, true);
      I18nUtil sharedI18nUtilInstance = I18nUtil.getInstance();
        sharedI18nUtilInstance.allowRTL(getApplicationContext(), true);
        SplashScreen.show(this);  // here
     //   FacebookSdk.sdkInitialize(getApplicationContext());
       // AppEventsLogger.activateApp(this);
        super.onCreate(savedInstanceState);
    }
    @Override
    protected String getMainComponentName() {
        return "BetterLife";
    }
}
