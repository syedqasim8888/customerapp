import { StyleSheet } from 'react-native';
import COLORS from './Color';

export default StyleSheet.create({
    roundedCornerSquareOrange: {
        flex: 1,
        borderWidth: 0.5,
        borderColor: COLORS.Orange,
        borderRadius: 5,

    },
    textSmallBlack: {
        fontSize: 12,
        color: COLORS.black
    }, textMediumBlack: {
        fontSize: 14,
        color: COLORS.black
    }, textLargeBlack: {
        fontSize: 16,
        color: COLORS.black
    },
    textLightGrey: {
        fontSize: 12,
        color: COLORS.lightgrey
    },
    boldTextSmall: {
        fontSize: 12,
        color: COLORS.black,
        fontWeight: 'bold'
    }, boldTextMedium: {
        fontSize: 14,
        color: COLORS.black,
        fontWeight: 'bold'
    },
    bottomButtonContainer: {
        flex: 1,
        position: 'absolute',
        bottom: 0,
        width: '100%',
        height: 50
    },
    boldTextMediumToLarge: {
        fontSize: 15,
        color: COLORS.black,
        fontWeight: 'bold'
    }, btnSquareBottom: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        backgroundColor: COLORS.Orange,
        height: '100%'
    },
    boldTextLarge: {
        fontSize: 16,
        color: COLORS.black,
        fontWeight: 'bold'
    }, lineStyle: {
        borderWidth: 0.35,
        borderColor: COLORS.Orange,

    },

});