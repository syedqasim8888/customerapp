const COLORS = {
    white: '#fff',
    black: '#000',
    Orange: '#F55130',
    grey:'#EDEDED',
    darkgrey:'#565656',
    placehldrgrey:'#C7C7CD',
    red :'#FF0000',
    bgColor: '#f05424',
    lightgrey:'#a9a9a9',
    lightblue:'#8AE1F8',
    lightblack:'#6E6E6E'
    // your colors
  }
  
  export default COLORS;