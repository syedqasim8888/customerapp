import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native';
 
import COLORS from '../stylehelper/Color';

import   * as RNLocalize  from 'react-native-localize';
import SplashScreen from 'react-native-splash-screen';

import    {setI18nConfig} from './../utils/i18n';
import      {translate } from './../utils/i18n';
import renderIf from '../components/common/renderIf';
 
import { setBaseURL, cancelRequest, setCustomHeaders } from "../apis";

export default class Congratulation extends Component {


    constructor(props) {
        super(props);
        setI18nConfig();

    }

    componentDidMount() {
        RNLocalize.addEventListener('change', this.handleLocalizationChange);
      }
    
      componentWillMount() {
        SplashScreen.hide();
      }
    
      componentWillUnmount() {
        cancelRequest();
        RNLocalize.removeEventListener('change', this.handleLocalizationChange);
      }


    render() {
        const notSuccsess = this.props.navigation.state.params.success == 'failure';
        console.log(notSuccsess)
        return (
            <View style={{ backgroundColor: COLORS.Orange, flex: 1, flexDirection: 'column', justifyContent: 'center', paddingLeft: 50, paddingRight: 50 }}>

                <Image
                    style={{ resizeMode: 'contain', width: 100, height: 100, alignSelf: "center" }}
                    source={ notSuccsess ?  require('../assets/bluecross.png')  :  require('../assets/thumbsup.png')}
                />

 
                <Text style={{ color: COLORS.white, textAlign: 'center', fontSize: 20, marginTop: 20 }}
                    numberOfLines={2}>{ notSuccsess  ? this.props.navigation.state.params.message :translate('Congratulations_order_accepted')}</Text>



                {/* <Text style={{ color: COLORS.white, textAlign: 'center', fontSize: 18, marginTop: 20 }}
                    ellipsizeMode="middle"
                    numberOfLines={3}>{translate('Congratulations_description')}</Text> */}


                <TouchableOpacity onPress={() => this.props.navigation.navigate('Orders')}  >
                    <Text style={styles.bottomUnderlineText}
                        ellipsizeMode="middle"
                        numberOfLines={1}
                    >{translate('Congratulations_view_your_order')}
                    </Text>
                </TouchableOpacity>


            </View>
        );
    }
}


const styles = StyleSheet.create({

    bottomUnderlineText: {
        textDecorationLine: 'underline',
        color: COLORS.white,
        textAlign: 'center',
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 20
    },
});


