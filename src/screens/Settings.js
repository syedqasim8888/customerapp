import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert,
  TextInput,
  Dimensions,
  I18nManager,
  Image,
  TouchableOpacity,

} from 'react-native';
import { bindActionCreators } from 'redux';

import SplashScreen from 'react-native-splash-screen';
import COLORS from '../stylehelper/Color';
 
import { setBaseURL, cancelRequest, setCustomHeaders } from "../apis";
import AsyncStorage from '@react-native-community/async-storage';

import { setI18nConfig } from './../utils/i18n';
import { translate } from './../utils/i18n';
import { translationGetters } from './../utils/i18n';
import * as RNLocalize from 'react-native-localize';
import RNRestart from 'react-native-restart';
import I18t from 'i18n-js';

export const { width } = Dimensions.get('window');

class Settings extends Component {

  constructor(props) {
    super(props)
    this.state = {
      manualcode: '',
      email: '',
      password: ''
    }


  }


  componentDidMount() {
    RNLocalize.addEventListener('change', this.handleLocalizationChange);
  }

  componentWillMount() {
    SplashScreen.hide();
  }

  componentWillUnmount() {
    cancelRequest();
    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
  }

  componentWillReceiveProps(nextProps) {

    if (this.props.savetoken !== nextProps.savetoken) {
      AsyncStorage.setItem('token', this.props.savetoken);
      this.props.navigation.navigate("MainStack");
    }
  }


  async changeAppLang(lang) {

    if (lang == 'ar') {
    await  AsyncStorage.setItem("language",lang );
    } else {
      await    AsyncStorage.setItem("language",lang );
    }
    RNRestart.Restart()
  }


  render() {

    return (

      <View style={{ flex: 1, flexDirection: 'column', backgroundColor: COLORS.Orange  ,alignItems:'center'}}>


     <TouchableOpacity
            style={[styles.buttonContainer, styles.loginButton]}
            onPress={() => this.changeAppLang('ar')}>
            <Text style={[styles.loginText]}>Arabic</Text>
          </TouchableOpacity>
 
 

             <TouchableOpacity
            style={[styles.buttonContainer, styles.loginButton]}
            onPress={() => this.changeAppLang('en')}>
            <Text style={[styles.loginText]}>English</Text>
          </TouchableOpacity>
      </View>


    )
  }
}

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#FFF'
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: COLORS.Orange,
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: 'rgba(255,255,255,0.2)',
    borderRadius: 30,
    marginLeft: 12,
    marginRight: 12,

    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  inputs: {

    paddingLeft: 30,
    borderBottomColor: '#FFFFFF',
    flex: 0,
    paddingTop: 15,
    paddingBottom: 15,
    color: 'white',
    fontSize: 20
  },
  inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 15,
    justifyContent: 'center'
  },
  buttonContainer: {

    flexDirection: 'row',
    marginBottom: 10,
    marginLeft: 15,
    marginRight: 15,
    height: 50,
    borderRadius: 30,
    borderWidth: 0.5,
    borderColor: '#fff',
    alignSelf: 'center',
    marginTop: 2
  },
  loginButton: {
    backgroundColor: "#F55130",
  },
  loginText: {
    flex: 1,
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
    alignSelf: 'center'

  },

  signinText: {
    fontSize: 22,
    color: "white",
    textAlign: 'left',
    marginLeft: 30,
    marginRight: 30
    // <-- the magic
  }
  ,
  TextContainer: {
    width: '100%',
    height: 40
  },
  buttonRoundSmall: {
    padding: 12,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 30,
    width: width / 2 - 30,
    backgroundColor: COLORS.white
  },
});



export default Settings;