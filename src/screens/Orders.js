
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    Alert,
    ScrollView,
    FlatList,
    Button,
    I18nManager,
}
    from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import Header from '../components/common/header';
import { setBaseURL, cancelRequest, setCustomHeaders } from "../apis";

import * as RNLocalize from 'react-native-localize';
import { setI18nConfig } from './../utils/i18n';
import { translate } from './../utils/i18n';

import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from "react-redux";
import { fetchAllOrders } from "../store/actions/OrderHistoryAction";
import { bindActionCreators } from 'redux';

class Orders extends Component {

    componentWillMount() {
        SplashScreen.hide();
    }
    constructor(props) {
        super(props);
        setI18nConfig();
        setBaseURL("http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1")
        this.getCustomerID('userobject');

    }

    getCustomerID = async (key) => {
        try {

            const object = await AsyncStorage.getItem(key);

            this.setState({
                name: JSON.parse(object).name,
                _id: JSON.parse(object)._id,
                email: JSON.parse(object).email
            });
            console.log('Name: ' + this.state.name + ' CustomerID: ' + this.state._id + " Email: " + this.state.email)

            const { fetchAllOrders } = this.props;
            fetchAllOrders(this.state._id);

            return object;
        } catch (error) {
            console.log(error);
        }
        return
    }
    componentDidMount() {
        RNLocalize.addEventListener('change', this.handleLocalizationChange);
    }

    componentWillUnmount() {
        cancelRequest();
        RNLocalize.removeEventListener('change', this.handleLocalizationChange);
    }

    clickEventListener() {
        Alert.alert("Success", "Product has beed added to cart")
    }

    render() {

        return (

            <View style={styles.flcontainer}>

                <Header marginHorizontal={20}
                    headerText={translate('order_title')}>
                </Header>


                <FlatList
                    data={this.props.data}
                    renderItem={
                        ({ item }) =>
                            <View>
                                <View style={styles.listItemStyle}>
                                    <Image
                                        style={styles.listItemImage}
                                        resizeMode='contain'
                                        source={{ uri: item.productImg }}
                                    />
                                    <View style={styles.itemMiddleContainer} >
                                        <View   >
                                            <Text style={[styles.text, { fontWeight: 'bold' }]}>{item.productName}</Text>

                                        </View>
                                        <Text style={[styles.item, { marginBottom: 10 }]}>{item.qty}</Text>
                                        <Text style={[styles.text]}>Order Date {item.orderDate}</Text>
                                        <Text style={[styles.text]}>{item.addressSmall}</Text>


                                    </View>

                                    <View style={styles.itemRightContainer} >
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderDetails', { item: item })}>
                                            <View style={styles.itemButton} >
                                                <Text style={[{ fontSize: 12, color: '#F55130', fontWeight: 'bold', width: '100%', textAlign: 'center', paddingTop: 5, paddingBottom: 5 }]}>{translate('order_buy_again')}</Text>
                                            </View>
                                        </TouchableOpacity>
                                        {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('DriverDetail')} >
                                            <View style={styles.itemButton} >
                                                <Text numberOfLines={1} ellipsizeMode='middle' style={[{ fontSize: 12, flex: 1, fontWeight: 'bold', color: '#F55130', textAlign: 'center', paddingTop: 5, paddingBottom: 5 }]}>{translate('order_track_order')}</Text>
                                            </View>
                                        </TouchableOpacity> */}
                                    </View>




                                </View>


                                <View style={[styles.lineStyle, { color: '#F55130' }]}>

                                </View>
                            </View>

                    }
                />
            </View>
        );



    }
}

const styles = StyleSheet.create({

    flcontainer: {
        flex: 1,
        backgroundColor: '#fff',
    },
    item: {
        marginTop: 10
    }, listItemStyle: {
        flexDirection: 'row',
        flex: 1,
        marginTop: 10,
        marginBottom: 10,
        marginEnd: 20,
        marginStart: 10
    }, listItemImage: {


        width: 80,
        height: 100,
        alignSelf: "center",

    }, itemMiddleContainer: {
        flexDirection: 'column',
        flex: 1,
        height: '100%',
        justifyContent: 'space-around',
        paddingTop: 5,
        paddingBottom: 5
    }, itemRightContainer: {
        flexDirection: 'column',
        flex: 1,
        height: '100%',
        justifyContent: 'space-evenly',
        marginLeft: 10,
    }, itemButton: {
        marginTop: 10,
        flexDirection: 'column',

        flex: 1,
        alignSelf: 'center',
        borderRadius: 30,

        borderColor: '#f55130',
        borderWidth: 0.5,


        alignContent: 'center',
        width: '100%',
        paddingHorizontal: 10
    },
    container: {
        flex: 1,
        marginTop: 20,
    },
    productImg: {
        width: 200,
        height: 200,
    },
    name: {
        fontSize: 25,
        color: "black",
        textAlign: 'right',
        alignSelf: 'flex-start',

    },
    lineStyle: {
        borderWidth: 0.5,
        borderColor: '#f55130',
        marginTop: 5,
        marginHorizontal: 20
    },
    viewContainer: {
        flexDirection: 'row',
        borderRadius: 5,
        marginTop: 10,
        padding: 10
    },
    viewContainer1: {
        flexDirection: 'row',
        borderRadius: 5,
        padding: 10
    },
    orderHeading: {

        fontSize: 13,
        color: "black",
        fontWeight: 'bold',
        alignSelf: 'flex-start'
    },
    text: {
        fontSize: 12,
        color: "black",
    },
    greytext: {
        marginTop: -3,
        fontSize: 13,
        color: "grey",
        alignSelf: 'flex-start'
    },
    boldtext: {
        marginTop: -3,
        fontSize: 13,
        color: "black",
        alignSelf: 'center',
        justifyContent: 'center',
        marginLeft: 10,
        fontWeight: 'bold'

    },
    btnNew: {
        paddingVertical: 7,
        width: 130,
        borderRadius: 20,
        marginHorizontal: 3,
        borderColor: '#F55130',
        borderWidth: 0.5,
        alignSelf: 'center',
        position: "absolute",
        right: 10
    },
    NewRequest: {

    },
    textNew: {
        textAlign: 'center',
        fontSize: 13,
        color: '#f55130',
    },
    star: {
        width: 40,
        height: 40,
    },
    btnColor: {
        height: 30,
        width: 30,
        borderRadius: 30,
        marginHorizontal: 3
    },
    btnSize: {
        height: 40,
        width: 40,
        borderRadius: 40,
        borderColor: '#778899',
        borderWidth: 1,
        marginHorizontal: 3,
        backgroundColor: 'white',

        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    starContainer: {
        justifyContent: 'center',
        marginHorizontal: 30,
        flexDirection: 'row',
        marginTop: 20
    },
    contentColors: {
        justifyContent: 'center',
        marginHorizontal: 30,
        flexDirection: 'row',
        marginTop: 20
    },
    contentSize: {
        justifyContent: 'center',
        marginHorizontal: 20,
        flexDirection: 'row',
        marginTop: 20
    },
    separator: {
        height: 1,
        backgroundColor: "#f55130",
        marginTop: 20,
        marginHorizontal: 30
    },
    shareButton: {
        marginTop: 10,
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
        backgroundColor: "#00BFFF",
    },
    shareButtonText: {
        color: "#FFFFFF",
        fontSize: 20,
    },
    addToCarContainer: {
        marginHorizontal: 30
    },

});


const mapStateToProps = ({ ordershistory }) => {
    console.log(ordershistory)
    // RNTestLibModule.show("Hello World");
    return { data: ordershistory.data, result: ordershistory.result, loading: ordershistory.loading };
}


function mapDispatchToProps(dispatch) {
    return {
        ...bindActionCreators({ fetchAllOrders }, dispatch)
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Orders);