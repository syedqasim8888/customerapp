import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Image,
  Icon
} from 'react-native';
import COLORS from '../stylehelper/Color';


export default class Notification extends Component {

  constructor(props) {
    super(props);
  }


  render() {
    return (
     
      <View style={[{flex: 1}]} >
        <ScrollView  
        contentContainerStyle={ { flexGrow: 1, justifyContent: 'space-between', flexDirection: 'column' ,paddingBottom: 80,paddingTop:30 } } 
        style={{backgroundColor: 'white' , height:'100%', width:'100%' }}
           >
        <View style={{justifyContent:'flex-start'}}  >
          <View style={{alignItems:'center', marginHorizontal:20}}>
             <Text style={styles.name}>Notifications</Text>
             </View>
             <View style={styles.lineStyle}></View>
            <View style={{marginHorizontal:20}}>
            {this.renderFlatList()} 
            </View>
    
        </View>

        </ScrollView>

 </View>
    );
  }

  renderFlatList() {
    return (
      

      <FlatList
      data={[
          { key: 'A' },
          { key: 'B' },
          { key: 'C' },
          { key: 'D' }
        
      ]}
      renderItem={
          ({ item }) =>

          <View style = {[{flexDirection:'row' , flex:1 ,marginBottom:5}]}>
               <View style={{ flex: 1, flexDirection: 'row', marginTop: 20, borderRadius: 10, borderColor: 'black', padding: 10, borderWidth: 0.5, width: '100%' }}>
                      <Image
                        resizeMode='contain'
                        style={{ alignSelf: "center", height: 25, width: 25 }}
                        source={require('../assets/ic_notification.png')}
                      />
                      <View style={{flexDirection:'column',paddingEnd:20}}>
                      <Text numberOfLines={1} ellipsizeMode='tail' style={{ marginLeft: 10, fontWeight: 'bold', textAlignVertical: 'center',color:COLORS.Orange}} >Simple dummy text of the printing.</Text>
                      <Text numberOfLines={2} ellipsizeMode='tail' style={{ marginLeft: 10, textAlignVertical: 'center' }} >{item.key}</Text>
                      </View>
                    </View>
           </View>
      }
  />
    );
  }

}

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'white',
    marginTop:30,
  },
  productImg:{
    width:200,
    height:200,
  },
  name:{
    fontSize:20,
    color:"black",
    textAlign: 'right',
    alignSelf: 'flex-start'
 
  },
  lineStyle:{
    borderWidth: 0.35,
    borderColor:'#f55130',
    marginTop:5,
    marginHorizontal:20
},
viewContainer:{
    flexDirection: 'row',
 
    borderWidth :0.5,
    borderColor :  '#f55130',
    marginHorizontal:20,
    borderRadius: 5,
    marginTop:20,
    padding: 10 
},
  orderHeading:{
    
    fontSize:13,
    color:"green",
    fontWeight:'bold',
    alignSelf:'flex-start'
  },
  textblack:{
    
    fontSize:12,
    color:"black",
    alignSelf:'flex-start'
  },
  greytext:{
 
    fontSize:13,
    color:"grey",
    alignSelf:'flex-start'
  },
  boldtext:{
 
    fontSize:13,
    color:"black",
    alignSelf:'center',
    justifyContent:'center',
    marginLeft:10,
    fontWeight:'bold'

  },
  btnNew: {
   paddingVertical:7,
    width:70,
    borderRadius:20,
    marginHorizontal:3,
    borderColor :  '#f55130',
    borderWidth :0.5,
    alignSelf:'center',
    position: "absolute",
    right:10

    
  
 
 
    
  },
  btnSquareleft: {
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
    height:'100%',
  
 
     backgroundColor:'#fffff0'
   },
   btnSquareright: {
    justifyContent: 'center',
    alignItems: 'center',
    flex:1,    
    height:'100%',
 
    backgroundColor:"#f55130"
  },
  textNew:{
    textAlign:'center',
    fontSize:12,
    color :  '#f55130',
  },
  boldgreen:{
  
    fontSize:12,
    color :  '#f55130',
    fontWeight:'bold',
   
  },
  separatorVertical:{
    width:1,
    backgroundColor:"#f55130",
    
    
  },
  bottom: {
 
    flex: 1,
     justifyContent: 'center', 
     alignItems: 'center',
    flexDirection:'row',
     
 
  },
  star:{
    width:40,
    height:40,
  },
  btnColor: {
    height:30,
    width:30,
    borderRadius:30,
    marginHorizontal:3
  },
  btnSize: {
    height:40,
    width:40,
    borderRadius:40,
    borderColor:'#778899',
    borderWidth:1,
    marginHorizontal:3,
    backgroundColor:'white',

    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  starContainer:{
    justifyContent:'center', 
    marginHorizontal:30, 
    flexDirection:'row', 
    marginTop:20
  },
  contentColors:{ 
    justifyContent:'center', 
    marginHorizontal:30, 
    flexDirection:'row', 
    marginTop:20
  },
  contentSize:{ 
    justifyContent:'center', 
    marginHorizontal:20, 
    flexDirection:'row', 
    marginTop:20
  },
  separator:{
    height:1,
    backgroundColor:"#f55130",
    marginTop:20,
    marginHorizontal:30
  },
  shareButton: {
    marginTop:10,
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius:30,
    backgroundColor: "#00BFFF",
  },
  shareButtonText:{
    color: "#FFFFFF",
    fontSize:20,
  },
  addToCarContainer:{
    marginHorizontal:30
  }
});     