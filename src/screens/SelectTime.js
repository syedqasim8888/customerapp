
import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Feather';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    TouchableHighlight,
    FlatList,
    TouchableOpacity,
    Image,
    ScrollView,
    I18nManager
}
    from 'react-native';
import COLORS, { Colors } from '../stylehelper/Color';
import SplashScreen from 'react-native-splash-screen';
import { setBaseURL, cancelRequest, setCustomHeaders } from "../apis";
import Header from '../components/common/header';
import StylesCommon from '../stylehelper/StylesCommon';
import renderIf from '../components/common/renderIf';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from "moment"; // For dae Formatting
import * as RNLocalize from 'react-native-localize';
import { setI18nConfig } from './../utils/i18n';
import { translate } from './../utils/i18n';
import translationGetters from './../utils/i18n';


export default class SelectTime extends Component {

    constructor(props) {
        super(props);
        var currentDate = new Date();
        //console.log(currentDate.getHours())
        currentDate.setDate(currentDate.getHours()>=18 ? currentDate.getDate() + 2 : currentDate.getDate() + 1)
        this.state = {
            isOnetime: true,
            selectedItemToday: 0,
            selectedItemRecurring: 0,
            date: currentDate,
            mode: 'date',
            show: false,
            obj:
                { 1: '9 AM - 1 PM', 2: '1 PM - 5 PM', 3: '1 PM - 3 PM', 4: '3 PM - 5 PM' }

        };
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', this.handleLocalizationChange);
    }

    componentWillUnmount() {
        cancelRequest();
        RNLocalize.removeEventListener('change', this.handleLocalizationChange);
    }


    setDate = (event, date) => {
        date = date || this.state.date;

        this.setState({
            show: Platform.OS === 'ios' ? true : false,
            date,
        });
    }

    show = mode => {
        this.setState({
            show: true,
            mode,
        });
    }

    datepicker = () => {
        this.show('date');
    }

    componentWillMount() {
        SplashScreen.hide();
    }

    onPressHandlerToday(id) {
        this.setState({ selectedItemToday: id });
    }



    onPressHandlerIsOneTime() {
        this.setState({ isOnetime: this.state.isOnetime ? false : true })
    }
    onPressHandlerRecurring(id) {
        this.setState({ selectedItemRecurring: id });
    }

    onSubmit() {
        const { shippingid, customerid } = this.props.navigation.state.params
        if (this.state.isOnetime) {

            // 
            if (!this.state.selectedItemToday) {//here
                alert('Please select time');
            } else if (this.state.selectedItemToday) {
                this.props.navigation.navigate('CheckOut', {
                    shippingtimeIisOneTime: true, shippingDate: this.state.date,
                    shippingtimeIisReccuring: false, shippingid: shippingid, customerid: customerid, shippingtimetimeSlot: this.state.obj[this.state.selectedItemToday] 
                })
            }
        } else { // Recurring block 
            if (!this.state.selectedItemRecurring || !this.state.selectedItemToday) {//here
                alert('Please select recurring schedule and time')
            } else {

                this.props.navigation.navigate('CheckOut', {
                    shippingtimeIisOneTime: false, shippingDate: this.state.date,
                    shippingtimeIisReccuring: true, shippingid: shippingid, customerid: customerid, 
                    shippingtimetimeSlot: this.state.obj[this.state.selectedItemToday] ,  reccuringPattern : this.state.selectedItemRecurring
                })

            }

        }

    }

    render() {
        var minDate = new Date();
        console.log("current hours: "+minDate.getHours())
        if(minDate.getHours()>=18){
            minDate.setDate(minDate.getDate() + 2)
        }
        else{
            minDate.setDate(minDate.getDate() + 1)
        }
        const { show, date, mode } = this.state;
        return (

            <View style={{ flex: 1, backgroundColor: COLORS.white }}>
                <ScrollView>
                    <View style={{ flex: 1, flexDirection: 'column', marginHorizontal: 20, marginBottom: 50 }}>

                        <Header headerText={translate('select_time_title')}></Header>

                        <View style={{ flexDirection: "row", marginTop: 20 }}>
                            <View style={{ flex: 1, flexDirection: 'row', marginTop: 5 }}>
                                <TouchableOpacity onPress={() => this.onPressHandlerIsOneTime()}>

                                    <View style={{ flex: 1, flexDirection: 'row' }}>
                                        <Image
                                            resizeMode='contain'
                                            style={{ flex: 0, height: 20 }}
                                            tintColor={this.state.isOnetime ? COLORS.red : COLORS.placehldrgrey}
                                            source={require('../assets/redtick.png')}
                                        />
                                        <Text style={[styles.TextStyleClass, { flex: 0 }]}>{translate('select_time_one_time')}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', marginTop: 5 }} >
                                <TouchableOpacity onPress={() => this.onPressHandlerIsOneTime()}>

                                    <View style={{ flex: 1, flexDirection: 'row' }}>
                                        <Image
                                            resizeMode='contain'
                                            style={{ flex: 0, height: 20 }}
                                            tintColor={this.state.isOnetime ? COLORS.placehldrgrey : COLORS.red}
                                            source={require('../assets/redtick.png')}
                                        />
                                        <Text style={[styles.TextStyleClass, { flex: 0 }]}>{translate('select_time_recurring')}</Text>
                                    </View>
                                </TouchableOpacity>

                                {renderIf(!this.state.isOnetime)(
                                    <FlatList
                                        data={[

                                            { key: translate('select_time_daily') },
                                            { key: translate('select_time_three_days') },
                                            { key: translate('select_time_two_weaks') },
                                            { key: translate('select_time_one_weak') },
                                            { key: translate('select_time_monthly') },

                                        ]}
                                        renderItem={
                                            ({ item }) =>
                                                <TouchableOpacity onPress={() => this.onPressHandlerRecurring(item.key)} >
                                                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 5 }}>
                                                        <Image
                                                            resizeMode='contain'
                                                            style={{ height: 20, }}
                                                            tintColor={this.state.isOnetime ? (COLORS.placehldrgrey) :
                                                                (this.state.selectedItemRecurring === item.key ? COLORS.red : COLORS.placehldrgrey)}
                                                            source={require('../assets/redtick.png')}
                                                        />
                                                        <Text style={[styles.TextStyleClass, { alignSelf: 'center' }]}>{item.key}</Text>
                                                    </View>
                                                </TouchableOpacity>
                                        }
                                    />
                                )}


                            </View>

                        </View >



                        <TouchableOpacity onPress={this.datepicker}>
                            <View style={{ flexDirection: 'row', marginTop: 40, borderRadius: 30, borderColor: COLORS.Orange, padding: 10, borderWidth: 0.5 }}>   
                                <Text numberOfLines={1} ellipsizeMode='tail' style={{ color: COLORS.Orange, flex: 0.85, fontWeight: 'bold', textAlignVertical: 'center' }} >{translate('select_time_start_Date') + ': ' + moment(date).format(' ddd  DD MMM')}</Text>
                                <Icon name="calendar" size={30} color={COLORS.Orange} style={{ flex: 0.15 }} />
                            </View>
                        </TouchableOpacity>


                        <View style={{ width: '100%', marginTop: 20 }}>
                            <Text style={{ fontSize: 15, textAlign: "left", width: '100%', fontWeight: "bold" }}> {translate('select_time_time')}</Text>
                        </View>

                        <View style={[styles.lineStyle, {}]}></View>


                        <View style={[styles.FlatlistViewStyle]}    >
                            <FlatList
                                horizontal
                                showsHorizontalScrollIndicator={false}
                                contentContainerStyle={{ paddingTop: 20, paddingBottom: 10 }}
                                data={[
                                    { id: 1, key: '9 AM - 1 PM' },
                                    { id: 2, key: '1 PM - 5 PM' },


                                ]}
                                renderItem={
                                    ({ item }) =>
                                        <TouchableOpacity
                                            onPress={() => this.onPressHandlerToday(item.id)}>
                                            <View style={[styles.ViewTextStyle]}
                                                backgroundColor={this.state.selectedItemToday === item.id ? COLORS.Orange : COLORS.white}>

                                                <Text
                                                    numberOfLines={1}
                                                    ellipsizeMode='tail'
                                                    style={[styles.TextStyle, { color: this.state.selectedItemToday === item.id ? COLORS.white : COLORS.Orange }]} >{item.key}
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                }
                            />
                        </View>


                    </View>

                </ScrollView>


                <View style={StylesCommon.bottomButtonContainer}>
                    <TouchableOpacity style={[StylesCommon.btnSquareBottom]} onPress={() => this.onSubmit()}>
                        <Text style={[StylesCommon.boldTextLarge, { color: COLORS.white }]}>{translate('select_time_confirm')}</Text>
                    </TouchableOpacity>
                </View>
                {show && <DateTimePicker value={date}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    minimumDate={minDate}
                    onChange={this.setDate} />
                }
            </View >

        );


    }
}

const styles = StyleSheet.create({

    FlatlistViewStyle: {
        flex: 0

    },

    ViewTextStyle: {
        borderRadius: 30,
        borderColor: COLORS.Orange,
        padding: 5,
        borderWidth: 0.5,
        height: 30,
        width: 100,
        marginLeft: 5,
        marginRight: 5,
        alignSelf: 'center'
    },

    TextStyle: {
        fontSize: 12,
        textAlignVertical: 'center',
        alignSelf: 'center',
        color: COLORS.Orange
    },

    TextInputStyleClass: {

        textAlign: 'left',
        height: 50,
        borderWidth: 0.5,
        borderColor: COLORS.black,
        borderRadius: 20,
        backgroundColor: "#FFFFFF",
        marginHorizontal: 20,
        marginTop: 20,
        textAlign: 'left',
        paddingHorizontal: 20


    },
    TextStyleClass: {
        height: 30,
        fontSize: 13,
        textAlign: 'left',
        marginLeft: 5,

    },
    lineStyle: {
        borderWidth: 0.5,
        color: COLORS.black,
        marginTop: 10,
    },
    ButtonStyleClass: {
        textAlign: 'center',
        height: 50,
        borderRadius: 20,
        backgroundColor: COLORS.Orange,
        marginHorizontal: 20,
        marginTop: 10,
        marginBottom: 5,
        textAlign: 'left',
        padding: 10

    },
    btnSquareright: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        backgroundColor: COLORS.Orange,
        height: '100%'
    },

});     
