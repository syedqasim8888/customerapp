
import React, { Component } from 'react';
import HTML from 'react-native-render-html';
import {
    Switch,
    ScrollView,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Dimensions
} from 'react-native';

import * as Animatable from 'react-native-animatable';
import Collapsible from 'react-native-collapsible';
import Accordion from 'react-native-collapsible/Accordion';
import SplashScreen from 'react-native-splash-screen';
import Header from '../components/common/header';
import COLORS from '../stylehelper/Color';

 
const ABOUT_US = "Happy Life is a distribution company created with the vision of distributing brands across the UAE. The team has managed and worked in some of the most complex distribution businesses in the UAE. We are starting with a portfolio of very strong brands and will be expanding our portfolio significantly.";

const TERM_AND_CONDITION = `<div class="modal-content">
           
            <div class="modal-body">
                <div class="modal-body-inner">
                    <p>These terms and conditions for mobile application (the “App”) and Website is made available by Happy Life General Trading LLC, a Limited Liability Company, incorporated in Dubai, United Arab Emirates under License No. 830867, located at Office No. 310, Barsha Boutique Building, Al Barsha, Dubai, UAE (hereinafter refer as Company)</p>

                    <p>HAPPY LIFE GENERAL TRADING LLC” maintains the http://happylife-uae.com/ Website ("Site")</p>

                    <p>The user of the App, confirm their acceptance of these App terms of use (“App Terms”). If you do not agree to these App Terms, you must immediately uninstall the App and discontinue its use. These App Terms should be read alongside Company Privacy Policy.</p>

                    <h3 class="secondary_title">General Terms</h3>

                    <h4>App and Related Terms</h4>

                    <p>Company may from time to time vary these App Terms. Please check these App Terms regularly to ensure you are aware of any variations made by Company. If you continue to use this App, you are deemed to have accepted such variations. If you do not agree to such variations, you should not use the App.</p>
                   
                    <h4>Use of the App</h4>

                    <p>You must be at least 18 years of age to use the App. Company hereby grants you a non-exclusive, non-transferable, revocable license to use the App for your personal, non-commercial use of the product by corporate and individual user and only for personal use as permitted by the applicable Terms and in accordance with these App Terms (“User License”). All other rights in the App are reserved by the Company.</p>

                    <p>In the event of your breach of these App Terms we will be entitled to terminate the User License immediately.</p>

                    <p>You acknowledge that your agreement with your mobile network provider (“Mobile Provider”) will apply to your use of the App. You acknowledge that you may be charged by the Mobile Provider for data services while using certain features of the App or any such third party charges as may arise and you accept responsibility for such charges. If you are not the bill payer for the Device being used to access the App, you will be assumed to have received permission from the bill payer for using the App.</p>

                    <p>You acknowledge that where you use services provided by Apple or Google (or any other third parties) in connection with your use of the App, you will be subject to Apple’s, Google’s (or the applicable third party’s) terms and conditions and privacy policy and you should ensure that you have read such terms.</p>
                    <h4>Payment Terms:</h4>

                    <p>The user can use the company services and payment can be made under the following modes:</p>
                    <ul>
                        <li>Cash on Delivery</li>
                        <li>Online payment through Debit/Credit cards in AED</li>
                        <li>Digital Wallet for prepaid payment</li>
                    </ul>
                    <h4>Payment Confirmation</h4>

                    <p>Payment confirmation will be provided to customer by email and notifications immediately, but in some cases it can take Max half hour from the delivery of the product</p>

                    <p>Cardholder must retain a copy of transaction records and http://happylife-uae.com/ policies and rules</p>

                    <h4>Intellectual Property</h4>

                    <p>The name and logo <b>BETTER LIFE</b>, and other trademarks, service marks, graphics and logos of the Company used in connection with the App are trademarks of the Company (collectively “Company Trademarks”). Other trademarks, service marks, graphics and logos used in connection with the App are the trademarks of their respective owners (collectively “Third Party Trademarks”). The Company Trademarks and Third Party Trademarks may not be copied, imitated or used, in whole or in part, without the prior written permission of the Company or the applicable trademark holder. The App and the content featured in the App are protected by copyright, trademark, patent and other intellectual property and proprietary rights which are reserved to the Company and its licensors.</p>
                    <h4>Prohibited Uses</h4>

                    <p>You agree not to use the App in any way that:</p>
                    <ul>
                        <li>is unlawful, illegal or unauthorised.</li>
                        <li>is defamatory of any other person.</li>
                        <li>is obscene or offensive.</li>
                        <li>promotes discrimination based on race, sex, religion, nationality, disability, sexual orientation or age.
                        </li>
                        <li>infringes any copyright, database right or trade mark of any other person.</li>
                        <li>is likely to harass, upset, embarrass, alarm or annoy any other person.</li>
                        <li>is likely to disrupt our service in any way or</li>
                        <li>advocates, promotes or assists any unlawful act such as (by way of example only) copyright infringement or computer misuse.
                        </li>
                    </ul>
                    <h4>Indemnification</h4>

                    <p>You agree to indemnify the Company for any breach of these App Terms. The Company reserves the right to control the defence and settlement of any third party claim for which you indemnify the Company under these App Terms and you will assist us in exercising such rights.</p>
                    
                    <h4>No Promises</h4>

                    <p>The Company provides the App on an ‘as is’ and ‘as available’ basis without any promises or representations, express or implied. In particular, the Company does not warrant or make any representation regarding the validity, accuracy, reliability or availability of the App or its content. All calorie or other drinks related information is based on information provided by producers and retailers and such data, and other content on the App, may be out of date and the Company makes no commitment to update it.</p>

                    <p>To the fullest extent permitted by applicable law, the Company hereby excludes all promises, whether express or implied, including any promises that the App is fit for purpose, of satisfactory quality, non-infringing, is free of defects, is able to operate on an uninterrupted basis, that the use of the App by you is in compliance with laws or that any information that you transmit in connection with this App will be successfully, accurately or securely transmitted.</p>
                    
                    <h4>Reliance on Information</h4>

                    <p>The App is intended to provide general information only and, as such, should not be considered as a substitute for advice covering any specific situation. You should seek appropriate advice before taking or refraining from taking any action in reliance on any information contained in the App. </p>

                    <p>THE APP IS IN NO WAY TO BE CONSIDERED OR USED AS A REPLACEMENT FOR PROFESSIONAL MEDICAL ADVICE.</p>
                    
                    <h4>Exclusion of the Company Liability</h4>

                    <p>Nothing in these App Terms shall exclude or in any way limit the Company liability for death or personal injury caused by its negligence or for fraud or any other liability to the extent the same may not be excluded or limited as a matter of law.</p>

                    <p>To the fullest extent permitted under applicable law, in no event shall the Company be liable to you with respect to use of the App and/or be liable to you for any direct, indirect, special or consequential damages including, without limitation, damages for loss of goodwill, lost profits, or loss, theft or corruption of your information, the inability to use the App, Device failure or malfunction.</p>

                    <p>The Company shall not be liable even if it has been advised of the possibility of such damages, including without limitation damages caused by error, omission, interruption, defect, failure of performance, unauthorised use, delay in operation or transmission, line failure, computer virus, worm, Trojan horse or other harm.</p>

                    <p>In the event that applicable law does not allow the exclusion of certain promises and/or the exclusion of liability for direct, indirect, consequential or other damages, in no event shall the Company liability arising under or in connection with these App Terms.</p>
                    <h4>General</h4>

                    <p>These App Terms shall be governed by the laws of United Arab Emirates and the parties submit to the exclusive jurisdiction of the courts of Dubai, UAE to resolve any dispute between them arising under or in connection with these App Terms.</p>

                    <p>If any provision (or part of a provision) of these App Terms is found by any court or administrative body of competent jurisdiction to be invalid, unenforceable or illegal, such term, condition or provision will to that extent be severed from the remaining terms, conditions and provisions which will continue to be valid to the fullest extent permitted by law.</p>

                    <p>We will not trade with or provide any services to OFAC and sanctioned countries</p>
                    <h4>Contact Us</h4>

                    <p>If you have any questions regarding our App, you can email us sales@happylife-uae.com</p>
                </div>
            </div>
        </div>
`;
const CANCELATION = "The user is permitted to cancel its order of the Goods of the company anytime before Delivery of Goods if the payment term of cash on delivery and digital wallet for prepaid payment is used. Any order made through online payment by debit/credit card cannot be cancelled.";
const REFUND = "Any payment made by the user either cash, digital wallet or through Debit/Credit Card is non-refundable. The user is not entitled to claim any refund once the payment is made and invoice has been issued by the company.";
const DELIVERY_POLICY = `<div class="modal-body-inner">
<p>The Company will sell and deliver to Customer, bottled water, Tissue papers and any other products in such quantities as customer orders from time to time. The company deliver the products within the territory of UAE and all deliveries are done through companies registered equipped and international standard vehicles. The Company assure that the delivery of products to customer will be as per the schedule of the company and as agreed by the customer between 9AM to 5 PM.
</p>
<p>Deliver will be made free of/ charges subject to minimum order value as per the company policy. The minimum order value can be amended by the company at its sole discretion, the company will inform the customer of minimum order value at the time of order placed by the customer.
</p>
</div>`;
const PRIVACY_POLICY = `<div class="modal-body-inner">
<p>Privacy is very important, at our online mobile application (mobile app) and website www.happylife-uae.com, the services are provided by Happy Life General Trading LLC. We are committed to protect and handle your privacy and information in the most transparent manner. This statement lays out how we collect, manage, store and protect your personal data. Please read carefully this statement to get a clearer understanding about our privacy policy. By providing us with any personal information, you are consenting to the use of your personal information as contemplated in this privacy notice. If you do not agree to any part of this Policy, Please stop accessing the mobile app and website and do not submit any of your personal information here.</p>

<p>This Privacy Policy details the commitment from the Company to protect your personal information. We collect personal information from you either directly or indirectly, and use it in a variety of ways. Personal information is any information that can be used to identify you or that can be intimately linked to you.</p>

<p>This Policy also covers the choices you can make about the data we collect and how you can impact upon decisions we make based on your personal data. Complete Terms and Conditions are available within mobile app and website are the authority in the case of inconsistency.
</p>

<h4>Policy applicability</h4>

<p>The Policy applies to this website www.happylife-uae.com and the mobile app. It does not apply to services or companies that are linked to this website.</p>

<h4>Types of Personal Information Collected</h4>

<p>We require information related to your personal information either of your home or business primarily to facilitate your registration, use of our website and mobile app, and to meet our regulatory obligations. We strive to be clear about the data we collect and why. We are obligated by law to protect your data and the wellbeing of our website and mobile app as an entity depends on it. We do not and will not sell your data to third parties.</p>

<p>In addition to the information you provide to us, Happy Life General Trading LLC may collect information about your business from third parties for the purposes of identification and verification.</p>

<h5>Information you provide</h5>

<p>We collect personal information you provide to us voluntarily, such as your name, phone number, email address, home address, business address, business name and tax information. This happens during mobile app signup and or when you contact our support staff for assistance with our mobile app.</p>

<h5>Information that is collected automatically</h5>

<p>We automatically collect information about you, such as Environment Data while you use our website and mobile app. This is collected using cookies or similar technologies when you interact with mobile app and website. We collect information about the type of device you use to access the website and mobile app, your operating system and software version identifiers, your IP address, your browser type and the pages you view on the mobile app and website, and lastly, what interactions you have with the content.</p>

<h5>Information obtained from other sources</h5>

<p>We receive information about you from other sources, including third parties such as credit ratings agencies, identity verification agencies, banks and service partners that are authorized to provide such information. We also seek and receive information about you from social media platforms when you interact with us on those platforms or when we are conducting fraud analysis.</p>
<h5>Information relating to children and minors</h5>

<p>Happy Life General Trading LLC does not target or market intentionally to children under eighteen. Visitors of all ages may interact with our mobile app or website, but no information is collected knowingly about those under the age of eighteen without parental consent. If a child under eighteen has registered on our mobile app using false information the account will be closed and any personal information collected will be erased from records.</p>
<h4>When Personal Information is Obtained</h4>
<h5>Researching Pin Payments</h5>

<p>While researching our mobile app and website to determine if it is right for you, you may visit the website, use our mobile app or contact the sales and support team. While doing this, you may provide personal information, such as your name, email address, and phone number. The website and mobile app will also implicitly collect information about you via cookies and logging when you visit the website and mobile app.</p>
<p><strong>Personal information collected:</strong></p>

<p>IP address and information derived from cookies and similar technologies.</p>
<p><strong>Why:</strong></p>

<p>This information is used to understand the location (geographically) of visitors, maintain state on our website and mobile app commonly visited pages so that the website and mobile app can be improved or customized to meet peoples’ needs.</p>
<h5>Converting your test account to a live account</h5>

<p>When you are satisfied you understand our mobile app and website, what it offers and how you intend to use it, you may then be ready to make the order. At this point you will need to convert your test account to a live account. In order to process this request, we need additional information. This information is used to meet our regulatory obligations, obligations to banking partners and to be able to forward money to required sector on the website and mobile app.</p>
<h5>Using the website and mobile app</h5>

<p>The main reasons we collect information during your everyday use of our website and mobile app is to maintain the security, correctness and stability of the website and mobile app. This information is also useful in providing customer support. Additionally, we use this information to better direct development effort towards improving our product and service.</p>
<p><strong>Personal information collected:</strong></p>

<p>login times and dates, IP addresses, device types, operating system and software versions, key activities performed within your account.</p>
<p><strong>Why:</strong></p>

<p>For the security, correctness and stability of the website and mobile app, for customer support and for planning development effort.</p>
<h5>Contacting the support team</h5>

<p>On occasion you may choose to contact the Happy Life support team for assistance with the website and mobile app. In order to be able to offer the best service possible we may request and collect data from you.</p>
<p><strong>Personal information collected:</strong></p>

<p>This might include contact information, such as your name, email address, phone number, social media identifiers (if you contact us via Twitter or Facebook), and the content of your communications with support staff.</p>
<p><strong>Why:</strong></p>

<p>This information is necessary to provide excellent support. Any context you provide in your communications may be important to the diagnosis of a problem you may be experiencing, and your contact details will enable Happy Life support team to follow up with you in regard to your support requests.</p>
<h4>How we uses Personal Information</h4>
<p>The data collected by us is used to provide you with the services offered. This includes using your data to improve and customize your use of the website and mobile app. The data is also used to communicate with you relevant events pertaining to your account, new service availability, changes
to the way the website and mobile app operates and security concerns.</p>
<h4>Providing Services</h4>
<p>We use the data to operate our website and mobile app to ensure secure service providing and process payments in an efficient, secure, correct and legal manner. The processing of personal information is required to perform our duties in the provision of these services.</p>
<h4>Merchant Support</h4>
<p>The support team respond to merchant inquiries and help diagnose service problems in addition to servicing other requests made by merchants.</p>
<h4>Improvements</h4>
<p>We uses data for research purposes and to develop and improve our website and mobile app in the form of new features, performance enhancements and usability improvements.</p>
<h4>Security, Correctness and Dispute Resolution</h4>
<p>Personal data that we collect is used to protect the security and safety of our website and mobile app, merchants using the mobile app and the security of merchants’ customers. This may be in the form of fraud detection, the identification of money laundering and confirming the correctness of behaviours by automated systems.</p>
<h4>Regulatory Requirements</h4>
<p>Data collected on our website and mobile app is used to ensure compliance, conform with business agreements and to meet legal and regulatory requirements.</p>
<h4>Business Operations</h4>
<p>Data is used in aggregate analysis and business intelligence to operate and report on business processes.</p>
<h5>Communication</h5>

<p>Data is used to personalize communications with merchants. By way of example, this may be in the form of notifications about services relevant to the merchant’s business model, or messages informing merchants of security problems associated with their chosen method of integration with our mobile app.</p>
<h5>Environment Data</h5>

    <p><strong>We collect:</strong></p>

    <p>Email address, location, language preference, device type, browser type, browser version, operating system, operating system version.</p>
    <p><strong>Why:</strong></p>

    <p>We collect this information to help ensure that website and mobile app is fit for purpose, and to customize your experience to better suit your technology or environment, whether that be language, screen size or the capabilities of your device.</p>

<h5>Information sharing</h5>

<p>Happy Life does not sell or rent any personal information to marketers or third parties. Your information will be used for purposes relating to applicant processing and the operation of the website and mobile app. This may involve sharing data with third party vendors, financial organizations, tax authorities, credit, risk and collections agencies.</p>
<h5>Third Party Vendors</h5>

<p>The website and mobile app may use third-party vendors to provide supporting services. System hosting, secure data storage, risk and credit analysis, fraud detection, security, compliance and email communications are supporting services provided by third-party vendors. Personal Information is only shared as required to meet need in the provision of services on your behalf. Third party vendors are bound by Data Processing Agreements entered into with Happy Life that detail the limitations of the authorized use of personal information under our control.</p>
<h5>Compelled Disclosure</h5>

<p>When Happy Life is legally required to do so. Personal information will be shared with law enforcement agencies and investigative agencies. Information may also be shared with such organizations in the interest of protecting our own rights, the rights of affiliates or merchants in the course of legal proceedings.</p>
<h4>Secure Storage of the Personal Information Collected</h4>
<h5>Security</h5>

<p>Happy Life takes the protection of personal information very seriously. Personal information within the website and mobile app is afforded the same protections as our card processing facilities.</p>

<p>The website and mobile app undergoes regular security compliance audits to maintain this status. A multitude of technical and organizational measures based on best practice and compliance frameworks are in place to ensure personal information is protected from loss, theft, misuse and unauthorized access.</p>

<p>‘All credit/debit cards’ details and personally identifiable information will NOT be stored, sold, shared, rented or leased to any third parties.</p>

<p>http://happylife-uae.com/ will not pass any debit/credit card details to third parties</p>

<p>http://happylife-uae.com/ is not responsible for the privacy policies of websites to which it links. If you provide any information to such third parties different rules regarding the collection and use of your personal information may apply. You should contact these entities directly if you have any questions about their use of the information that they collect.
</p>

<h5>Storage Timeframe</h5>

<p>Happy Life will store your personal information as long as it is necessary to provide service and to comply with applicable laws and regulations. When it is no longer necessary to retain personal information it is erased.</p>
<h5>Your Rights</h5>

<p>Happy Life appreciates your desire to maintain control of your personal information. The
    following means of doing so are available to you:</p>
<ul>
    <li>You can ask for a copy of the personal information we hold on you.</li>
    <li>You can inform us of any changes in your personal information, or you can request corrections to personal information we hold on you.
    </li>
    <li>Within the limitations of legal and regulatory requirements, you can ask us to erase or restrict personal information we hold on you. In some instances, this may mean we cannot provide service through our mobile app.
    </li>
    <li>Within the limitations of legal and regulatory requirements, you can request that we forward your personal information to a third party.
    </li>
    <li>In cases where we are using information with your consent, you remain entitled to withdraw consent.
    </li>
</ul>
<p>Requests can be made to sales@happylife-uae.com. A response will be made as quickly as possible.</p>

<p>If you believe Happy Life has been unable to meet your needs within the framework above, you may
    be able to lodge a complaint with the data protection authority in your country (if one exists)
    or a supervisory authority.</p>
<h5>Roles within the context of the General Data Protection Regulation (GDPR)</h5>

<p>Happy Life website and mobile app is a data controller for personal information collected and
    used on the website and mobile app.</p>

<p>Happy Life is a data processor for personal information collected and controlled by merchants who
    make use of the services provided by the website and mobile app.</p>
<h5>Notification of changes to this policy</h5>

<p>Happy Life reserves the right to update or alter this policy and you are encouraged to check it
    periodically for updates. When the policy has been revised the ‘Last updated’ date at the bottom
    of this policy will reflect as such.</p>
<h5>Contact us</h5>

<p>If you have any questions regarding this policy or the use of personal information collected by
    Happy Life General Trading LLC, please send an email to sales@happylife-uae.com.</p>

</div>`;


const CONTENT = [
    {
title:'About Us',
content:ABOUT_US
    },
    {
        title: 'Term and Conditions',
        content: TERM_AND_CONDITION,
    },
    {
        title: 'Cancellation',
        content: CANCELATION,
    },
    {
        title: 'Refund',
        content: REFUND,
    },
    {
        title: 'Privacy Policy',
        content: PRIVACY_POLICY,
    },
    {
        title: 'Delivery Policy',
        content: DELIVERY_POLICY,
    },
  
];



export default class AboutCompany extends Component {
    state = {
        activeSections: [],
        collapsed: true,
        multipleSelect: false,
    };

    toggleExpanded = () => {
        this.setState({ collapsed: !this.state.collapsed });
    };

    setSections = sections => {
        this.setState({
            activeSections: sections.includes(undefined) ? [] : sections,
        });
    };

    componentWillMount() {
        SplashScreen.hide();
    }


    renderHeader = (section, _, isActive) => {
        return (
            <Animatable.View
                duration={400}
                style={[styles.header, isActive ? styles.active : styles.inactive]}
                transition="backgroundColor"
            >
                <View  style = {[isActive? {flex:1 , backgroundColor:COLORS.Orange}: {flex:1 , backgroundColor:'#F5FCFF'} ]}>
                <Text style={ [styles.headerText,  isActive ? styles.activeHeaderText : styles.inactiveHeaderText ] }>{section.title}</Text>
                </View>
            </Animatable.View>
        );
    };

    renderContent(section, _, isActive) {
        return (
            <Animatable.View
                duration={400}
                style={[styles.content, isActive ? styles.active : styles.inactive]}
                transition="backgroundColor"
            >

                <HTML html={section.content} imagesMaxWidth={Dimensions.get('window').width} />

            </Animatable.View>
        );
    }

    render() {
        const { multipleSelect, activeSections } = this.state;

        return (
            <View style={styles.container}>
                <ScrollView  >
                    <Header  marginHorizontal ={20}   headerText={'About Company'} ></Header>

                    <Collapsible collapsed={this.state.collapsed} align="center">
                        <View style={styles.content}>
                            <Text>
                                Bacon ipsum dolor amet chuck turducken landjaeger tongue spare
                                ribs
              </Text>
                        </View>
                    </Collapsible>
                    <Accordion
                        activeSections={activeSections}
                        sections={CONTENT}
                        touchableComponent={TouchableOpacity}
                        expandMultiple={multipleSelect}
                        renderHeader={this.renderHeader}
                        renderContent={this.renderContent}
                        duration={400}
                        onChange={this.setSections}
                        containerStyle ={{marginTop:10}}
                    />
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
     
    },
    title: {
        textAlign: 'center',
        fontSize: 22,
        fontWeight: '300',
        marginBottom: 20,
    },
    header: {
        backgroundColor: '#F5FCFF',
        marginHorizontal:10,
    
    },
    headerText: {
 
        marginLeft:10,
        fontSize: 16,
        marginTop:10,
        marginBottom:10,
        fontWeight: '500',
    },
    content: {
        padding: 20,
        backgroundColor: '#fff',
    },
    active: {
        backgroundColor:COLORS.white,
    },
    inactive: {
        backgroundColor: 'rgba(245,252,255,1)',
    },activeHeaderText:{

        color:'white'
        
    },inactiveHeaderText:{
        color:'black'
    },
    selectors: {
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    selector: {
        backgroundColor: '#F5FCFF',
        padding: 10,
    },
    activeSelector: {
        fontWeight: 'bold',
    },
    selectTitle: {
        fontSize: 14,
        fontWeight: '500',
        padding: 10,
    },
    multipleToggle: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginVertical: 30,
        alignItems: 'center',
    },
    multipleToggle__title: {
        fontSize: 16,
        marginRight: 8,
    },
});