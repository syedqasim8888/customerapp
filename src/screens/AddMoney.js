import React, { Component } from 'react';
import SplashScreen from 'react-native-splash-screen';

import COLORS from '../stylehelper/Color';
import { setBaseURL, cancelRequest, setCustomHeaders } from "../apis";

import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    Alert,
    ScrollView,
    FlatList,
 
    TextInput,
    I18nManager,
    Button
} from 'react-native';
import { translate } from '../utils/i18n';

import * as RNLocalize from 'react-native-localize';
import { setI18nConfig } from './../utils/i18n';

const paymentBundle = {
    AED100: '100',
    AED200: '200',
    AED500: '500',

}
export default class Add_Money extends Component {


    constructor(props) {
        super(props);
        this.state = {
            amount: 0.0,
            selectedOption: 0

        };
        setI18nConfig();
    }




    componentWillMount() {
        SplashScreen.hide();
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', this.handleLocalizationChange);
    }

    componentWillUnmount() {
        cancelRequest();
        RNLocalize.removeEventListener('change', this.handleLocalizationChange);
    }


    clickEventListener() {
        Alert.alert("Success", "Product has been added to cart")
    }


    onConfirmHandler() {
        if (this.state.selectedOption > 0) {
            this.props.navigation.navigate('PaymentScreen', { 'walletamount': this.state.selectedOption })
            return
        }
        if (this.state.amount < 100) {
            alert('Minimum  value for recharge your wallet is 100')
            return
        }
        this.props.navigation.navigate('PaymentScreen', { 'walletamount': this.state.amount })

    }
    onOptionClicked(clickedItem) {
        if(this.state.selectedOption == clickedItem){
            this.setState({ selectedOption: 0 })
        }
        else{
            this.setState({ selectedOption: clickedItem })
        }
    }


    render() {
        const balance = this.props.amount >0 ?this.props.amount.toFixed(2):''
        return (

            <View style={{ flex: 1 }}>

                <ScrollView style={{ marginBottom: 60 }}>

                    <View style={styles.inputContainer}  >

                        <View style={{ alignItems: 'center', marginTop: 30 }}>
                            <Text style={styles.name}>{translate('add_money_title')}</Text>
                        </View>

                        <View style={[styles.lineStyle, { marginTop: 10 }]}>
                        </View>

                        <View style={{ alignItems: 'center', marginVertical: 20 }}>
                            <Text style={[styles.greytext]} >{translate('add_money_better_life_wallet')}</Text>
                            <Text style={{ flexDirection: 'row' }}>
                                <Text style={[styles.greytext, { fontSize: 15 }]}>{translate('add_money_balance')}:</Text>
                                <Text>{I18nManager.isRTL ? this.props.navigation.state.params.balance + ' ' + translate('common_aed') : translate('common_aed') + ' ' + this.props.navigation.state.params.balance}</Text>
                            </Text>
                            <View>

                            </View>
                        </View>

                        <View style={{ flex: 1 }}>
                            {this.renderFlatList()}
                        </View>


                        <Text style={styles.textNorml}>{translate('add_money_pay_over_500')}</Text>


                        <View style={styles.orContainer}>
                            <View style={[styles.lineStyle, { marginRight: 20, height: 1, width: '40%' }]}>
                            </View>
                            <Text style={{ textAlign: 'center', fontSize: 12 }}>{translate('add_money_or')}</Text>
                            <View style={[styles.lineStyle, { marginLeft: 20, height: 1, width: '40%' }]}>
                            </View>
                        </View>

                        <View style={[styles.TextInputContainer, { width: '100%', marginTop: 20, borderRadius: 30 }]}>
                            <TextInput style={{ textAlign: I18nManager.isRTL ? 'right' : 'left' }} keyboardType={'numeric'} placeholder="100" onChangeText={(amount) => this.setState({ amount })} />
                        </View>


                        <View style={styles.promoCodeContainer}>
                            <Text style={{ textAlignVertical: 'center', width: '50%', color: COLORS.bgColor }}>{translate('add_money_promo_code')}</Text>
                            <View style={[styles.TextInputContainer]}>
                                <TextInput />
                            </View>
                        </View>

                    </View>
                </ScrollView>

                <View style={styles.confirmContainer}>
                    <TouchableOpacity style={[styles.btnCheckOutProceed]} onPress={() => this.onConfirmHandler()}>
                        <Text style={[styles.textBold, { color: 'white', flex: 1, alignSelf: 'center', fontSize: 20 }]}>{translate('common_confirm')}</Text>
                    </TouchableOpacity>

                </View>



            </View>

        );
    }

    renderFlatList() {
        return (

            <View>
                <FlatList
                    data={[
                        { key: paymentBundle.AED200, description: translate('add_money_pay_200'), title: I18nManager.isRTL ? '200' + ' ' + translate('common_aed') : translate('common_aed') + ' ' + '200' },
                        { key: paymentBundle.AED500, description: translate('add_money_pay_500'), title: I18nManager.isRTL ? '500' + ' ' + translate('common_aed') : translate('common_aed') + ' ' + '500' },

                    ]}
                    renderItem={
                        ({ item }) =>

                            <TouchableOpacity onPress={() => this.onOptionClicked(item.key)}>
                                <View style={[styles.ItemviewContainer, { flexDirection: 'column' }]} >

                                    <View style={[{ flexDirection: 'column', flex: 1, height: '100%', width: '100%' }]}>

                                        <View style={[{ flexDirection: 'row', flex: 1, width: '100%', padding: 10 }]}>
                                            <Image
                                                style={{ resizeMode: 'contain', alignSelf: "center", height: 20, tintColor: this.state.selectedOption == item.key ? COLORS.red : COLORS.placehldrgrey }}

                                                source={require('../assets/redtick.png')}
                                            />
                                            <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.textblack, { paddingLeft: 10, fontWeight: 'bold' }]}>{item.title}</Text>

                                        </View>

                                        <View style={[styles.lineStyle]}></View>

                                        <View style={[{ flexDirection: 'row', width: '100%', paddingLeft: 13, paddingTop: 10, paddingRight: 10, paddingBottom: 10 }]}>
                                            <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.textblack, { fontWeight: 'bold' }]}>{item.description}</Text>
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                    }
                />
            </View>


        );
    }

}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        flexDirection: 'column'
    },
    name: {
        fontSize: 20,
        color: "black",
        textAlign: 'right',
        alignSelf: 'flex-start'

    }, inputContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        flexDirection: 'column',
        marginHorizontal: 20,
    }, textNorml: {
        marginTop: 15,
        marginBottom: 15,
        textAlign: 'center',
        fontSize: 12
    }, orContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%'
    }, confirmContainer: {
        flex: 1,
        width: '100%',
        height: 50,
        bottom: 0,
        position: 'absolute'
    },
    lineStyle: {
        borderWidth: 0.35,
        borderColor: COLORS.Orange,
        backgroundColor: COLORS.Orange
    }, promoCodeContainer: {
        height: 40,
        width: '100%',
        flexDirection: 'row',
        marginTop: 20
    },
    viewContainer: {
        borderWidth: 0.5,
        borderColor: '#000',
        borderRadius: 10,
        marginTop: 10,
        padding: 10,
    },
    TextInputContainer: {
        borderWidth: 0.5,
        borderColor: COLORS.bgColor,
        borderRadius: 15,
        width: '50%',
        paddingLeft: 10,
        paddingRight: 10
    },
    ItemviewContainer: {
        flexDirection: 'row',
        borderWidth: 0.5,
        borderColor: '#f55130',
        borderRadius: 5,
        marginTop: 10,
    },

    textblack: {

        fontSize: 12,
        color: "black",
    },
    greytext: {
        fontSize: 20,
        color: "grey",
    },
    boldtext: {
        fontSize: 13,
        color: "black",
        alignSelf: 'center',
        justifyContent: 'center',
        marginLeft: 10,
        fontWeight: 'bold'
    },
    btnCheckOutProceed: {
        flex: 1,

        width: '100%',
        backgroundColor: "#F55130"
    },
    textBold: {
        fontWeight: 'bold',
        fontSize: 12,
        textAlignVertical: 'center',
    },

});     