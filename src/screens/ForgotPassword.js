import React, { Component } from 'react';
import {
  I18nManager,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,

} from 'react-native';


import SplashScreen from 'react-native-splash-screen';
import COLORS from '../stylehelper/Color';
import Spinner from 'react-native-loading-spinner-overlay';
import    {setI18nConfig} from './../utils/i18n';
import      {translate } from './../utils/i18n';
import   * as RNLocalize  from 'react-native-localize';

export default class Forgotpass extends Component {

  constructor(props) {
    super(props)
    this.state = {
      manualcode: '',
      email: '',
    }
    //setI18nConfig();

  }

  componentDidMount() {
    RNLocalize.addEventListener('change', this.handleLocalizationChange);
  }



  componentWillUnmount() {
    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
  }

  handleLocalizationChange = () => {
    setI18nConfig();
    this.forceUpdate();
  };

  render() {

    return (

      <View style={{ flex: 1, flexDirection: 'column', backgroundColor: COLORS.Orange }}>

        <Spinner
          visible={this.props.loadingotp}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />


        <View style={[styles.container, { flex: 0.92 }]}>

          <Image
            source={require('../assets/logo.png')}
            resizeMode={"contain"}
            style={{ flex: 0, width: 150, height: 50, alignSelf: 'center', marginTop: 20 }}
          />

          <View
            style={[styles.TextContainer]}>
            <Text style={[styles.signinText, { alignSelf: 'stretch' }]}>{translate('forgot_password')}</Text>
          </View>

          <TextInput
            style={[styles.inputContainer, styles.inputs]}
            placeholderTextColor={'white'}
            placeholder={translate('forgot_password_please_enter_email')}
            keyboardType="numeric"
            onChangeText={(manualcode) => this.setState({ manualcode })}
            underlineColorAndroid='transparent' />


          <TouchableOpacity style={[styles.buttonContainer, styles.loginButton, { marginTop: 25 }]}
          >
          <Text style={[styles.loginText]}>{translate('forgot_password_btn_send')}</Text>
          </TouchableOpacity>

        
        </View>


        <View style={{ flex: 0.08, flexDirection: 'column', justifyContent: 'center', bottom: 0, height: 60, backgroundColor: 'rgba(255,255,255,0.2)', width: '100%' }}>
          <Text style={{ flexDirection: 'row', alignSelf: 'center' }}>
            <Text style={{ color: COLORS.white, alignSelf: 'center', textAlignVertical: 'center' }}>{translate('forgot_password_back_to')}</Text>
            <Text style={{ color: COLORS.white, alignSelf: 'center', textAlignVertical: 'center', fontWeight: 'bold' }}>{translate('forgot_password_login')}</Text>
          </Text>
        </View>
      </View>


    )
  }
}

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#FFF'
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: COLORS.Orange,
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: 'rgba(255,255,255,0.2)',
    borderRadius: 30,
    marginLeft: 12,
    marginRight: 12,

    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  inputs: {

    paddingLeft: 30,
    borderBottomColor: '#FFFFFF',
    flex: 0,
    paddingTop: 15,
    paddingBottom: 15,
    color: 'white',
    fontSize: 20
  },
  inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 15,
    justifyContent: 'center'
  },
  buttonContainer: {

    flexDirection: 'row',
    marginBottom: 10,
    marginLeft: 15,
    marginRight: 15,
    height: 50,
    borderRadius: 30,
    borderWidth: 0.5,
    borderColor: '#fff',
    alignSelf: 'center',
    marginTop: 2
  },
  loginButton: {
    backgroundColor: "#F55130",
  },
  loginText: {
    flex: 1,
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
    alignSelf: 'center'

  },

  signinText: {
    fontSize: 22,
    color: "white",
    textAlign: 'left',
    marginLeft: 30,
    marginRight: 30
    // <-- the magic
  }
  ,
  TextContainer: {
    width: '100%',
    height: 40
  },

});

