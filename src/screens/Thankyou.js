import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import COLORS from '../stylehelper/Color';
import { setI18nConfig } from './../utils/i18n';
import { translate } from './../utils/i18n';
import * as RNLocalize from 'react-native-localize';
import SplashScreen from 'react-native-splash-screen';
import { setBaseURL, cancelRequest } from '../apis';
import renderIf from '../components/common/renderIf';

export default class Thankyou extends Component {


    constructor(props) {
        super(props);
        setBaseURL('http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1');

        // setI18nConfig();

    }
    componentWillMount() {
        SplashScreen.hide();
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', this.handleLocalizationChange);
    }



    componentWillUnmount() {
        RNLocalize.removeEventListener('change', this.handleLocalizationChange);
    }

    handleLocalizationChange = () => {
        setI18nConfig();
        this.forceUpdate();
    };


    //{this.props.navigation.state.params.walletamount} neechy AED k baad lgega


    render() {
        const notSuccsess = this.props.navigation.state.params.success == 'failure';
        console.log(notSuccsess)
        return (
            <View style={{ backgroundColor: COLORS.Orange, flex: 1, flexDirection: 'column', justifyContent: 'center', paddingLeft: 50, paddingRight: 50 }}>

                <Image
                    style={{ resizeMode: 'contain', width: 100, height: 100, alignSelf: "center" }}
                    source={notSuccsess ? require('../assets/bluecross.png') : require('../assets/bluetick.png')}
                />
                {renderIf(notSuccsess)(
                    <Text style={{ color: COLORS.white, textAlign: 'center', fontSize: 22, marginTop: 20 }}
                        ellipsizeMode="middle"
                    >{this.props.navigation.state.params.message}</Text>)}

           
           {renderIf(!notSuccsess)(
                    <Text style={{ color: COLORS.white, textAlign: 'center', fontSize: 22, marginTop: 20 }}
                        ellipsizeMode="middle"
                    >{translate('thankyou_for_your_wallet')}</Text>)}

                {renderIf(!notSuccsess)(

                    <View style={{ flexDirection: 'column' }}>
                        <Text style={{ color: COLORS.white, textAlign: 'center', fontSize: 18, marginTop: 20 }}
                            ellipsizeMode="middle"
                            numberOfLines={1}>{translate('thankyou_you_hvjust_paid')}</Text>

                        <Text style={{ color: COLORS.white, textAlign: 'center', fontSize: 25, fontWeight: 'bold' }}
                            ellipsizeMode="middle"
                            numberOfLines={1}>AED {this.props.navigation.state.params.walletamount}</Text>

                    </View>)}

                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('Wallet')}
                >
                    <Text style={{ textDecorationLine: 'underline', color: COLORS.white, textAlign: 'center', fontSize: 20, marginTop: 20 }}
                        ellipsizeMode="middle"
                        numberOfLines={1}
                    >{translate('thankyou_Go_to_your_wallet')}
                    </Text>
                </TouchableOpacity>
                {/*    <TouchableOpacity>
                    <Text style={{ textDecorationLine: 'underline', color: COLORS.white, textAlign: 'center', fontSize: 20, marginTop: 20 }}
                        ellipsizeMode="middle"
                        numberOfLines={1}
                    >{translate('thankyou_View_your_Order')}

                    </Text>
                </TouchableOpacity> */}

            </View>
        );
    }
}


const styles = StyleSheet.create({
    loginButton: {
        backgroundColor: COLORS.lightblue,
    },
    buttonContainer: {
        flexDirection: 'row',
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 50,
        height: 50,
        borderRadius: 10,
        borderWidth: 0.5,
        borderColor: COLORS.white,
        width: 100,
        alignSelf: 'center'
    },
    loginText: {
        flex: 1,
        color: 'white',
        fontSize: 25,
        textAlign: 'center',
        alignSelf: 'center'

    },
});


