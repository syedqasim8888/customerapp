import React, { Component } from 'react';
import { Keyboard, StyleSheet, Text, View, FlatList, TouchableOpacity, Dimensions, Image, ScrollView } from 'react-native';


import COLORS from '../assets/colors';
import SplashScreen from 'react-native-splash-screen';
import { setBaseURL, cancelRequest, setCustomHeaders } from "../apis";
import { connect } from "react-redux";
import { fetchProducts, addtoCart, increaseQuantity, decreaseQuantity } from "../store/actions/ProductAction";
import { bindActionCreators } from 'redux';

 
var s = require('../components/style');
import * as RNLocalize from 'react-native-localize';
import { setI18nConfig } from './../utils/i18n';
import { translate } from './../utils/i18n';



class ProductDetails extends Component {

    componentWillMount() {
        SplashScreen.hide();
    }



    componentDidMount() {
        RNLocalize.addEventListener('change', this.handleLocalizationChange);
    }


    componentWillUnmount() {
        cancelRequest();
        RNLocalize.removeEventListener('change', this.handleLocalizationChange);
    }



    shouldComponentRender() {
        const { loading } = this.props;

        if (this.loading === false) return false;
        // more tests
        return true;
    }
    constructor(props) {
        super(props);

        setI18nConfig();
        this.state = {


            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
            orientation: (Dimensions.get('window').width > Dimensions.get('window').height) ? 'LANDSCAPE' : 'PORTRAIT'
        };

        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        Keyboard.dismiss();
    }

    handleClick = (id, qty) => {
        this.props.addtoCart(id, qty);
    }



    render() {
      
       const  item =  this.props.products.find(item => item._id === this.props.navigation.state.params.item._id )       

        item.quantity = item.quantity && item.quantity > 0 ? item.quantity : item.quantity = 1;  
        console.log(item.productname)
        return (

            <View style={{flex:1,flexDirection:'row'  }}> 
            <View style={styles.cardContainer}>

                <Image source={{ uri: item.productimage }} resizeMode="contain" style={styles.cardImageStyle} />
                <Text style={styles.cardTextBold}>{item.productname}</Text>

                <View style={{ flexDirection: 'row',  alignItems:'center',alignSelf:'center' ,alignContent:'center' , marginTop:10 }} >
                    <Text style={styles.cardTextDecoration}>{item.currencytypes} {(item.discount + item.price).toFixed(2)}</Text>
                    <Text style={[styles.cardTextBold, { marginTop: 0,   }]}>{item.currencytypes} {item.price}</Text>
                    {/* <Text style={[ { marginTop: 0,fontWeight:'normal' ,fontSize:13 , color: '#cecece'}]}>(Plus Tax)</Text> */}
                </View>
                <View style={{flexDirection:'row' ,width:'80%' , alignItems:'center',alignSelf:'center' ,alignContent:'center',marginTop:10  }}>
                <View style={styles.cardQuantityContainer}>
                    <TouchableOpacity onPress={() => { this.props.decreaseQuantity(item._id, item.quantity) }}>
                        <Text style={styles.cardTextSelect}>-</Text>
                    </TouchableOpacity>
                    <Text style={{ alignSelf: 'center', fontSize: 12, color: 'black', padding: 5 }}>{item.quantity} </Text>
                    <TouchableOpacity onPress={() => { this.props.increaseQuantity(item._id, item.quantity) }} >
                        <Text style={styles.cardTextSelect}>+</Text>
                    </TouchableOpacity>

                </View>
                <TouchableOpacity style={styles.addtoCartButton} onPress={() => { this.props.addtoCart(item._id, 5) }} >
                    <Text style={styles.textWhite}>{translate('home_add_to_cart')}</Text>
                </TouchableOpacity>
                </View>
            </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({

    cardContainer: {
        flex:1,
        paddingTop: 20,
        flexDirection: 'column',
        backgroundColor: 'white'

    }, cardImageStyle: {
        alignSelf: 'center',
        width: 150,
        flex:0.6,
        marginTop: 10
    }, cardTextBold: {
        fontSize: 14,
        marginLeft:10,
        marginRight:1,
       
        fontWeight: 'bold',
        color: COLORS.black,
        textAlign: 'center',


    }, cardTextDecoration: {
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid',
        fontSize: 13,
        marginLeft: 5,
        textDecorationColor: COLORS.bgColor,
        color: '#cecece'
    }, cardQuantityContainer: {
        flexDirection: 'row',
        flex:1,
        justifyContent: 'space-between',
 
 
        borderWidth: 1,
        borderTopLeftRadius: 3,
        borderBottomLeftRadius: 3,
        borderColor: COLORS.bgColor,
        borderRightWidth:0
    }, cardTextSelect: {
        alignSelf: 'center',
        color: COLORS.bgColor,
        fontWeight: 'bold',
        fontSize: 18,
        paddingHorizontal:10,
         
        flex: 1
    }, addtoCartButton: {
        flex:1,
        borderTopRightRadius: 3,
        borderBottomRightRadius: 3,
        backgroundColor: COLORS.bgColor
    }, textWhite: {
        alignSelf: 'center',
        paddingTop: 5,
        paddingBottom: 5,
        color: 'white'
    }, listrow: {
        flex: 1,
        justifyContent: "space-around",

    }
});



const mapStateToProps = ({ products }) => {
    // RNTestLibModule.show("Hello World");
    console.log('IN Product Details' ,products.products)
    return { products: products.products, error: products.error, loading: products.loading };
}


function mapDispatchToProps(dispatch) {
    return {
        ...bindActionCreators({ addtoCart, increaseQuantity, decreaseQuantity }, dispatch)
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductDetails);
