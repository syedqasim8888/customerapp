import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert,
  TextInput,
  Dimensions,
  StatusBar,
  Image,
  ScrollView,
  TouchableOpacity,
  I18nManager,
  Keyboard

} from 'react-native';
import { bindActionCreators } from 'redux';
import SplashScreen from 'react-native-splash-screen';
import COLORS from '../stylehelper/Color';
import { connect } from "react-redux";
import { setBaseURL, cancelRequest, setCustomHeaders } from "../apis";
import { registerUser } from "../store/actions/SignUpAction";
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';
import Toast, { DURATION } from 'react-native-easy-toast'
import { setI18nConfig } from './../utils/i18n';
import { translate } from './../utils/i18n';
import * as RNLocalize from 'react-native-localize';

const { width } = Dimensions.get('window');


class EnterPhoneno extends Component {

  constructor(props) {
    super(props)
    setBaseURL("http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1");
    this.state = {
      name: this.props.navigation.state.params.name,
      email: this.props.navigation.state.params.email,
      password: this.props.navigation.state.params.password,
      mobile: '',
      logintype:this.props.navigation.state.params.logintype,      
    }

    console.log("name: "+this.state.name+"\nemail: "+this.state.email+"\npassword: "+this.state.password+"\nlogintype: "+this.state.logintype);

  }

  // onCreate
  componentWillMount() {
    SplashScreen.hide();
  }

  componentDidMount() {
    RNLocalize.addEventListener('change', this.handleLocalizationChange);
  }

  componentWillUnmount() {
    cancelRequest();
    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
  }


  handleLocalizationChange = () => {
    setI18nConfig();
    this.forceUpdate();
  };


  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.error !== nextProps.error || this.props.message !== nextProps.message || this.props.loading !== nextProps.loading) {
      return true
    }
    return false
  }

  componentWillReceiveProps(newProps) {
    console.log(newProps)
    // false

      // false && true   ->default
      // true && true    ->failure
      if (newProps.error  && !newProps.succsess) {
        alert("Register Error" + newProps.message);
      } 
      // true && false  ->default
      // true && true    ->succsess
      // false && false    ->failure

      else if (!newProps.error  &&  newProps.succsess)  {
        //
        (async () => {
          await this._storePassword();
        })();

      }else if (newProps.error){
        // TODO: print errpr message
      }
  


  }

  _storePassword = async () => {
    try {
      if (this.state.password && this.state.email) {
        // Alert.alert('Saved', 'Successful'+this.props.message.email);
        await AsyncStorage.setItem('password', this.state.password);
        await AsyncStorage.setItem('email', this.state.email);
        const password = await AsyncStorage.getItem('password');
        const email = await AsyncStorage.getItem('email');
        // Alert.alert('Saved', 'Suc passav '+password + ' Sav Email: '+email );
        this.props.navigation.navigate("OTP");
      }
      else {
        // Alert.alert('Saved', 'Error in async ');
      }

    } catch (error) {
      // Error saving data
    }
  };

  componentDidMount() {
    cancelRequest();
  }


  CheckTextInput = () => {
    //Handler for the Submit onPress
    if (this.state.name != '') {
      if (this.state.email != '') {
        //Check for the Name TextInput

        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (reg.test(this.state.email) === true) {

            if (this.state.password != '') {

             if (this.state.mobile != '') {

                       //Check for the Email TextInput
                        this.props.registerUser({
                          name: this.state.name,
                          email: this.state.email,
                          lastName: "",
                          password: this.state.password,
                          wallet: "",
                          nationality: "",
                          birthDate: "",
                          mobile: this.state.mobile,
                          gender: "",
                          TRNNumber: "",
                          devicekey: "",
                          devicename: "",
                          logintype: this.state.logintype,
                          google_id: "",
                          fb_id: ""

                          
                        });

                  } else {
                    this.refs.toast.show('please enter mobile no',500);
                  }

            } else {
              this.refs.toast.show('please enter password',500);
            }
          } 
        else {
          this.refs.toast.show('email is not valid',500);
        }
      }
      else {
        this.refs.toast.show('please enter email',500);
      }
    }
    else {
      this.refs.toast.show('please enter name',500);
    }
  };


  signup() {
    // Actions.OrderList();
  }


  render() {

    return (

      <View style={{ flex: 1, backgroundColor: COLORS.Orange }}>

        <Spinner
          visible={this.props.loading}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />

        <ScrollView  >


          <View style={styles.container}>

            <Image
              source={require('../assets/logo.png')}
              resizeMode={"contain"}
              style={{ flex: 0, width: 150, height: 50, alignSelf: 'center', marginTop: 20 }}
            />

            <View
              style={[styles.TextContainer]}>
              <Text style={[styles.signinText, { alignSelf: 'stretch' }]}>Phoneno</Text>
            </View>


            <TextInput
              style={[styles.inputContainer, styles.inputs]}
              placeholderTextColor={'white'}
              placeholder="Enter phone no"
              onChangeText={(mobile) => this.setState({ mobile })}
              underlineColorAndroid='transparent'
              label={"Field 1"}
              keyboardType='phone-pad'
              blurOnSubmit={false}
              onSubmitEditing={() => this.sirnamefamilynameRef.focus()}
              />

            <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]}

              onPress={() => {
                this.CheckTextInput()
              }}>

              <Text style={[styles.loginText]}>Submit</Text>
            </TouchableOpacity>

            <Toast
              ref="toast"
              style={{ backgroundColor:COLORS.lightblack, borderRadius: 30}}
              position='bottom'
              positionValue={200}
              textStyle={{ color: COLORS.white }}
            />

          </View>

        </ScrollView >
      </View>

    )
  }
}

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#FFF'
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: COLORS.Orange,
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: 'rgba(255,255,255,0.2)',
    textAlign: I18nManager.isRTL ? "center" : "left",
    borderRadius: 30,
    marginBottom: 10,
    flexDirection: 'row',
    marginHorizontal:15
  },
  inputs: {

    paddingLeft:30,
    paddingRight:30,
    borderBottomColor: '#FFFFFF',
    flex: 0,
    paddingTop: 10,
    paddingBottom: 10,
    color: 'white',
    fontSize: 16
  },
  inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 15,
    justifyContent: 'center'
  },
  buttonContainer: {

    flexDirection: 'row',
    marginBottom: 10,
    marginTop:20,
    marginLeft: 15,
    marginRight: 15,
    paddingTop: 10,
    paddingBottom: 10,
    borderRadius: 30,
    borderWidth: 0.5,
    borderColor: '#fff'
  },
  loginButton: {
    backgroundColor: "#F55130",
  },
  loginText: {
    flex: 1,
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
    alignSelf: 'center'

  },

  signinText: {
    fontSize: 22,
    color: "white",
    textAlign: 'left',
    marginLeft: 30,
    marginRight: 30
    // <-- the magic
  }
  ,
  TextContainer: {
    width: '100%',
    height: 40
  },
  buttonRoundSmall: {
    padding: 12,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 30,
    width: width / 2 - 30,
    backgroundColor: COLORS.white
  },
});


const mapStateToProps = ({ signup, loading }) => {

  console.log(signup)
  //this.refs.toast.show(signup.message.verificationcode+"",500);
  // console.warn("Data:"+signup.message.email)
  //console.log( 'Map State to props--------- ',signup.message,signup.data)
  return {
    message: signup.message,
    error: signup.error,
    loading: signup.loading,
    succsess:signup.succsess
  };
};


function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({ registerUser }, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps)
  (EnterPhoneno);