import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Feather';

import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableHighlight,
    Image

} from 'react-native';
import COLORS from '../stylehelper/Color';

export default class Contactus extends Component {


    constructor(props) {
        super(props);
    

    }


    render() {
        return (
            <View style={{ backgroundColor: COLORS.white, flex: 1, flexDirection: 'column' }}>

                <View style={{ flexDirection: 'row', marginHorizontal: 20, marginTop: 20 }}>
                    <Icon name="arrow-left" size={30} color={COLORS.Orange} />
                    <Text numberOfLines={1} ellipsizeMode='tail' style={{ color: COLORS.Orange, fontSize: 18, textAlignVertical: 'center',marginLeft:5}}>Back</Text>

                </View>

                <TextInput
                    placeholder="Please state your problem or suggestion here. if you advice your small id we will revert back if required"
                    underlineColorAndroid='transparent'
                    multiline={true}
                    style={[styles.TextInputStyleClass]} />

                <TouchableHighlight >
                    <Text style={[styles.ButtonStyleClass, {marginTop:40, textAlignVertical: 'center', textAlign: 'center', color: COLORS.white,fontWeight:'bold' }]}>Submit</Text>
                </TouchableHighlight>

                <View style={{ marginHorizontal:20,flexDirection: 'row', top: 50, borderRadius: 10, borderColor: 'black', padding: 15, borderWidth: 0.5 }}>
                <Image
                  style={{ flex: 0.15, resizeMode: 'contain', alignSelf: "center", height: 30 }}
                  source={require('../assets/ic_phone.png')}
                />

                <Text numberOfLines={1} ellipsizeMode='tail' style={{ flex: 0.85, fontWeight: 'bold', textAlignVertical: 'center' }} >Call us</Text>

            
              </View>

            </View>
        );
    }
}


const styles = StyleSheet.create({

    TextInputStyleClass: {

        textAlign: 'left',
        height: 300,
        borderWidth: 0.5,
        borderColor: COLORS.black,
        borderRadius: 10,
        backgroundColor: "#FFFFFF",
        marginHorizontal: 20,
        marginTop: 30,
        textAlignVertical: 'top',
        paddingHorizontal: 20
    },
    ButtonStyleClass: {
        height: 50,
        borderRadius: 20,
        backgroundColor: COLORS.Orange,
        marginHorizontal: 20,
        marginTop: 10,
        marginBottom: 5,
        textAlign: 'left',
        padding: 10

    },
});


