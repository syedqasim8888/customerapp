
import React, { Component } from 'react';
import { removeFromCart, increaseQuantity, decreaseQuantity } from "../store/actions/ProductAction";
import { getCartTotal } from "../store/reducer/ProductReducer";
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { cancelRequest } from '../apis';
import SplashScreen from 'react-native-splash-screen';
import { I18nManager, StyleSheet, Text, View, TouchableOpacity, Image, Alert, Dimensions, FlatList, ScrollView, }
    from 'react-native';

import * as RNLocalize from 'react-native-localize';
import { setI18nConfig } from './../utils/i18n';
import { translate } from './../utils/i18n';
import translationGetters from './../utils/i18n';
import COLORS from '../stylehelper/Color';
import MapViewDirections from 'react-native-maps-directions';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
const width = Dimensions.get('window').width;
const origin = {latitude: 37.3318456, longitude: -122.0296002};
const destination = {latitude: 37.771707, longitude: -122.4053769};
const GOOGLE_MAPS_APIKEY = "AIzaSyAe3jdYRdpYH8NopuJ52EnFyNQwMDVXP10";
export default class DriverTrack extends Component {

    componentWillMount() {
        SplashScreen.hide();
    }

    constructor(props) {
        super(props);
        setI18nConfig();
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', this.handleLocalizationChange);
    }

    componentWillUnmount() {
        cancelRequest();
        RNLocalize.removeEventListener('change', this.handleLocalizationChange);
    }

    clickEventListener() {
        Alert.alert("Success", "Product has beed added to cart")
    }

    render() {
        return (
            <View style={styles.container}>
                <MapView
                    provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                    style={styles.map}
                    region={{
                        latitude: 37.78825,
                        longitude: -122.4324,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121,
                    }}
                >

                    <MapViewDirections
                        origin={origin}
                        destination={destination}
                        apikey={GOOGLE_MAPS_APIKEY}
                    />
                </MapView>
            </View>


        );


    }
}

const styles = StyleSheet.create({


    container: {
        ...StyleSheet.absoluteFillObject,
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },


});

