import React, { Component } from 'react';
import {
  I18nManager,
  StyleSheet,
  Text,
  View,
  Alert,
  TextInput,
  Dimensions,
  Image,
  TouchableOpacity,
  Keyboard,
  Share,
  Platform
} from 'react-native';

import SplashScreen from 'react-native-splash-screen';
import { connect } from "react-redux";
import { setBaseURL, cancelRequest, setCustomHeaders } from "../apis";
import { registerUserFacebook, registerUserGmail, loginUser, fetchToken } from "../store/actions/LoginAction";
import COLORS from '../stylehelper/Color';
import { bindActionCreators } from 'redux';
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast, { DURATION } from 'react-native-easy-toast'
import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
const { width } = Dimensions.get('window');
import * as RNLocalize from 'react-native-localize';
import { setI18nConfig } from './../utils/i18n';
import { translate } from './../utils/i18n';
import firebase from 'react-native-firebase';
import { LoginManager, AccessToken } from "react-native-fbsdk";


class Login extends Component {
  constructor(props) {
    super(props);

    setBaseURL("http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1");
    setI18nConfig();

    this.state = {
      UserEmail: '',
      UserPassword: '',
      DeviceToken: "",
      devicekey: "",
      devicename: Platform.OS === 'ios' ? 'ios' : 'android',
      name: '',
      email: '',
      logintype: "",
      google_id: "",
      fb_id: "",
      login_type: "",
    };

  }



  async checkPermission() {
    console.log('Check Permission');
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      console.log('Enabled');
      this.getToken();
    } else {
      console.log('requestPermission');
      this.requestPermission();
    }
  }
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }

  async getToken() {
    console.log(' get Token ');
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    if (!fcmToken) {
      console.log('Not FCM TOken ');
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        //console.warn(fcmToken)
        console.log('  FCM TOken ');
        // user has a device token

        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
    this.setState({ DeviceToken: fcmToken });
    console.log(' get Token ' + fcmToken);
  }


  async  componentDidMount() {
    this.checkPermission();
    this.createNotificationListeners(); //add this line
    RNLocalize.addEventListener('change', this.handleLocalizationChange);
    GoogleSignin.configure();
  }


  // Somewhere in your code
  signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log(userInfo)
      console.log("signin ", userInfo.user.name, "/", userInfo.user.email, "/", userInfo.user.id);
      this.setState({ userInfo });
      //setstate
      this.setState({
        name: userInfo.user.name,
        email: userInfo.user.email,
        google_id: userInfo.user.id,
        login_type: "google"
      });
      // Check for the Email TextInput
      this.props.registerUserGmail({
        name: userInfo.user.name,
        email: userInfo.user.email,
        lastName: "",
        password: userInfo.user.id,
        wallet: "",
        nationality: "",
        birthDate: "",
        mobile: "",
        gender: "",
        TRNNumber: "",
        devicekey: this.state.DeviceToken,
        devicename:this.state.devicename,
        logintype: "google",
        google_id: "",
        fb_id: ""
      });


    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        //Alert.alert('Error', 'success.')
      } else if (error.code === statusCodes.IN_PROGRESS) {

        // Alert.alert('sign in is in progress already')

      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {

        // Alert.alert('play services not available or outdated')

      } else {
        console.log(error)

        //Alert.alert('Something went wrong')

      }
    }
  };


  componentWillMount() {
    SplashScreen.hide();
  }

  componentWillUnmount() {
    this.notificationListener();
    this.notificationOpenedListener();
    cancelRequest();
    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
  }

  async createNotificationListeners() {
    /*
    * Triggered when a particular notification has been received in foreground
    * */
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      const { title, body } = notification;
      this.showAlert(title, body);
    });

    /*
       If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      const { title, body } = notificationOpen.notification;
      this.showAlert(title, body);
    });

    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
      const { title, body } = notificationOpen.notification;
      this.showAlert(title, body);
    }
    this.messageListener = firebase.messaging().onMessage((message) => {
      //process data message
      console.log(JSON.stringify(message));
    });
  }
  showAlert(title, body) {
    Alert.alert(
      title, body,
      [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ],
      { cancelable: false },
    );
  }


  componentWillReceiveProps(newProps) {
    // this.setState({ UserEmail: newProps.eamil })

    switch (newProps.case) {

      case 'TOKEN':
        if (this.props.savetoken !== newProps.savetoken) {
          console.log("will recieve props: " + newProps.savetoken);
          this.onSave(newProps);
        }
        else if (newProps.errtoken) {
          this.refs.toast.show('Some thing went wrong please try again',500);
        }
        break;
      case 'LOGIN':

        // default --> !false && false
        if (!newProps.err && newProps.loginsuccess) {
          console.log(this.state.UserEmail)
          this.props.fetchToken({
            email: this.state.UserEmail,
            password: this.state.UserPassword
          });
        }
        else if (newProps.err && !newProps.loginsuccess) {
          this.refs.toast.show('email/password is incorrect',500);
        }
        break
      case 'GMAIL':

        console.log("Is Varified: " + newProps.varified);

        if (!newProps.varified && newProps.gmailsuccsess) {
          console.log("name:", this.state.name, "email:", this.state.email, "pass:", this.state.google_id, "logintype:", this.state.login_type, "googleid:", this.state.google_id);
          this.props.navigation.navigate('EnterPhoneno', { name: this.state.name, email: this.state.email, pass: this.state.google_id, logintype: "google", googleid: this.state.google_id })
        }
        else if (newProps.varified && newProps.gmailsuccsess) {
          //logintype
          this.props.fetchToken({
            email: this.state.email,
            password: this.state.google_id
          });

          //this.props.navigation.navigate('MainStack', { name: this.state.name, email: this.state.email, pass: this.state.google_id, logintype: this.state.login_type, googleid: this.state.google_id })
        } else if (newProps.err) {
          // TODO: print gmail error message 
        }
        break;

      case 'FACEBOOK':

        console.log("Is Varified: " + newProps.varified);

        if (!newProps.varified && newProps.facebooksuccsess) {
          console.log("name:", this.state.name, "email:", this.state.email, "pass:", this.state.google_id, "logintype:", this.state.login_type, "googleid:", this.state.google_id);
          this.props.navigation.navigate('EnterPhoneno', { name: this.state.name, email: this.state.email, password: this.state.fb_id, logintype: "facebook", googleid: this.state.google_id })
        }
        else if (newProps.varified && newProps.facebooksuccsess) {
          //logintype
          this.props.fetchToken({
            email: this.state.email,
            password: this.state.fb_id
          });

        } else if (newProps.err) {
          // TODO: print facebbok error message 
        }
        break;



      default:
        return
    }
  

  }


  onSave = async (newProps) => {
    try {
      console.log('Token in Onsave Login ', newProps.savetoken)
      console.log('USERDATA in Onsave Login ', newProps.userdata)

      AsyncStorage.setItem("token", newProps.savetoken);
      AsyncStorage.setItem('password', this.state.UserPassword);
      AsyncStorage.setItem("userobject", JSON.stringify(newProps.userdata));
      AsyncStorage.setItem('islogin', '1');

      // alert('on SaveToken')

      this.props.navigation.navigate('MainStack', { token: newProps.savetoken })
      //   Alert.alert('Saved', 'Successful');
    } catch (error) {
      console.log(error)
      Alert.alert('Error', 'There was an error.')
    }
  }

  async removeItemValue(key) {
    try {
      await AsyncStorage.removeItem(key);
      return true;
    }
    catch (exception) {
      return false;
    }
  };

  handleLocalizationChange = () => {
    // setI18nConfig();
    this.forceUpdate();
  };

  CheckTextInput = () => {
    //Handler for the Submit onPress
    if (this.state.UserEmail != '') {
      //Check for the Name TextInput

      const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

      if (reg.test(this.state.UserEmail) === true) {

        if (this.state.UserPassword != '') {
          //Check for the Email TextInput
          this.props.loginUser({
            email: this.state.UserEmail,
            password: this.state.UserPassword,
            devicekey: this.state.DeviceToken,
            devicename: "android"

          });

        } else {
          this.refs.toast.show('please enter password',500);
        }
      }
      else {
        this.refs.toast.show('email is not valid',500);
      }
    } else {
      this.refs.toast.show('please enter email',500);
    }
  };

  render() {

    return (

      <KeyboardAwareScrollView
        style={{ backgroundColor: COLORS.Orange }}
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={styles.container}
      >

        <Spinner
          visible={this.props.loading}
          textStyle={styles.spinnerTextStyle}
        />

        <View style={[styles.container, { flex: 0.92 }]}>

          <Image
            source={require('../assets/logo.png')}
            resizeMode={"contain"}
            style={{ flex: 0, width: 150, height: 50, alignSelf: 'center' }}
          />

          <View style={styles.TextContainer}>

            <Text style={styles.signinText}>{translate('login_title')}</Text>
          </View>

          <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
              placeholderTextColor={'white'}
              placeholder={translate('login_email')}
              keyboardType="email-address"
              autoCapitalize='none'
              underlineColorAndroid='transparent'
              onChangeText={(UserEmail) => this.setState({ UserEmail })}
              blurOnSubmit={false}
              onSubmitEditing={() => this.pass.focus()}
            />
          </View>

          <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
              placeholderTextColor={'white'}
              placeholder={translate('login_password')}
              keyboardType="default"
              secureTextEntry={true}
              onSubmitEditing={Keyboard.dismiss}
              ref={ref => this.pass = ref}
              underlineColorAndroid='transparent'
              onChangeText={(UserPassword) => this.setState({ UserPassword })} />
          </View>


          <TouchableOpacity
            style={[styles.buttonContainer, styles.loginButton]}
            onPress={() => {

              this.CheckTextInput()

            }}>
            <Text style={[styles.loginText]}>{translate('login_submit_login')}</Text>
          </TouchableOpacity>


          <TouchableOpacity style={{ alignSelf: 'flex-end' }}
            onPress={() => {
              this.props.navigation.navigate('ForgotPassword');
            }} >
            <Text style={{ alignSelf: 'stretch', textAlign: 'right', color: '#fff', paddingTop: 5, paddingBottom: 12, marginEnd: 20 }}>{translate('login_forgot_password')}</Text>

          </TouchableOpacity>

        


          <View style={{ flexDirection: 'row', marginTop: 20 }}>

          {/* 
            <Text style={{ color: '#fff', textAlign: 'center', padding: 12 }}>{translate('login_or')}</Text>
          <TouchableOpacity style={[styles.buttonRoundSmall]}
              onPress={() => {
                this.signIn();
              }}
            >
              <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
                <Image
                  source={require('../assets/google.png')}
                  resizeMode={"contain"}
                  tintColor={COLORS.Orange}
                  style={{ width: 20, height: 20, alignSelf: 'center' }}
                />
                <Text style={{ marginLeft: 8, color: COLORS.Orange, alignSelf: 'center', fontWeight: 'bold' }}>{translate('login_google')}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.buttonRoundSmall]}
              onPress={() => {
                // Attempt a login using the Facebook login dialog asking for default permissions.
                LoginManager.logInWithPermissions(["public_profile"]).then(
                  function (result) {
                    if (result.isCancelled) {
                      console.log("Login cancelled");
                    } else {
                      //console.log("Login success with permissions: " +result.grantedPermissions.toString());
                      console.log("Successfully Login", result);
                      AccessToken.getCurrentAccessToken().then(
                        (data) => {
                          console.log(data.accessToken.toString())
                          const { accessToken } = data

                          // with the help of access token you can get details for fb login

                          console.log('accessToken: ' + accessToken);
                          fetch('https://graph.facebook.com/v2.5/me?fields=email,name,friends&access_token=' + accessToken)
                            .then((response) => response.json())
                            .then((json) => {
                              console.log('jsonn: ' + json);
                              console.log('Login name: ' + json.name);
                              console.log('Login id: ' + json.id);
                              console.log('Login email: ' + json.email);
                              var nsn = json.name
                              console.log('Login email state: ' + nsn);
                              var idfb = json.id
                              console.log('Login email state: ' + idfb);
                              var idemail = json.email
                              console.log('Login email state: ' + idemail);
                                    //setstate
                                    this.setState({
                                      name: json.name,
                                      email: json.id + "happylife@gmail.com",
                                      google_id: '',
                                      login_type: "facebook",
                                      fb_id:json.id
                                    });
                                    // Check for the FACEBOOK TextInput
                                    this.props.registerUserFacebook({
    
                                      name: this.state.name,
                                      email: this.state.email,
                                      lastName: "",
                                      password: json.id,
                                      wallet: "",
                                      nationality: "",
                                      birthDate: "",
                                      mobile: "",
                                      gender: "",
                                      TRNNumber: "",
                                      devicekey: this.state.DeviceToken,
                                      devicename:this.state.devicename,
                                      logintype: "fb",
                                      googleid: "",
                                      fb_id: json.id
                                    });

                            })
                            .catch((err) => {
                              alert('ERROR GETTING DATA FROM FACEBOOK' ,err)
                              console.log(err)
                            })

                        })
                    }
                  }.bind(this),
                  function (error) {
                    console.log("Login fail with error: " + error);
                  }
                );
              }}>
              <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
                <Image
                  source={require('../assets/f.png')}
                  resizeMode={"contain"}
                  style={{ width: 20, height: 20, alignSelf: 'center' }}
                />
                <Text style={{ marginLeft: 8, color: COLORS.Orange, alignSelf: 'center', fontWeight: 'bold' }}>{translate('login_facebook')}</Text>
              </View>
            </TouchableOpacity> */}

 
          </View>
        </View>

        <Toast
              ref="toast"
              style={{ backgroundColor:COLORS.lightblack, borderRadius: 30}}
              position='bottom'
              positionValue={200}
              textStyle={{ color: COLORS.white }}
            />

        <TouchableOpacity style={[styles.container, { flex: 0.08, backgroundColor: 'rgba(255,255,255,0.2)', width: '100%' }]}
          onPress={() => {
            this.props.navigation.navigate('SignUp');
            this.removeItemValue('token')
          }} >
          <Text
            style={{ textAlignVertical: 'center', color: COLORS.white }}>
            <Text
              style={{ textAlignVertical: 'center', color: COLORS.white }}>{translate('login_newaccount')}</Text>
            <Text style={{ fontWeight: "bold" }}>{translate('login_create')}</Text>
          </Text>
        </TouchableOpacity>


      </KeyboardAwareScrollView>
    )

  }
}

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#FFF'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: COLORS.Orange,
  }, countryPickerContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ecf0f1',
    padding: 15,
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: 'rgba(255,255,255,0.2)',
    borderRadius: 30,
    marginLeft: 15,
    marginRight: 15,
    height: 50,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputs: {
    height: 45,
    marginStart: 30,
    textAlign: I18nManager.isRTL ? "right" : "left",

    borderBottomColor: '#FFFFFF',
    flex: 1,
    color: 'white',
    fontSize: 20,
  },
  inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 15,
    justifyContent: 'center',
  },
  buttonContainer: {
    flexDirection: 'row',
    marginBottom: 10,
    marginLeft: 15,
    marginRight: 15,
    height: 50,
    borderRadius: 30,
    borderWidth: 0.5,
    borderColor: '#fff',
  },
  loginButton: {
    backgroundColor: '#F55130',
  },
  loginText: {
    flex: 1,
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
    alignSelf: 'center'
  },

  signinText: {
    marginLeft: 8,
    color: COLORS.white,
    alignSelf: 'center',
    fontWeight: 'bold',
  },
  orText: {
    color: '#fff',
    textAlign: 'auto',
    padding: 12,
  },
  TextContainer: {
    width: 300,
    height: 60,
  },
  buttonRoundSmall: {
    padding: 12,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 30,
    width: width / 2 - 30,
    backgroundColor: COLORS.white,
  },
});


const mapStateToProps = ({ login }) => {

  // console.log("matchstatetoken: " + login.user_gmaildata.email);


  return {
    casetype: login.casetype,
    userdata: login.userdata,
    loading: login.loading,
    savetoken: login.tokenstring,
    err: login.err,
    errtoken: login.errtoken,
    varified: login.varified,
    case: login.case,
    gmailsuccsess: login.gmailsuccsess,
    facebooksuccsess: login.facebooksuccsess,
    loginsuccess: login.loginsuccess
  };
};

function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({ registerUserFacebook, registerUserGmail, loginUser, fetchToken }, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps)
  (Login);