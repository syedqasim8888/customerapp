
import React, { Component } from 'react';
import { removeFromCart,  increaseQuantity  ,decreaseQuantity} from "../store/actions/ProductAction";
import {  getCartTotal } from "../store/reducer/ProductReducer";
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import {cancelRequest} from '../apis';
import SplashScreen from 'react-native-splash-screen';
import { I18nManager, StyleSheet, Text,View, TouchableOpacity, Image, Alert, Dimensions, FlatList, ScrollView, TextInput,}
    from 'react-native';
    
import   * as RNLocalize  from 'react-native-localize';
import    {setI18nConfig} from '../utils/i18n';
import      {translate } from '../utils/i18n';
import   translationGetters from '../utils/i18n';
import COLORS from '../stylehelper/Color';
import AsyncStorage from '@react-native-community/async-storage';

 
const width = Dimensions.get('window').width;
  class CustomerProfile extends Component {

    componentWillMount() {
        SplashScreen.hide();
      }
    
    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = {
            useremail: '',
            username: '',
            userphone: '',
            isEditable:false,
           // USerAddress:'',
           
          };
          this.retrieveUserDetails('userobject');

    }

    componentDidMount() {
        RNLocalize.addEventListener('change', this.handleLocalizationChange);
      }
    
      componentWillUnmount() {
        cancelRequest();
        RNLocalize.removeEventListener('change', this.handleLocalizationChange);
      }
   
    clickEventListener() {
        Alert.alert("Success", "Product has beed added to cart")
    }

    retrieveUserDetails  = async (key) => {
        try {
          const storedValue = await AsyncStorage.getItem(key);
          const email = JSON.parse(storedValue).email;
          const name = JSON.parse(storedValue).name;
          const phone = JSON.parse(storedValue).mobile;
         // const address = JSON.parse(storedValue).address;

          this.setState ({
            useremail: email,
            username: name,
            userphone: phone,
            //USerAddress:address
          })
    
          console.log('email: '+this.state.useremail+" name: "+this.state.username+ " phone: "+this.state.userphone)
     
          return
        } catch (error) {
          console.log(error);
        }
        return
      }

      saveProfile(item) {
        if (item === 'edit') {
            this.setState({ isEditable: true })
        }
        else{
            if (item === 'editdone') {
                this.setState({ isEditable: false })
            }
        }

      }

    render() {
        return (

            <View style={[styles.mainContaner]} >

                <View style={[styles.subcontainer1]}>

                    <View style={{flex:0.4,paddingHorizontal:20}}>

                    <Text style={styles.name}>{translate('profile_title')}</Text>

                    <View style={[styles.lineStyle, { marginTop: 5 }]} />

                    <View style={{flex:1,flexDirection:'column',justifyContent:'center'}}>

                    <Image source={{ uri: 'https://reactnativecode.com/wp-content/uploads/2017/10/Guitar.jpg' }}
                    style={{
                             alignSelf: 'center',
                             width: 100,
                             height: 100,
                             overflow: "hidden",
                             borderRadius: 100 / 2,
                            }} />

                    <Text style={{marginTop:10,fontSize:20,color:COLORS.Orange,textAlign:'center'}}>{this.state.username}</Text>
                    </View>
                    </View>

                    <View style={{flex:0.6,backgroundColor:COLORS.grey,paddingHorizontal:20}}>
                    <View style={{ flexDirection:'column',marginTop:20}}>
                    <Text style={{ textAlign :  "left" ,fontSize:16,color:COLORS.darkgrey}}>{translate('profile_email')}</Text>
                    <TextInput style={{ textAlign :  "left" ,fontSize:16, color:COLORS.black}} editable={this.state.isEditable}>{this.state.useremail}</TextInput>
                    </View>
                    
                
                    <View style={{ flexDirection:'column',marginTop:20}}>
                    <Text style={{ textAlign :  "left",fontSize:15,color:COLORS.darkgrey}}>{translate('profile_phone')}</Text>
                    <TextInput style={{ textAlign :  "left" ,fontSize:15, color:COLORS.black}} editable={this.state.isEditable}>{this.state.userphone}</TextInput>
                    </View>
                    <View style={{ flexDirection:'column',marginTop:20}}>
                    <Text style={{ textAlign :  "left" ,fontSize:15,color:COLORS.darkgrey}}>{translate('profile_address')}</Text>
                    <TextInput style={{ textAlign :  "left" ,fontSize:15 ,color:COLORS.black}} editable={this.state.isEditable}>#000, Aman ST, Daira, Dubai, UAE</TextInput>
                    </View>
                 
                 </View>

               
                </View>

                <View style={[{ flexDirection: 'column', flex: 0, width: '100%', bottom: 0 }]}>
                    <View style={styles.bottom}>


                        {this.state.isEditable ?
                        <TouchableOpacity style={[styles.bottomButton, { backgroundColor: COLORS.Orange }]} onPress={() =>  this.saveProfile('editdone')}>
                        <Text style={[styles.boldtext]}>Update</Text>
                        </TouchableOpacity>
                                    :
                        <TouchableOpacity style={[styles.bottomButton, { backgroundColor: COLORS.Orange }]} onPress={() =>  this.saveProfile('edit')}>
                        <Text style={[styles.boldtext]}>Edit</Text>
                        </TouchableOpacity>
                        }
                        
                    </View>
                </View>


            </View>

        );


    }
}

const styles = StyleSheet.create({

    squareContainer: {
        flexDirection: 'row',
        borderRadius: 10,
        borderColor: 'black',
        padding: 15,
        borderWidth: 0.5
    },
    estimatedtimetoarrive: {
        flex: 0.80,
        fontWeight: 'bold',
        textAlignVertical: 'center'
    },
    mainContaner: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white'
    }, subcontainer1: {
        flex: 1,
        flexDirection: 'column',
        marginTop: 20
    },
    itemContainer: {
        flexDirection: 'row',
        flex: 1,
        marginTop: 10,
        marginBottom: 10
    }, itemImage: {
        width: 100,
        height: 100,
        marginTop:5,

        alignSelf: "center",

    }, itemMiddleContainer: {
        flexDirection: 'column',
        flex: 1,
        height: '100%',
        marginTop: 5,
        marginBottom: 5
    }, itemLastContaner: {
        flexDirection: 'column',
        flex: 1,
        position: 'absolute',
        height: '100%',
        right: 0,
        justifyContent: 'space-between',
        marginBottom: 10,
        marginTop: 5,
    }, textIncrement: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#F55130',
        alignSelf: 'center',
        padding: 5
    }, quantityContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        borderRadius: 5,
        borderColor: '#f55130',
        borderWidth: 0.5,
        width: 100
    },

    flcontainer: {
        flex: 1,
        paddingTop: 22,
        backgroundColor: '#fff',
    },

    bottomButton: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15,
    },

    bottom: {
        flex: 0,
        flexDirection: 'row'
        },
  
    container: {
        flex: 1,
        marginTop: 20,
    },
    productImg: {
        width: 200,
        height: 200,
    },
    name: {
        fontSize: 20,
        color: "black",
        textAlign: 'right',
        alignSelf: 'flex-start',

    },
    lineStyle: {
        borderWidth: 0.5,
        borderColor: '#f55130',
        marginTop: 5
    },

    text: {
        fontSize: 0.03 * width,
        color: "black",
    },
    greytext: {
        marginTop: -3,
        fontSize: 13,
        color: "grey",
        alignSelf: 'flex-start'
    },
    boldtext: {
        fontSize: 0.04 * width,
        color: "white",
        alignSelf: 'center',
        justifyContent: 'center',

        fontWeight: 'bold'
    },

});     
const mapStateToProps = ({ products, loading }) => {
 
    return { cart: products.cart, error: products.error, loading: products.loading ,productCount:products.productCount ,total:getCartTotal(products) };
  }
  
  
  function mapDispatchToProps(dispatch) {
    return {
      ...bindActionCreators({ removeFromCart,increaseQuantity  ,decreaseQuantity}, dispatch)
    }
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(CustomerProfile);
  