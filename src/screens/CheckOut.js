import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Entypo';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
  TextInput,
  Button, I18nManager
} from 'react-native';
import COLORS, { Colors } from '../stylehelper/Color';
import { setBaseURL, cancelRequest, setCustomHeaders } from "../apis";
import { connect } from "react-redux";
import { removeFromCart, placeOrder ,getWalletBalance} from "../store/actions/ProductAction";
import { bindActionCreators } from 'redux';
import { getCartTotal, getCartItemQuantity } from "../store/reducer/ProductReducer";
import Header from '../components/common/header';
import SplashScreen from 'react-native-splash-screen';
import Menu, { MenuItem, MenuDivider } from  'react-native-material-menu';
import * as RNLocalize from 'react-native-localize';
import { setI18nConfig } from './../utils/i18n';
import { translate } from './../utils/i18n';
import { StackActions, NavigationActions } from 'react-navigation';
import Spinner from 'react-native-loading-spinner-overlay';
class CheckOut extends Component {

  navigateToScreen = (route, parameters) => {
    const resetAction = StackActions.reset({
      index: 0,
      key: null,
      actions: [NavigationActions.navigate({ routeName: route, params: parameters })],
    });
    this.props.navigation.dispatch(resetAction);
  };

  generateNumber = (min, max) => {

    return Math.floor(Math.random() * (max - min + 1) + min)
  }

  constructor(props) {
    super(props);
    this.state = {
      selectedItemID:0,
      paymentmode: translate('checkout_select_payment'),
      refreshing:false
    }
    setI18nConfig();
  }
  componentWillMount() {
    SplashScreen.hide();
  }

  componentDidMount() {
    RNLocalize.addEventListener('change', this.handleLocalizationChange);
  }

  componentWillUnmount() {
    cancelRequest();
    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
  }

  clickEventListener() {
    Alert.alert("Success", "Product has beed added to cart")
  }
  // Place Order 

  onConfirmHandler() {
    if (this.props.totalQuantity < 1) {
      alert('Your shopping cart is empty');
      return
    }else if((this.props.total).toFixed(2) <15 )
    {
      alert('Minimum AED 15 order is accepted');
      return
    }else if(this.state.selectedItemID ===0){
      alert(translate('checkout_select_payment'));
      return  
    }
    const { reccuringPattern, shippingtimeIisOneTime, shippingtimeIisReccuring, shippingDate, customerid, shippingid,shippingtimetimeSlot } = this.props.navigation.state.params
    var cartProducts = JSON.parse(JSON.stringify(this.props.cart).replace(/\"quantity\":/g, "\"qty\":"));
    if (this.state.selectedItemID == 1) {
      // COD
      this.setState({
        refreshing:true
      })
     
      this.props.placeOrder({
        "uniqueorderid": this.generateNumber(0, 1000000),
        "customerid": customerid,
        "shippingid": shippingid,
        "tax": "5d563fe6ab95e07a445d2cb3",
        "deliverytype": "Free",
        "deliverycost": null,
        "orderstatus": "open",
        "paymentmode": "COD",
        "qty": this.props.totalQuantity,
        "paymentrefrence": "",
        "promoCode": "",
        "total": (this.props.total).toFixed(2),
        "shippingtimeIisOneTime": shippingtimeIisOneTime,
        "shippingtimeIisReccuring": shippingtimeIisReccuring,
        "shippingtimetimeSlot": shippingtimetimeSlot,
        "shippingDate": shippingDate,
        "products": cartProducts,
        "reccuringPattern":reccuringPattern
      }) .then((response) => {
        this.setState({  refreshing: false })
    }, (error)  => {
      this.setState({  refreshing: false })
    } )

     
      //  this.props.navigation.navigate('Congratulation', { 'total': (this.props.total * 0.05 + this.props.total)  })
    } else if (this.state.selectedItemID == 2) {
      //credit card

      this.props.navigation.navigate('PaymentScreen', { 'total': (this.props.total ) ,
      order: {
        "uniqueorderid": this.generateNumber(0, 1000000),
        "customerid": customerid,
        "shippingid": shippingid,
        "tax": "5d563fe6ab95e07a445d2cb3",
        "deliverytype": "Free",
        "deliverycost": null,
        "orderstatus": "open",
        "paymentmode": "COD",
        "qty": this.props.totalQuantity,
        "paymentrefrence": "",
        "promoCode": "",
        "total": (this.props.total).toFixed(2),
        "shippingtimeIisOneTime": shippingtimeIisOneTime,
        "shippingtimeIisReccuring": shippingtimeIisReccuring,
        "shippingtimetimeSlot": shippingtimetimeSlot,
        "shippingDate": shippingDate,
        "products": cartProducts,
        "reccuringPattern":reccuringPattern
      } 
    })
   //   this.props.navigation.navigate('PaymentScreen', { 'total': (this.props.total * 0.05 + this.props.total) })
    } else if (this.state.selectedItemID == 3) {
      // wallet 
      //TODO: check customer current wallet and accept or reject on runtime 
      this.setState({
        refreshing:true
      })
      this.props.getWalletBalance(customerid)
                .then((response) => {
                    console.log("Wallet Data Response", response)
              
                    let _response = response.data ||  0;
                    if (_response <(this.props.total).toFixed(2)  ) {
                       alert('your wallet balance is not enough for this transaction')
                       this.setState({  refreshing: false })
                       return
                    }
                  
                }, (error)  => {
                  alert('We can not process your order now')
      
                  this.setState({  refreshing: false })
                  return
                } )

                this.setState({
                  refreshing:true
                })
      this.props.placeOrder({
        "uniqueorderid": this.generateNumber(0, 1000000),
        "customerid": customerid,
        "shippingid": shippingid,
        "tax": "5d563fe6ab95e07a445d2cb3",
        "deliverytype": "Free",
        "deliverycost": null,
        "orderstatus": "open",
        "paymentmode": "wallet",
        "qty": this.props.totalQuantity,
        "paymentrefrence": "",
        "promoCode": "",
        "total": (this.props.total).toFixed(2),
        "shippingtimeIisOneTime": shippingtimeIisOneTime,
        "shippingtimeIisReccuring": shippingtimeIisReccuring,
        "shippingtimetimeSlot": shippingtimetimeSlot,
        "shippingDate": shippingDate,
        "products": cartProducts,
        "reccuringPattern" :reccuringPattern
      }) .then((response) => {
        this.setState({  refreshing: false })
    }, (error)  => {
      this.setState({  refreshing: false })
    } )

    //  this.navigateToScreen('Congratulation', { 'total': (this.props.total * 0.05 + this.props.total) })
      //  this.props.navigation.navigate('Congratulation', { 'totalwallet': (this.props.total * 0.05 + this.props.total)  })
    }

  }
  _menu = null;

  setMenuRef = ref => {
    this._menu = ref;
  };

  hideMenu = (id,name) => {
    this.setState({
      selectedItemID: id,
      paymentmode:name
    });
    this._menu.hide();
  };

  showMenu = () => {
    this._menu.show();
  };

  componentWillReceiveProps(nextProps) {

    if(this.props.status !== nextProps.status)
    {
    if( nextProps.status == 'SUCCESS' )
    {

      console.log('In Order Success ')
      if (this.state.selectedItemID == 3) {
      this.navigateToScreen('Congratulation',   { 'totalwallet': (this.props.total)  })
      }else {
        this.navigateToScreen('Congratulation', { 'total': (this.props.total) })
      }
      return null
    }else if(nextProps.status == 'FAILURE'){

      alert("We can't process your order now  ")
      return null
    }
   }

  }

  render() {
    return (






      <View style={{ flex: 1, backgroundColor: 'white' }} >
        <ScrollView>
        <Spinner
          visible={this.state.refreshing}
          textStyle={styles.spinnerTextStyle}
        />
          <View style={[{ flex: 1, flexDirection: 'column', marginTop: 20, marginHorizontal: 20 }]} >
            <Text style={styles.name}>{translate('checkout_title')}</Text>
            <View style={[styles.lineStyle, { color: '#F55130', marginTop: 5 }]}></View>
            <View style={{ flex: 1 }}>{this.renderFlatList()}</View>

            <View style={{ height: 40, width: '100%', flexDirection: 'row', marginTop: 20 }}>
              <Text style={{ textAlignVertical: 'center', width: '50%', color: COLORS.bgColor }}>{translate('checkout_promocode')}</Text>
              <View style={[styles.viewContainer]}>
                <TextInput />
              </View>
            </View>



            <View style={{ flexDirection: 'column', marginTop: 20, marginBottom: 20 }}>

              <View style={{ flexDirection: 'row', width: '100%', marginTop: 5 }}>
                <Text style={styles.rightText}>{translate('checkout_total_quantity')}</Text>
                <Text style={styles.leftText}>{this.props.totalQuantity.toFixed(2)}</Text>
              </View>

              <View style={{ flexDirection: 'row', width: '100%', marginTop: 5 }}>
                <Text style={styles.rightText}>{translate('checkout_total_price')}</Text>
                <Text style={styles.leftText}>{translate('common_aed')} {this.props.total.toFixed(2)}</Text>
              </View>

              <View style={{ flexDirection: 'row', width: '100%', marginTop: 5 }}>
                <Text style={styles.rightText}>{translate('checkout_delivery')}</Text>
                <Text style={styles.leftText}>{translate('checkout_delivery_free')}</Text>
              </View>
 

            </View>


            <View style={[styles.lineStyle, { color: '#F55130', marginTop: 10 }]}></View>

            <View style={{ flexDirection: 'row', width: '100%', marginTop: 10 }}>
              <Text style={[styles.rightText, { fontWeight: 'bold' }]}>{translate('checkout_total_price')}</Text>
              <Text ellipsizeMode="tail" numberOfLines={1} style={[styles.leftText, { fontWeight: 'bold' }]}>{I18nManager.isRTL ? ( this.props.total).toFixed(2) + '  ' + translate('common_aed') :
                translate('common_aed') + ' ' + (  this.props.total).toFixed(2)}</Text>
            </View>




            <View style={{ flex: 1 }}>
              <Menu
                ref={this.setMenuRef}
                style={{ flex: 1, width: '80%', alignItems: 'center', justifyContent: 'center' }}
                button={
                  <TouchableOpacity onPress={this.showMenu}>
                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 20, marginBottom: 100, borderRadius: 10, borderColor: 'black', padding: 10, borderWidth: 0.5, width: '100%' }}>
                      <Image
                        resizeMode='contain'
                        style={{ alignSelf: "center", height: 25, width: 25 }}
                        source={require('../assets/wl.png')}
                      />

                      <Text numberOfLines={1} ellipsizeMode='tail' style={{ marginLeft: 10, fontWeight: 'bold', textAlignVertical: 'center' }} >{ this.state.paymentmode }</Text>

                      <Icon
                        style={{ position: 'absolute', right: 10, alignSelf: 'center' }}
                        name='chevron-down'
                        size={25} color="#000"
                      ></Icon>
                    </View>
                  </TouchableOpacity>
                }
              >
                <MenuItem disabled={this.state.selectedItemID === 1} onPress={() => this.hideMenu(1 , translate('checkout_payment_cod'))}>{translate('checkout_payment_cod')}</MenuItem>
                <MenuItem disabled={this.state.selectedItemID === 2} onPress={() => this.hideMenu(2,translate('checkout_payment_credit_card'))}>{translate('checkout_payment_credit_card')}</MenuItem>
                <MenuItem disabled={this.state.selectedItemID === 3} onPress={() => this.hideMenu(3,translate('checkout_payment_wallet'))}>{translate('checkout_payment_wallet')}</MenuItem>

              </Menu>
            </View>





          </View>
        </ScrollView>


        <View style={[{ flex: 0, position: 'absolute', bottom: 0, width: '100%', backgroundColor: 'white' }]}>

          <View style={[{ flexDirection: 'row', marginTop: 5, marginBottom: 5, marginLeft: 20, marginRight: 20, justifyContent: 'space-between' }]}>
            <Text numberOfLines={1} ellipsizeMode='tail' style={{ fontWeight: "bold", fontSize: 15 }}>{translate('checkout_total_price')}</Text>
            <Text numberOfLines={1} ellipsizeMode='tail' style={{ fontWeight: "bold", fontSize: 15 }}>{I18nManager.isRTL ? (  this.props.total).toFixed(2) + '  ' + translate('common_aed') :
              translate('common_aed') + ' ' + ( this.props.total).toFixed(2)}</Text>
          </View>
          <TouchableOpacity style={[styles.btnConfirm, { paddingVertical: 15 }]} onPress={() =>
            this.onConfirmHandler()
          }>
            <Text style={[styles.textBold, { color: 'white', fontSize: 15 }]}>{translate('checkout_payment_confirm')}</Text>
          </TouchableOpacity>
        </View>




      </View>
    );
  }

  renderFlatList() {
    return (


      <FlatList
        data={this.props.cart}
        keyExtractor={item => item._id.toString()}
        renderItem={
          ({ item }) =>


            <View style={{ flexDirection: 'column' }}>

              <View style={[styles.cardItemContainer]}>
                <View style={[styles.itemLeftContainer]}>
                  <Text numberOfLines={2} ellipsizeMode='tail' style={[styles.textBold]} >{item.productname}</Text>
                </View>
                <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.textblack, { height: '100%', alignSelf: 'center', fontWeight: 'bold' }]}>{item.quantity}</Text>

                <View style={styles.itemMiddleContainer}>
                  <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.textBold, { fontSize: 7, marginTop: 5, right: 5 }]}>AED</Text>
                  <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.textBold]}>{item.price}</Text>
                </View>
                <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.textblack, { height: '100%', right: 0, fontWeight: 'bold', color: COLORS.bgColor }]}>{(item.price * item.quantity).toFixed(2)}</Text>
                <TouchableOpacity onPress={() => { this.props.removeFromCart(item._id) }}>
                  <Image
                    resizeMode='contain'
                    style={{ alignSelf: "center", height: 20 }}
                    source={require('../assets/bin.png')}
                  />
                </TouchableOpacity>
              </View>

              <View style={[styles.lineStyle, { color: '#F55130' }]}></View>
            </View>

        }
      />



    );
  }

}

const styles = StyleSheet.create({

  rightText: {
    width: I18nManager.isRTL ? '60%' : '50%'
  },
  leftText: {

    width: I18nManager.isRTL ? '40%' : '50%',
    textAlign: 'left'
  },
  cardItemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
    width: '100%',
    marginTop: 12,
    marginBottom: 12

  },
  itemLeftContainer: {
    flex: 0,
    height: '100%',
    flexDirection: 'column',
    alignSelf: 'center',
    width: '45%'

  }, itemMiddleContainer: {
    flexDirection: 'row',
    height: '100%',
    justifyContent: 'center',
    fontWeight: 'bold'
  }, textBold: {
    fontWeight: 'bold',
    fontSize: 12
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
    marginTop: 30,
  },
  viewContainer: {
    borderWidth: 0.5,
    borderColor: COLORS.bgColor,
    borderRadius: 15,
    width: '50%',
  },
  productImg: {
    width: 200,
    height: 200,
  },
  name: {
    fontSize: 20,
    color: "black",
    alignSelf: 'flex-start'

  },
  lineStyle: {
    borderWidth: 0.35,
    borderColor: '#f55130',
    marginTop: 5,
  },

  orderHeading: {

    fontSize: 13,
    color: "green",
    fontWeight: 'bold',
    alignSelf: 'flex-start'
  },
  textblack: {
    fontSize: 12,
    color: "black",
  },
  greytext: {

    fontSize: 13,
    color: "grey",
    alignSelf: 'flex-start'
  },
  boldtext: {
    fontSize: 18,
    color: "white",
    textAlign: 'center',
    textAlignVertical: 'center',
    fontWeight: 'bold',
    height: '100%',
    width: '100%'

  },
  btnNew: {
    paddingVertical: 7,
    width: 70,
    borderRadius: 20,
    marginHorizontal: 3,
    borderColor: '#f55130',
    borderWidth: 0.5,
    alignSelf: 'center',
    position: "absolute",
    right: 10
  },
  btnSquareleft: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    backgroundColor: '#74D5F7'
  },
  btnConfirm: {

    alignItems: 'center',
    width: '100%',
    backgroundColor: "#F55130"
  },
  textNew: {
    textAlign: 'center',
    fontSize: 12,
    color: '#f55130',
  },
  boldgreen: {

    fontSize: 12,
    color: '#f55130',
    fontWeight: 'bold',

  },
  separatorVertical: {
    width: 1,
    backgroundColor: "#f55130",


  },

  star: {
    width: 40,
    height: 40,
  },
  btnColor: {
    height: 30,
    width: 30,
    borderRadius: 30,
    marginHorizontal: 3
  },
  btnSize: {
    height: 40,
    width: 40,
    borderRadius: 40,
    borderColor: '#778899',
    borderWidth: 1,
    marginHorizontal: 3,
    backgroundColor: 'white',

    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  starContainer: {
    justifyContent: 'center',
    marginHorizontal: 30,
    flexDirection: 'row',
    marginTop: 20
  },
  contentColors: {
    justifyContent: 'center',
    marginHorizontal: 30,
    flexDirection: 'row',
    marginTop: 20
  },
  contentSize: {
    justifyContent: 'center',
    marginHorizontal: 20,
    flexDirection: 'row',
    marginTop: 20
  },
  separator: {
    height: 1,
    backgroundColor: "#f55130",
    marginTop: 20,
    marginHorizontal: 30
  },
  shareButton: {
    marginTop: 10,
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    backgroundColor: "#00BFFF",
  },
  shareButtonText: {
    color: "#FFFFFF",
    fontSize: 20,
  }, buttonContainer: {

    flexDirection: 'row',
    backgroundColor: '#f05424',
    marginBottom: 10,
    marginLeft: 20,
    marginRight: 20,
    height: 45,
    borderRadius: 30,
    marginTop: 30


  },
  loginText: {
    flex: 1,
    color: 'white',
    fontSize: 13,
    textAlign: 'center',
    alignSelf: 'center',
    fontWeight: 'bold'

  },
});

const mapStateToProps = ({ products }) => {
  // RNTestLibModule.show("Hello World");
  return { orderData :products.orderData ,status :products.status  ,  orderLoading : products.orderLoading , cart: products.cart, error: products.error, loading: products.loading, total: getCartTotal(products), totalQuantity: getCartItemQuantity(products) };
}


function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({ removeFromCart, placeOrder ,getWalletBalance }, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CheckOut);
