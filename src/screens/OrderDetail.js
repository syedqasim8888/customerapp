import React, { Component } from 'react';
import {
  I18nManager,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Alert,
  ScrollView,
  FlatList,
} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import * as RNLocalize from 'react-native-localize';
import { setI18nConfig } from './../utils/i18n';
import { translate } from './../utils/i18n';
import { setBaseURL, cancelRequest, setCustomHeaders } from "../apis";



export default class OrderDetails extends Component {

  
  constructor(props) {
    super(props);
    setI18nConfig();
  }

  componentWillMount() {
    SplashScreen.hide();
  }

  componentDidMount() {
    RNLocalize.addEventListener('change', this.handleLocalizationChange);
  }

  componentWillUnmount() {
    cancelRequest();
    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
  }

  clickEventListener() {
    Alert.alert("Success", "Product has beed added to cart")
  }

  render() {

    const  item = this.props.navigation.state.params.item.orderid;  
    console.log('asda',item);
    

    return (

      <View style={[{ flex: 1 }]} >
        <ScrollView   >

          <View style={{ justifyContent: 'flex-start' }}  >

            <View style={styles.viewContainer}>

              <View style={{ flexDirection: 'column' }} >
                <Text style={styles.orderHeading}>{translate('order_detail_order_no')} #{this.props.navigation.state.params.item.orderNumber}</Text>
                <Text style={[styles.textblack, { marginTop: -3 }]}>{translate('order_detail_order_date')}:{this.props.navigation.state.params.item.orderDate}</Text>
              </View>
              <TouchableOpacity style={[styles.btnNew]}>
                <Text style={styles.textNew}>{translate('order_detail_new')}</Text>
              </TouchableOpacity>
            </View>
            <View style={[styles.viewContainer, { flexDirection: 'column' }]} >
              {this.renderFlatList()}
              <View style={[{ flexDirection: 'row', flex: 1, width: '100%', marginTop: 10, marginBottom: 20 }]}>
                <Text style={[styles.boldtext, { flex: 1, position: 'absolute', left: 0, marginLeft: 0 }]}>{translate('order_detail_total')}</Text>
                <Text style={[styles.boldtext, { position: 'absolute', right: 0, marginLeft: 0 }]}> {I18nManager.isRTL ? '110' + ' ' + translate('common_aed') : translate('common_aed') + ' '}{this.props.navigation.state.params.item.totalPrice} </Text>
              </View>

            </View>
            <View style={styles.viewContainer}>

              <View style={{ flexDirection: 'column' }} >
                <Text style={[styles.textblack, { marginTop: -2 }]}>{this.props.navigation.state.params.item.addressFull}</Text>
              </View>

            </View>
            
            {/* <TouchableOpacity style={[styles.buttonContainer]}  >
              <Text style={[styles.loginText]}>{translate('order_detail_order_again')}</Text>
            </TouchableOpacity> */}



          </View>
        </ScrollView>

      </View>
    );
  }

  renderFlatList() {
    return (


      <FlatList
        data={this.props.navigation.state.params.item.products}
        renderItem={
          ({ item }) =>

            <View style={[{ flexDirection: 'row', flex: 1, width: '100%', marginBottom: 4 }]}>
              <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.textblack, { flex: 0.40 }]}>{item.name}</Text>
              <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.textblack, { flex: 0.10 }]}>{item.qty}</Text>

              <View style={{ flexDirection: 'row', flex: 0.10 }}>
                <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.textblack, { fontSize: 7, marginTop: 5 }]}>AED</Text>
                <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.textblack]}>{item.unitPrice}</Text>

              </View>

              <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.textblack, { flex: 0.40, alignSelf: 'flex-end', position: 'absolute', right: 0 }]}>AED {item.subTotal}</Text>

            </View>

        }
      />

    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    marginTop: 30,
  },



  viewContainer: {
    flexDirection: 'row',
    
    borderWidth: 0.5,
    borderColor: '#f55130',
    marginHorizontal: 20,
    borderRadius: 5,
    marginTop: 20,
    padding: 10
  },
     countryPickerContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#ecf0f1',
      padding:15,
    },
  orderHeading: {

    fontSize: 13,
    color: "green",
    fontWeight: 'bold',
    alignSelf: 'flex-start'
  },
  textblack: {

    textAlign: 'left' ,
    fontSize: 12,
    color: "black",
  },

  boldtext: {

    fontSize: 13,
    color: "black",
    alignSelf: 'center',
    justifyContent: 'center',
    marginLeft: 10,
    fontWeight: 'bold'

  },
  btnNew: {
    paddingVertical: 7,
    width: 70,
    borderRadius: 20,
    marginHorizontal: 3,
    borderColor: '#f55130',
    borderWidth: 0.5,
    alignSelf: 'center',
    position: "absolute",
    right: 10






  },


  textNew: {
    textAlign: 'center',
    fontSize: 12,
    color: '#f55130',
  },
  boldgreen: {

    fontSize: 12,
    color: '#f55130',
    fontWeight: 'bold',

  },

  bottom: {

    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',


  },




  buttonContainer: {

    flexDirection: 'row',
    backgroundColor: '#f05424',
    marginBottom: 10,
    marginLeft: 20,
    marginRight: 20,
    height: 45,
    borderRadius: 30,
    marginTop: 30


  },
  loginText: {
    flex: 1,
    color: 'white',
    fontSize: 13,
    textAlign: 'center',
    alignSelf: 'center',
    fontWeight: 'bold'

  },
});     