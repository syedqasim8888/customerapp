import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import Header from '../components/common/header';
import CommonStyles from '../stylehelper/StylesCommon';

import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    I18nManager,
    Alert,
} from 'react-native';
import COLORS from '../stylehelper/Color';
import SplashScreen from 'react-native-splash-screen';
import * as RNLocalize from 'react-native-localize';
import { setI18nConfig } from './../utils/i18n';
import { translate } from './../utils/i18n';
import { setBaseURL, cancelRequest, setCustomHeaders } from "../apis";
import { getWalletBalance } from '../store/actions/WalletAction';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import AsyncStorage from '@react-native-community/async-storage';

 class Wallet extends Component {

    constructor(props) {
        super(props);
        setI18nConfig();
        this.state = { 
            name: 0,
            email: 0,
            _id: 0,
        };
    
 
    }
    
    retriveUserDetails = async (key) => {
        try {
            const retrievedItem = await AsyncStorage.getItem('token');
            const object = await AsyncStorage.getItem(key);
            console.log('_idn in home', JSON.parse(object)._id)
            this.setState({
                name: JSON.parse(object).name,
                _id: JSON.parse(object)._id,
                email: JSON.parse(object).email
            });
            console.log('Name: ' + this.state.name + ' CustomerID: ' + this.state._id + " Email: " + this.state.email)
            setCustomHeaders([
                {
                    name: "Authorization",
                    value: 'Bearer ' + retrievedItem,
                }
            ]);
            const { getWalletBalance } = this.props;
            getWalletBalance( this.state._id);
            //todo add customer id here
            //fetchShippingAddress(this.state._id);
            return retrievedItem;
        } catch (error) {
            console.log(error);
        }
        return
    }


    componentWillMount() {
        SplashScreen.hide();
        setBaseURL("http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1")
        this.retriveUserDetails('userobject');  
        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
                this.retriveUserDetails('userobject');  
            }
          );
      }
    
      componentDidMount() {
        RNLocalize.addEventListener('change', this.handleLocalizationChange);
      }
    
      componentWillUnmount() {
        cancelRequest();
        RNLocalize.removeEventListener('change', this.handleLocalizationChange);
        this.willFocusSubscription.remove();
      }
    

    clickEventListener() {
        Alert.alert("Success", "Product has beed added to cart")
    }
    

    render() {
        const balance = this.props.amount >0 ?this.props.amount:''
        return (

            <View style={[styles.mainContainer]} >
                <Header  marginHorizontal = {20} headerText={translate('wallet_title')} ></Header>

                <View style={[CommonStyles.roundedCornerSquareOrange, { marginLeft: 20, marginRight: 20, marginTop: 20, flexDirection: 'column', flex: 0  }]}>
                 
                    <View style={{  height :150,   justifyContent: 'center',   }}>
                        <Text style={[CommonStyles.textLightGrey, { alignSelf: 'center' }]} >{translate('wallet_balance')}</Text>
                        <Text style={[CommonStyles.boldTextLarge, { alignSelf: 'center' }]} >{ I18nManager.isRTL ?balance + ' ' +translate('common_aed')  : translate('common_aed') + ' ' +  balance} </Text>
                    </View>
                    <View style={{ width: '100%',  flex: 0,  }} >
                        <View style={[CommonStyles.lineStyle]}></View>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('AddMoney' , { balance: balance })}>
                            <View style={[styles.itemContainer]}>
                                <Image
                                    resizeMode='contain'
                                    tintColor={COLORS.Orange}
                                    style={styles.itemImage}
                                    source={require('../assets/wallet.png')}
                                />
                                <Text style={[styles.itemText]} >{translate('wallet_add_money')}</Text>


                                <Icon
                                    name={'ios-arrow-forward'}
                                    size={15}
                                    color={COLORS.Orange}
                                    style={[styles.itemIcon,{ transform: [{ rotate:    I18nManager.isRTL ?  "180deg" : "0deg"}]} ]}
                                />
                            </View>
                        </TouchableOpacity>
                        <View style={[CommonStyles.lineStyle]}></View>
                        <TouchableOpacity   onPress={() => this.props.navigation.navigate('WalletTransaction')}>
                            <View style={[styles.itemContainer]}>
                                <Image
                                    resizeMode='contain'
                                    tintColor={COLORS.Orange}
                                    style={styles.itemImage}
                                    source={require('../assets/swaparrow.png')}
                                />
                                <Text style={[styles.itemText]} >{translate('wallet_transactions')}</Text>


                                <Icon
                                    name={'ios-arrow-forward'}
                                    size={15}
                                    color={COLORS.Orange}
                                    style={[styles.itemIcon,{  transform: [{ rotate:    I18nManager.isRTL ?  "180deg" : "0deg"}]} ]}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                </View >
            </View >

        );
    }

}

const styles = StyleSheet.create({

    mainContainer: {
        flex: 1,
        flexDirection: 'column'
    }, itemContainer: {
        flexDirection: 'row',
        width: '100%',
        flex: 0,
        marginBottom: 5,
        marginTop: 5
    }, itemImage: {
        height: 25,
        width: 25,
        alignSelf: 'flex-start',
        padding: 5,
        marginLeft: 15
    }, itemText: {
        fontSize: 12,
        color: COLORS.Orange,
        alignSelf: 'center',
        marginLeft: 15
    }, itemIcon: {

        padding: 5,
        position: 'absolute',
        right: 0,
        marginRight: 15
    },
});     

const mapStateToProps = ({ wallet }) => {
    
    // RNTestLibModule.show("Hello World");
    return { loading:wallet.loading , amount:wallet.amount};
  }
  
  
  function mapDispatchToProps(dispatch) {
    return {
      ...bindActionCreators({getWalletBalance }, dispatch)
    }
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(Wallet);