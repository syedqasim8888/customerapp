
import React, { Component } from 'react';
import { removeFromCart,  increaseQuantity  ,decreaseQuantity} from "../store/actions/ProductAction";
import {  getCartTotal } from "../store/reducer/ProductReducer";
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import {cancelRequest} from '../apis';
import SplashScreen from 'react-native-splash-screen';
import { I18nManager, StyleSheet, Text,View, TouchableOpacity, Image, Alert, Dimensions, FlatList, ScrollView,}
    from 'react-native';
    
import   * as RNLocalize  from 'react-native-localize';
import    {setI18nConfig} from './../utils/i18n';
import      {translate } from './../utils/i18n';
import   translationGetters from './../utils/i18n';

 
const width = Dimensions.get('window').width;
  class Cart extends Component {

    componentWillMount() {
        SplashScreen.hide();
      }
    
    constructor(props) {
        super(props);
        this.renderItem = this.renderItem.bind(this);
        setI18nConfig();
    }



    componentDidMount() {
        RNLocalize.addEventListener('change', this.handleLocalizationChange);
      }
    
      componentWillUnmount() {
        cancelRequest();
        RNLocalize.removeEventListener('change', this.handleLocalizationChange);
      }
    renderItem({ item }, props) {
        return (
            <View>
                <View    style={[styles.itemContainer, {}]}>
                    <Image
                        style={[styles.itemImage]}
                        resizeMode={'contain'}
                        source={  {uri:item.productimage}}
                    />
                    <View style={[styles.itemMiddleContainer]} >
                        <Text style={[styles.text, { fontWeight: 'bold',width:'70%' }]}>{item.productname}</Text>
                        <Text style={[styles.text, { fontWeight: 'bold', includeFontPadding: false }]}>{item.productDescription}</Text>
                        <Text style={[styles.text, { fontWeight: 'bold', position: 'absolute', bottom: 0, left: 0, includeFontPadding: false }]}>AED {item.price}</Text>
                    </View>

                    <View style={[styles.itemLastContaner]} >
                        <TouchableOpacity onPress={()=>{ this.props.removeFromCart(item._id)}} >
                            <Image
                                style={{ alignSelf: 'center', width: 20, height: 20, marginTop: 10 }}
                                resizeMode='contain'
                                source={require('../assets/bin.png')}
                            />
                        </TouchableOpacity>
                        <View style={styles.quantityContainer} >
                            <TouchableOpacity  onPress={()=>{this.props.decreaseQuantity(item._id ,item.quantity)}} >
                                <Text style={[styles.textIncrement]}>-</Text>
                            </TouchableOpacity>
                            <Text style={[styles.text, { alignSelf: 'center', }]}>{item.quantity}</Text>
                            <TouchableOpacity   onPress={()=>{this.props.increaseQuantity(item._id ,item.quantity)}}>
                                <Text style={[styles.textIncrement]}>+</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>


                <View style={[styles.lineStyle, { marginTop: 20 }]}>
                </View>
            </View>);

    }
    clickEventListener() {
        Alert.alert("Success", "Product has beed added to cart")
    }

    render() {
        return (

            <View style={[styles.mainContaner]} >

                <ScrollView>
                    <View style={[styles.flatListContainer]}  >
                        <Text style={styles.name}>{translate('cart_title')}</Text>
                        <View style={[styles.lineStyle, { marginTop: 5 }]} />
                        <FlatList
                            data={this.props.cart}
                            renderItem={(item) => this.renderItem(item, this.props)}
                            keyExtractor={item => item._id.toString()}
                        />
                    </View>
                </ScrollView>

                <View style={[{ flexDirection: 'column', flex: 0, width: '100%', bottom: 0 }]}>
                    <View style={[{ flexDirection: 'row', width: '100%' }]}>
                        <Text style={{ left: 20, textAlign: 'left', width: '50%', fontWeight: "bold", fontSize: 0.04 * width }}>{translate('cart_total')}</Text>
                        <Text style={{ right: 20, textAlign: 'right', width: '50%', fontWeight: "bold", fontSize: 0.04 * width }}>{I18nManager.isRTL ?    this.props.total +' ' +translate('common_aed') :translate('common_aed')+' '+  parseFloat(this.props.total).toFixed(2) } </Text>
                    </View>
                    <View style={styles.bottom}>
                        <TouchableOpacity style={[styles.bottomButton, { backgroundColor: '#74D5F7' }]} onPress={() => this.props.navigation.navigate('HomeScreen')}>
                            <Text style={[styles.boldtext]}>{translate('cart_continue_shopping')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.bottomButton, { backgroundColor: "#F55130" }]} onPress={() => this.props.navigation.navigate('ShippingAddress')}>
                            <Text style={[styles.boldtext]}>{translate('cart_checkout')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>


            </View>

        );



    }
}

const styles = StyleSheet.create({

    mainContaner: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white'
    }, flatListContainer: {
        flex: 1,
        flexDirection: 'column',
        marginHorizontal: 20,
        marginTop: 20
    },
    itemContainer: {
        flexDirection: 'row',
        flex: 1,
        marginTop: 10,
        marginBottom: 10
    }, itemImage: {
        width: 100,
        height: 100,
        marginTop:5,

        alignSelf: "center",

    }, itemMiddleContainer: {
        flexDirection: 'column',
        flex: 1,
        height: '100%',
        marginTop: 5,
        marginBottom: 5
    }, itemLastContaner: {
        flexDirection: 'column',
        flex: 1,
        position: 'absolute',
        height: '100%',
        right: 0,
        justifyContent: 'space-between',
        marginBottom: 10,
        marginTop: 5,
    }, textIncrement: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#F55130',
        alignSelf: 'center',
        padding: 5
    }, quantityContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        borderRadius: 5,
        borderColor: '#f55130',
        borderWidth: 0.5,
        width: 100
    },

    flcontainer: {
        flex: 1,
        paddingTop: 22,
        backgroundColor: '#fff',
    },

    bottomButton: {
        flex: 0.5,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15,
    },

    bottom: {
        flex: 0,
        flexDirection: 'row',
        marginTop: 10
    },
    item: {
    },
    container: {
        flex: 1,
        marginTop: 20,
    },
    productImg: {
        width: 200,
        height: 200,
    },
    name: {
        fontSize: 20,
        color: "black",
        textAlign: 'right',
        alignSelf: 'flex-start',

    },
    lineStyle: {
        borderWidth: 0.5,
        borderColor: '#f55130',
        marginTop: 5
    },

    text: {
        fontSize: 0.03 * width,
        color: "black",
    },
    greytext: {
        marginTop: -3,
        fontSize: 13,
        color: "grey",
        alignSelf: 'flex-start'
    },
    boldtext: {
        fontSize: 0.04 * width,
        color: "white",
        alignSelf: 'center',
        justifyContent: 'center',

        fontWeight: 'bold'
    },

});     
const mapStateToProps = ({ products, loading }) => {
 
    return { cart: products.cart, error: products.error, loading: products.loading ,productCount:products.productCount ,total:getCartTotal(products) };
  }
  
  
  function mapDispatchToProps(dispatch) {
    return {
      ...bindActionCreators({ removeFromCart,increaseQuantity  ,decreaseQuantity}, dispatch)
    }
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(Cart);
  