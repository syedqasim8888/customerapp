import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Feather';
import { BackHandler, TouchableOpacity } from "react-native";
import { setBaseURL, cancelRequest, setCustomHeaders } from "../apis";
import { ContactUs } from "../store/actions/ContactUsAction";
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';

import {
    Linking,
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableHighlight,
    Image,
    ScrollView,
    I18nManager,
} from 'react-native';
import COLORS from '../stylehelper/Color';
import { translate } from '../utils/i18n';
import * as RNLocalize from 'react-native-localize';
import { setI18nConfig } from './../utils/i18n';
import SplashScreen from 'react-native-splash-screen';
import Spinner from 'react-native-loading-spinner-overlay';
import { ToastAndroid } from 'react-native';


class Contactus2 extends Component {


    constructor(props) {
        super(props);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        setI18nConfig();
        this.state = {
            name: "",
            email: "",
            phonenumber: "",
            message: "",
            customerid: "",
        };
    }

    componentWillMount() {
        SplashScreen.hide();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', this.handleLocalizationChange);
    }


    componentWillUnmount() {
        cancelRequest();
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        RNLocalize.removeEventListener('change', this.handleLocalizationChange);
    }

    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;
    }

    componentWillReceiveProps(newProps) {
       
        if (newProps.success && !newProps.err) {
         ToastAndroid.show('your suggestion/problem has been submitted', ToastAndroid.LONG);
        }
        else if (!newProps.success && newProps.err) {
          ToastAndroid.show('Some thing went wrong please try again', ToastAndroid.LONG);
        }
}


    validateAndsubmit = () => {

        if (this.state.name != '') {

            //Handler for the Submit onPress
            if (this.state.email != '') {
                //Check for the Name TextInput

                const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

                if (reg.test(this.state.email) === true) {

                    if (this.state.phonenumber != '') {
                        //Check for the Email TextInput

                        if (this.state.message != '') {

                            this.props.ContactUs({
                                name: this.state.name ,
                                email: this.state.email,
                                phonenumber: this.state.phonenumber,
                                message: this.state.message,
                                customerid: "123"
                            });

                        } else {
                            ToastAndroid.show('please enter message', ToastAndroid.SHORT);
                        }

                    } else {

                        ToastAndroid.show('please enter phoneno', ToastAndroid.SHORT);
                    }
                }
                else {
                    ToastAndroid.show('email is not valid', ToastAndroid.SHORT);
                }
            } else {
                ToastAndroid.show('please enter email', ToastAndroid.SHORT);
            }
        } else {
            ToastAndroid.show('please enter name', ToastAndroid.SHORT);
        }
    };


    render() {
        return (
            <ScrollView style={{backgroundColor:COLORS.white}}>
                <View style={styles.mainContainer}>

                    <Spinner
                        visible={this.props.loading}
                        textContent={'Loading...'}
                        textStyle={styles.spinnerTextStyle}
                    />


                    <TouchableOpacity onPress={this.handleBackButtonClick}>
                        <View style={styles.backIconContainer}>
                            <Icon name="arrow-left" size={30} color={COLORS.Orange} />
                            <Text numberOfLines={1} ellipsizeMode='tail' style={styles.textBackIcon}>Back</Text>
                        </View>
                    </TouchableOpacity>
                    <TextInput
                        placeholder={translate('contact_us_2_name')}
                        underlineColorAndroid='transparent'
                        multiline={true}
                        style={[styles.TextInputStyleClass, { marginTop: 30, paddingTop:10, paddingBottom:10 }]}
                        onChangeText={(name) => this.setState({ name })} />

                    <TextInput
                        placeholder={translate('contact_us_2_email')}
                        underlineColorAndroid='transparent'
                        multiline={true}
                        style={[styles.TextInputStyleClass,{paddingTop:10, paddingBottom:10}]}
                        onChangeText={(email) => this.setState({ email })} />

                    <TextInput
                        placeholder={translate('contact_us_2_mobile_no')}
                        underlineColorAndroid='transparent'
                        multiline={true}
                        style={[styles.TextInputStyleClass,{paddingTop:10, paddingBottom:10}]}
                        keyboardType='numeric'
                        onChangeText={(phonenumber) => this.setState({ phonenumber })} />

                    <TextInput
                        placeholder={translate('contact_us_2_suggestions')}
                        underlineColorAndroid='transparent'
                        multiline={true}
                        style={[styles.TextInputStyleClass, { height: 200 ,paddingTop:10, paddingBottom:10 }]}
                        onChangeText={(message) => this.setState({ message })} />

                    <TouchableHighlight >
                        <Text
                            style={[styles.ButtonStyleClass,
                            { marginTop: 30, textAlignVertical: 'center', textAlign: 'center', color: COLORS.white, fontWeight: 'bold' }]}
                            onPress={() => { this.validateAndsubmit() }}
                        >{translate('contact_us_2_submit')}</Text>
                    </TouchableHighlight>


                    <TouchableOpacity onPress={() => { Linking.openURL('tel:+971600555113'); }}
                        style={[styles.squareContainer, { marginBottom: 15 }]}>
                        <Image
                            style={{ flex: 0.15, resizeMode: 'contain', alignSelf: "center", height: 25 }}
                            source={require('../assets/ic_phone.png')}
                        />

                        <Text numberOfLines={1}
                            ellipsizeMode='tail'
                            style={[styles.textCallUs, { marginLeft: 12 }]} >{translate('contact_us_2_call_us')}</Text>

                    </TouchableOpacity>

                </View>
            </ScrollView>
        );
    }
}



const styles = StyleSheet.create({

    mainContainer: {
        backgroundColor: COLORS.white,
        flex: 1,
        flexDirection: 'column'
    },
    backIconContainer: {
        flexDirection: 'row',
        marginHorizontal: 20,
        marginTop: 20
    }, textBackIcon: {
        color: COLORS.Orange,
        fontSize: 18,
        textAlignVertical: 'center',
        marginLeft: 5
    }, squareContainer: {
        marginHorizontal: 20,
        flexDirection: 'row',
        marginTop: 40,
        borderRadius: 10,
        borderColor: 'black',
        padding: 15,
        borderWidth: 0.5
    }, textCallUs: {
        flex: 0.85,
        fontWeight: 'bold',
        textAlignVertical: 'center'
    },
    TextInputStyleClass: {
        textAlign: 'left',
        borderWidth: 0.5,
        borderColor: COLORS.black,
        borderRadius: 10,
        backgroundColor: "#FFFFFF",
        marginHorizontal: 20,
        marginTop: 10,
        textAlignVertical: 'top',
        paddingHorizontal: 20
    },
    ButtonStyleClass: {
        height: 50,
        borderRadius: 20,
        backgroundColor: COLORS.Orange,
        marginHorizontal: 20,
        marginTop: 10,
        marginBottom: 5,
        textAlign: 'left',
        padding: 10

    },
    spinnerTextStyle: {
        color: '#FFF'
      },
});


const mapStateToProps = ({ contactus }) => {

      console.log("matchstateContactusmessage: " + contactus.contactussuccess);


    return {
        data: contactus.contactusdata,
        loading: contactus.loading,
        err: contactus.err,
        success:contactus.contactussuccess

    };
};

function mapDispatchToProps(dispatch) {
    return {
        ...bindActionCreators({ ContactUs }, dispatch)
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps)
    (Contactus2);