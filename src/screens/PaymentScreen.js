import React, { Component } from 'react';
//import react in our code. 
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { WebView } from "react-native-webview";
import { getPaymentURL, getTELRResponse, placeOrder, rechargeWallet, getPaymentUrlfromAPI } from "../store/actions/PaymentAction";
import SplashScreen from 'react-native-splash-screen';
import { connect } from "react-redux";
import { setBaseURL, cancelRequest, setCustomHeaders } from "../apis";

import { bindActionCreators } from 'redux';
import { StackActions, NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';

const queryString = require('query-string');
//import all the components we are going to use. 

/*  TODO: send shipping address details to telr

    bill_fname
    bill_sname
    bill_addr1
    bill_city
    bill_country
    bill_email

    bill_custref -> For recurring Billing (STORED CARDS )
*/


class PaymentScreen extends Component {

  navigateToScreen = route => () => {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: route })],
    });

    this.props.navigation.dispatch(resetAction);
  };

  retriveUserDetails = async (key) => {
    try {
      const retrievedItem = await AsyncStorage.getItem('token');
      const object = await AsyncStorage.getItem(key);
      console.log('Token in home', retrievedItem)
      this.setState({
        name: JSON.parse(object).name,
        _id: JSON.parse(object)._id,
        email: JSON.parse(object).email
      });
      console.log('Name: ' + this.state.name + ' CustomerID: ' + this.state._id + " Email: " + this.state.email)
      setCustomHeaders([
        {
          name: "Authorization",
          value: 'Bearer ' + retrievedItem,
        }
      ]);
      this.getTelrResponsefromAPI()
      //todo add customer id here
      //fetchShippingAddress(this.state._id);
      return retrievedItem;
    } catch (error) {
      console.log(error);
    }
    return
  }


  _onNavigationStateChange(webViewState) {
    console.log(this.props.data.ref)
    if ("http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1/successTelrRespose" === webViewState.url) {
      console.log("REF ", this.props.data.ref)
      setBaseURL("http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1");
      if (this.props.navigation.state.params.walletamount > 0) {

        //   this.navigateToScreen('Thankyou' ,{'walletamount':this.props.navigation.state.params.walletamount})
        //   this.props.navigation.navigate ( 'Thankyou' ,{'walletamount':this.props.navigation.state.params.walletamount})
        // getTELRResponse 
        this.props.getTELRResponse({ "typeOfTransaction": "wallet", "telrRefrance": this.props.data.ref, "customerid": this.state.customerid })
      } else {
        this.props.getTELRResponse({ "typeOfTransaction": "order", "telrRefrance": this.props.data.ref, "customerid": this.state.customerid })
      }

      //this.navigateToScreen('Congratulation', { 'bc': '11' })
      //this.props.navigation.navigate ( 'Congratulation',{'bc':'11'})

    } else if ("http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1/failTelrRespose" === webViewState.url) {
      setBaseURL("http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1");
      if (this.props.navigation.state.params.walletamount > 0) {
        this.props.getTELRResponse({ "typeOfTransaction": "wallet", "telrRefrance": this.props.data.ref, "customerid": this.state.customerid })
      } else {
        this.props.getTELRResponse({ "typeOfTransaction": "order", "telrRefrance": this.props.data.ref, "customerid": this.state.customerid })
      }

    } else if ("http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1/telrReturnRespose" === webViewState.url) {
      setBaseURL("http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1");
      if (this.props.navigation.state.params.walletamount > 0) {
        this.props.getTELRResponse({ "typeOfTransaction": "wallet", "telrRefrance": this.props.data.ref, "customerid": this.state.customerid })
      } else {
        this.props.getTELRResponse({ "typeOfTransaction": "order", "telrRefrance": this.props.data.ref, "customerid": this.state.customerid })
      }
    }
    console.log(webViewState.url)
  }

  componentWillReceiveProps(newProps) {

    // if(newProps)
    if (newProps.telrmessage !== this.props.telrmessage) {

      if (newProps.telrmessage == "success") {
        if (this.props.navigation.state.params.walletamount > 0) {

          // recharge api   
          console.log('In Success Wallet : ', newProps.telrdata)

          this.props.rechargeWallet({
            "customerid": this.state._id,
            "telrTransactionId": this.props.data.ref,
            "totalAmountTopUp": parseFloat(this.props.navigation.state.params.walletamount),
            "promoCode": "",
            "entrytype": "in"
          })
          // "totalAmountTopUp": this.props.navigation.state.params.walletamount,  
          //    this.props.navigation.navigate ( 'Thankyou', {'walletamount':this.props.navigation.state.params.walletamount ,"updatedAmount":0} )
        } else {
          // 

          this.props.navigation.state.params.order.paymentrefrence = this.props.data.ref
          this.props.navigation.state.params.order.paymentmode = "card"
          this.props.placeOrder(this.props.navigation.state.params.order)
          console.log('In Success ORDER : ', newProps.telrdata)
        }

      } else if (newProps.telrmessage == "failure") {
        if (this.props.navigation.state.params.walletamount > 0) {
          console.log('In Failure Wallet : ', newProps.telrmessage)
          alert('' + newProps.error)
          this.props.navigation.navigate('Thankyou', { 'success': 'failure', 'message': newProps.error })


        } else {
          console.log('In Failure ORDER : ', newProps.telrmessage)
          alert('' + newProps.error)
          this.props.navigation.navigate('Congratulation', { 'success': 'failure', 'message': newProps.error })
        }

      }
    }
    if (newProps.ordermessage !== this.props.ordermessage) {
      if (newProps.ordermessage == "success") {
        this.props.navigation.navigate('Congratulation', { 'success': 'success' })
      } else if (newProps.ordermessage == "failure") {
        alert('Order is not processed')
        this.props.navigation.navigate('Congratulation', { 'success': 'failure', 'message': 'Order Not processed this time' })
      }
    }
    if (newProps.walletmessage !== this.props.walletmessage) {
      if (newProps.walletmessage == "success") {
        this.props.navigation.navigate('Thankyou', { 'walletamount': this.props.navigation.state.params.walletamount, "updatedAmount": 0 })
      } else if (newProps.ordermessage == "failure") {
        alert('your wallet not updated ')
        this.props.navigation.navigate('Thankyou', { 'walletamount': this.props.navigation.state.params.walletamount, "updatedAmount": 0 })
      }
    }

  }

  constructor(props) {
    super(props);
    //  setBaseURL("https://secure.telr.com/gateway");
    this.state = {
      name: 0,
      email: 0,
      _id: 0,
    };
    this.retriveUserDetails('userobject');
  }

  getTelrResponsefromAPI() {
    const { getPaymentURL, getPaymentUrlfromAPI } = this.props;
    // recive props here 
    /* 
        
    */
    //Enhancement: this is new TELR PAYMENT URL 
    if (this.props.navigation.state.params.total) {
      getPaymentUrlfromAPI({
        "customerid": this.state._id,
        "amount": this.props.navigation.state.params.total
      })
    }
    else if (this.props.navigation.state.params.walletamount) {
      getPaymentUrlfromAPI({
        "customerid": this.state._id,
        "amount": this.props.navigation.state.params.walletamount,
      })
    }
    //FIXME: here i change the code 
    return;

    if (this.props.navigation.state.params.total) {
      getPaymentURL({
        ivp_method: "create",
        ivp_store: "22226",
        ivp_authkey: "k3vg-Xcbjv#2C6wj",
        ivp_cart: Math.random().toString(),
        ivp_test: "1",
        ivp_amount: this.props.navigation.state.params.total,
        ivp_currency: "AED",
        ivp_desc: "SSSS",

        return_auth: "http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1/successTelrRespose",
        return_can: "http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1/failTelrRespose",
        return_decl: "http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1/telrReturnRespose",

        "bill_fname": this.state.name,
        "bill_sname": "Ahmed",
        "bill_addr1": "Test Address 1",
        "bill_city": "Dubai",
        "bill_country": "UAE",
        "bill_email": this.state.email,


      });

      /* FIXME:  
          
                FailTelrCancelResponse   -> Place order 
                Telr Return Response     -> Place order 
      */
    } else if (this.props.navigation.state.params.walletamount) {
      getPaymentURL({
        bill_custref: "customerid",
        ivp_method: "create",
        ivp_store: "22226",
        ivp_authkey: "k3vg-Xcbjv#2C6wj",
        ivp_cart: Math.random().toString(),
        ivp_test: "1",
        ivp_amount: this.props.navigation.state.params.walletamount,
        ivp_currency: "AED",
        ivp_desc: "SSSS",

        return_auth: "http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1/successTelrRespose",
        return_can: "http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1/failTelrRespose",
        return_decl: "http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1/telrReturnRespose",
        "bill_fname": this.state.name,
        "bill_sname": "Ahmed",
        "bill_addr1": "Test Address 1",
        "bill_city": "Dubai",
        "bill_country": "UAE",
        "bill_email": this.state.email,
      });
    }
  }
  componentWillMount() {
    SplashScreen.hide();

  }



  render() {
    if (this.props.loading) {
      return (


        <Spinner
          visible={this.props.loading}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />

      );

    } else {

      return (



        <WebView
          onNavigationStateChange={this._onNavigationStateChange.bind(this)}
          source={{ uri: this.props.url }}

        />

      );
    }

  }
}
const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#FFF'
  },
});
const mapStateToProps = ({ payment, loading }) => {
  console.log('REFrence : ', payment.ref)
  return {
    url: payment.url, data: payment, ref: payment.ref,
    loading: payment.loading, telrdata: payment.telrdata,
    telrmessage: payment.telrmessage, ordermessage: payment.ordermessage,
    error: payment.message,
    walletmessage: payment.walletmessage
  };
};



function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({ getPaymentURL, getTELRResponse, placeOrder, rechargeWallet, getPaymentUrlfromAPI }, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps)
  (PaymentScreen);