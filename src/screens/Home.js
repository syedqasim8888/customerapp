import React, { Component } from 'react';
import { Keyboard, StyleSheet, Text, View, FlatList, TouchableOpacity, Dimensions, Image, ScrollView } from 'react-native';

import { SliderBox } from '../components/SliderBox';
import COLORS from '../assets/colors';
import SplashScreen from 'react-native-splash-screen';
import { setBaseURL, cancelRequest, setCustomHeaders } from "../apis";
import { connect } from "react-redux";
import { fetchProducts, addtoCart, increaseQuantity, decreaseQuantity, fetchAllCategory } from "../store/actions/ProductAction";
import { bindActionCreators } from 'redux';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';
var s = require('../components/style');
import * as RNLocalize from 'react-native-localize';
import { setI18nConfig } from './../utils/i18n';
import { translate } from './../utils/i18n';
import renderIf from '../components/common/renderIf';


class Home extends Component {


  filterProducts = async (e, id) => {

    this.setState({
      isLoading: true
    })
    let text = e.toLowerCase()
    console.log('Text : ', text)
    let products = this.props.products
    let filteredName = products.filter((item) => {
      return item.categoryName.toLowerCase().match(text)
    })
    if (!text || text === '' || text == 'all') {
      console.log('ALL CASE ')
      this.setState({
        data: this.props.products
      })
    } else if (!Array.isArray(filteredName) && !filteredName.length) {
      console.log('NO DATA ')
      // set no data flag to true so as to render flatlist conditionally
      this.setState({

        noData: true
      })
    } else if (Array.isArray(filteredName)) {
      console.log('Filtered DATA ', filteredName)
      this.setState({
        noData: false,
        data: filteredName
      })
    }
    this.setState({
      isLoading: false
    })
    this.setState({
      selectedCategoryid: id
    })
  }

  retrieveUserDetails = async (key) => {
    try {
      const storedValue = await AsyncStorage.getItem("userobject");
      const email = JSON.parse(storedValue).email;
      const password = JSON.parse(storedValue).password;

      console.log('email: ' + email + " password: " + password)

      return
    } catch (error) {
      console.log(error);
    }
    return
  }

  componentDidMount() {
    RNLocalize.addEventListener('change', this.handleLocalizationChange);
  }

  componentWillUnmount() {
    cancelRequest();
    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.products !== prevState.products) {
      console.log('get Dreived State ')
      return ({ data: nextProps.products, products: nextProps.products }) // <- this is setState equivalent
    }
    else if (nextProps.categoryList !== prevState.categoryList) {

    }

    return null
  }

  retriveToken = async (key) => {
    try {
      const retrievedItem = await AsyncStorage.getItem(key);
      console.log(retrievedItem);

      console.log('Token in home', retrievedItem)

      this.setState({
        token: retrievedItem,
      });

      setCustomHeaders([
        {
          name: "Authorization",
          value: 'Bearer ' + retrievedItem,
        }
      ]);
      const { fetchProducts, fetchAllCategory } = this.props;
      fetchProducts();
      fetchAllCategory();
      return retrievedItem;
    } catch (error) {
      console.log(error);
    }
    return
  }

  shouldComponentRender() {

    return true;
  }
  constructor(props) {
    super(props);
    setBaseURL("http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1");
    setI18nConfig();
    this.state = {
      images: [

        'http://itehadmotors.com/assets/banner.png',
        'https://homepagebannershappylife.s3.us-east-2.amazonaws.com/BL_Mob+App+Banner_003.png',
        'https://homepagebannershappylife.s3.us-east-2.amazonaws.com/BL_Mob+App+Banner_002.png'
      ],
      noData: true,
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
      orientation: (Dimensions.get('window').width > Dimensions.get('window').height) ? 'LANDSCAPE' : 'PORTRAIT',
      selectedCategoryid: 0,
      products: [],
      isLoading: false
    };
    SplashScreen.hide();
    this.shouldComponentRender = this.shouldComponentRender.bind(this);
    this.renderItem = this.renderItem.bind(this);

    Keyboard.dismiss();

    this.retriveToken('token');
  }

  handleClick = (id, qty) => {
    this.props.addtoCart(id, qty);
  }
  renderItem({ item }, props) {
    // function body here  
    item.quantity = item.quantity && item.quantity > 0 ? item.quantity : item.quantity = 1;
    return (
      <View style={styles.cardContainer} >
        <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.navigation.navigate('ProductDetails', { item: item })}>

          <Image source={{ uri: item.productimage }} resizeMode="contain" style={styles.cardImageStyle} />
          <Text style={styles.cardTextBold}>{item.productname}</Text>
        
      
          <View style={{ flexDirection: 'row' }} >
         
            <Text style={styles.cardTextDecoration}>{ item.discount && item.discount > 0? item.currencytypes +(item.discount + item.price).toFixed(2) : ''}</Text>
      
            <Text style={[styles.cardTextBold, { marginTop:0, position: 'absolute', right: 5 }]}>{item.currencytypes} {item.price}</Text>
        
          </View>
      
        </TouchableOpacity>
        <View style={styles.cardQuantityContainer}>
          <TouchableOpacity onPress={() => { this.props.decreaseQuantity(item._id, item.quantity) }}>
            <Text style={styles.cardTextSelect}>-</Text>
          </TouchableOpacity>
          <Text style={{ alignSelf: 'center', fontSize: 12, color: 'black', padding: 5 }}>{item.quantity} </Text>
          <TouchableOpacity onPress={() => { this.props.increaseQuantity(item._id, item.quantity) }} >
            <Text style={styles.cardTextSelect}>+</Text>
          </TouchableOpacity>

        </View>
        <TouchableOpacity style={styles.addtoCartButton} onPress={() => { this.props.addtoCart(item._id, 5) }} >
          <Text style={styles.textWhite}>{translate('home_add_to_cart')}</Text>
        </TouchableOpacity>
      </View>

    );

  }

  renderCategoryListItem({ item, index }) {

    return (

      <View style={[styles.filterContainer]}>
        <TouchableOpacity onPress={() => this.filterProducts(item.name, item._id)}>
          <Text style={this.state.selectedCategoryid == item._id ? styles.textSelected : styles.textUnselected}>{item.name}</Text>
        </TouchableOpacity>

        {renderIf(this.props.categoryList && this.props.categoryList.length - 1 != index)(
          <View style={[s.lineSepHorizontal, { marginLeft: 8, marginRight: 8, marginTop: 2, marginBottom: 2 }]} ></View>

        )}
      </View>

    )
  }


  render() {

    const { products, error } = this.props
    if (!this.shouldComponentRender()) return <View>

      <Text>{error}</Text>

    </View>

    return (
      <View style={styles.container}>
        <Spinner
          visible={this.state.isLoading}
        />
        <ScrollView keyboardShouldPersistTaps={"always"} >

          <SliderBox
            images={this.state.images}
            dotColor={COLORS.bgColor}
            paginationBoxStyle={styles.paginationBoxStyle}
            dotStyle={styles.dotStyle}
          />

          <View style={{ marginLeft: 15, marginRight: 15, marginTop: 10, flexDirection: 'row' }}>

            <FlatList
              extraData={this.state}
              horizontal={true}
              data={this.props.categoryList}
              renderItem={(item, index) => this.renderCategoryListItem(item, index)}
              keyExtractor={item => item._id.toString()}
            />

          </View>



          <View style={{ marginLeft: 15 }}>

            <FlatList
              extraData={this.state.selectedCategoryid}
              numColumns={2}
              data={this.state.data}
              renderItem={(item) => this.renderItem(item, this.props)}
              initialNumToRender={5}
              keyExtractor={item => item._id.toString()}
            />

          </View>

        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ededed'
  }, paginationBoxStyle: {
    position: 'absolute',
    bottom: 0,
    padding: 0,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
    margin: 0
  }, dotStyle: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: -5,

    padding: 0,
    margin: 0,
    backgroundColor: 'rgba(128, 128, 128, 0.92)'
  },

  filterContainer: {
    flexDirection: 'row',
    paddingVertical: 5,

  }, textUnselected: {
    fontSize: 14,
    color: COLORS.bgColor
  }, textSelected: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'black'
  }, cardContainer: {
    flexDirection: 'column',
    borderRadius: 5,
    marginRight: 15,
    marginBottom: 10,
    width: (Dimensions.get('window').width / 2 - 22.5),
    backgroundColor: 'white'
  }, cardImageStyle: {
    alignSelf: 'center',
    width: 60,
    height: 60,
    marginTop: 10
  }, cardTextBold: {
    fontSize: 10,
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    fontWeight: 'bold',
    textAlign: 'center',
    flex: 1

  }, cardTextDecoration: {
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
    fontSize: 10,
    marginLeft: 5,
    textDecorationColor: COLORS.bgColor,
    color: '#cecece'
  }, cardQuantityContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5,
    borderWidth: 1,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderColor: COLORS.bgColor,
    borderBottomWidth: 0
  }, cardTextSelect: {
    alignSelf: 'center',
    color: COLORS.bgColor,
    fontWeight: 'bold',
    fontSize: 15,
    padding: 5,
    flex: 1
  }, addtoCartButton: {
    marginBottom: 5,
    marginLeft: 5,
    marginRight: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    backgroundColor: COLORS.bgColor
  }, textWhite: {
    alignSelf: 'center',
    paddingTop: 5,
    paddingBottom: 5,
    color: 'white'
  }, listrow: {
    flex: 1,
    justifyContent: "space-around",

  }
});



const mapStateToProps = ({ products }) => {

  return {
    products: products.products, error: products.error, loading: products.loading,
    categoryList: products.categoryList
  };
}


function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({ fetchProducts, addtoCart, increaseQuantity, decreaseQuantity, fetchAllCategory }, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
