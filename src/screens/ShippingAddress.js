
import React, { Component } from 'react';
import {
    StyleSheet, Text, Platform, TextInput, View, TouchableHighlight,
    FlatList,
    TouchableOpacity,
    Image,
    ScrollView,
    I18nManager,
    Keyboard,
    Picker,
    ActivityIndicator
}
    from 'react-native';



import COLORS, { Colors } from '../stylehelper/Color';
import SplashScreen from 'react-native-splash-screen';
import Header from '../components/common/header';
import StylesCommon from '../stylehelper/StylesCommon';
import { setBaseURL, cancelRequest, setCustomHeaders } from "../apis";
import { connect } from "react-redux";
import { fetchShippingAddress, submitShippingAddress, fetchCitiesList } from "../store/actions/ShippingAction";
import { bindActionCreators } from 'redux';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';
import * as RNLocalize from 'react-native-localize';

import { setI18nConfig } from './../utils/i18n';
import { translate } from './../utils/i18n';
import translationGetters from './../utils/i18n';
import RNPickerSelect, { defaultStyles } from 'react-native-picker-select';
const placeholder_city = {
    label: translate('shipping_address_city'),
    value: null,
    color: '#9EA0A4',
};
class ShippingAddress extends Component {


    constructor(props) {
        super(props);
        this.state = {
            selectedItem: 0,
            buildingName: 0,
            flatNumber:0,
            street: 0,
            area: 0,
            city: 0,
            landMark: 0,
            phoneNo: 0,
            name: 0,
            email: 0,
            _id: 0,
            citiesList: []

        };

        setBaseURL("http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1")
        //  this.retriveToken('token');
        this.retriveUserDetails('userobject');
        this.onPressHandler = this.onPressHandler.bind(this);
    }


    retriveToken = async (key) => {
        try {
            const retrievedItem = await AsyncStorage.getItem(key);
            console.log(retrievedItem);

            console.log('Token in home', retrievedItem)

            this.setState({
                token: retrievedItem,
            });

            setCustomHeaders([
                {
                    name: "Authorization",
                    value: 'Bearer ' + retrievedItem,
                }
            ]);
            const { fetchShippingAddress } = this.props;
            fetchShippingAddress("");

            return retrievedItem;
        } catch (error) {
            console.log(error);
        }
        return
    }
    componentDidMount() {
        RNLocalize.addEventListener('change', this.handleLocalizationChange);
    }

    componentWillUnmount() {
        cancelRequest();
        RNLocalize.removeEventListener('change', this.handleLocalizationChange);
    }

    retriveUserDetails = async (key) => {
        try {
            const retrievedItem = await AsyncStorage.getItem('token');
            const object = await AsyncStorage.getItem(key);
            console.log('Token in home', retrievedItem)
            this.setState({
                name: JSON.parse(object).name,
                _id: JSON.parse(object)._id,
                email: JSON.parse(object).email
            });
            console.log('Name: ' + this.state.name + ' CustomerID: ' + this.state._id + " Email: " + this.state.email)
            setCustomHeaders([
                {
                    name: "Authorization",
                    value: 'Bearer ' + retrievedItem,
                }
            ]);
            const { fetchShippingAddress, fetchCitiesList } = this.props;
            fetchShippingAddress(this.state._id);
            fetchCitiesList();
            //todo add customer id here
            //fetchShippingAddress(this.state._id);
            return retrievedItem;
        } catch (error) {
            console.log(error);
        }
        return
    }

    componentWillMount() {
        SplashScreen.hide();

    }
    componentWillReceiveProps(newProps) {
        if (newProps.result !== this.props.result) {
       }
        if (newProps.cities !== this.props.cities) {
            this.setState({
                citiesList: newProps.cities
            })
        }
    
    }
    onPressHandler(id) {
        this.setState({ selectedItem: id });
    }
    onSubmitHandler() {
/*
 phonenumber, buildingName,street,area,landmark,city,name ,customerid ,email
*/
        const { phoneNo, flatNumber, buildingName, area, street, landMark, city } = this.state
        if (!phoneNo && !flatNumber && !buildingName && !area && !street && !landMark && !city)
            alert('Please fill required Fields')
        else
            this.props.submitShippingAddress({
                address1: this.state.flatNumber + this.state.buildingName + this.state.area + this.state.street + this.state.landMark,
                address2: this.state.city,
                phonenumber: this.state.phoneNo,
                flatNumber: this.state.flatNumber,
                buildingName: this.state.buildingName,
                street: this.state.street,
                area  : this.state.area ,
                landmark:    this.state.landMark,
                city: this.state.city,
                name: this.state.name,
                customerid: this.state._id,
                email: this.state.email
            }).then((response) => {
                this.props.navigation.navigate('SelectTime', { shippingid: response._id, customerid: this.state._id })
     
    }, (error)  => {
       alert(error)
    } )

    }
    onConfirmHandler() {
        if (!this.state.selectedItem)
            alert('Kindly select address or submit new address')
        else
            this.props.navigation.navigate('SelectTime', { shippingid: this.state.selectedItem, customerid: this.state._id })
    }




    render() {
        if (this.props.loading) {
            return (
                <View style={{ flex: 1, paddingTop: 20 }}>
                    <ActivityIndicator />
                </View>
            );
        }
        return (



            <View style={{ flex: 1, backgroundColor: COLORS.grey }}>

                <Spinner visible={this.props.loading} />
                <ScrollView>

                    <View style={styles.mainContainer}>

                        <Header marginHorizontal={20} headerText={translate('shipping_address_title')}></Header>

                        <TextInput
                            placeholder={translate('shipping_address_flat')}
                            underlineColorAndroid='transparent'
                            onChangeText={(flatNumber) => this.setState({ flatNumber })}
                            style={[styles.TextInputStyleClass]}
                            blurOnSubmit={false}
                            onSubmitEditing={() => this.streetRef.focus()} />
                        
                        
                        <TextInput
                            placeholder={translate('shipping_address_building')}
                            underlineColorAndroid='transparent'
                            onChangeText={(buildingName) => this.setState({ buildingName })}
                            style={[styles.TextInputStyleClass, { marginTop: 10 }]}
                            blurOnSubmit={false}
                            onSubmitEditing={() => this.streetRef.focus()} />


                        <TextInput
                            placeholder={translate('shipping_address_Street')}
                            underlineColorAndroid='transparent'
                            onChangeText={(street) => this.setState({ street })}
                            style={[styles.TextInputStyleClass, { marginTop: 10 }]} 
                            ref={ref => this.streetRef = ref} 
                            blurOnSubmit={false}
                            onSubmitEditing={() => this.address.focus()}/>

                        <View style={{ flexDirection: 'row', marginHorizontal: 20, marginTop: 10 }}>

                            <TextInput placeholder={translate('shipping_address_area')}
                                underlineColorAndroid='transparent'
                                onChangeText={(area) => this.setState({ area })}
                                style={[styles.TextInputStyleClass, { flex: 1, marginRight: 5, marginHorizontal: 0, marginTop: 0 }]}
                                ref={ref => this.address = ref}
                                blurOnSubmit={false}
                                onSubmitEditing={() => this.city.focus()} />
                            <View style={[styles.TextInputStyleClass, { flex: 1, marginRight: 5, marginHorizontal: 0, marginTop: 0 }]}
                            >
                                <Picker
                                    style={[styles.TextInputStyleClass, { flex: 1, marginRight: 5, marginHorizontal: 0, marginTop: 0 }]}
                                    selectedValue={this.state.city}
                                    itemStyle={[styles.TextInputStyleClass, { flex: 1, marginRight: 5, marginHorizontal: 0, marginTop: 0 }]}
                                     onValueChange={(itemValue, itemIndex) => this.setState({ city: itemValue })} >
                                    <Picker.Item color='#9EA0A4' label="City" value="" />
                                      { this.state.citiesList.map((item) => (
                                            <Picker.Item color={COLORS.black} label={item.lable} value={item.value} key={item.lable} />)
                                        )}

                                </Picker>
                            </View>


                        </View>

                        <TextInput
                            placeholder={translate('shipping_address_landmark')}
                            returnKeyType={(Platform.OS === 'ios') ? 'done' : 'next'}
                            underlineColorAndroid='transparent'
                            onChangeText={(landMark) => this.setState({ landMark })}
                            style={[styles.TextInputStyleClass, { marginTop: 10 }]}
                            ref={ref => this.landmark = ref}
                            blurOnSubmit={false}
                            onSubmitEditing={() => this.phoneno.focus()} />


                        <TextInput
                            placeholder={translate('shipping_address_phoneno')}
                            returnKeyType={(Platform.OS === 'ios') ? 'done' : 'next'}
                            keyboardType="numeric"
                            underlineColorAndroid='transparent'
                            onChangeText={(phoneNo) => this.setState({ phoneNo })}
                            style={[styles.TextInputStyleClass, { marginTop: 10 }]}
                            ref={ref => this.phoneno = ref}
                            blurOnSubmit={false}
                            onSubmitEditing={Keyboard.dismiss} />


                        <TouchableOpacity style={styles.ButtonStyleClass} onPress={() =>
                            this.onSubmitHandler()} >
                            <Text style={[styles.boldtext]}>{translate('shipping_address_submit')}</Text>
                        </TouchableOpacity>


                        <View style={{ flex: 1, backgroundColor: COLORS.grey, marginTop: 5 }}>

                            <Text style={{ fontSize: 20, marginHorizontal: 20, marginTop: 10, textAlign: "left", width: '100%' }}>{translate('shipping_address_below')}</Text>


                            <View style={[styles.lineStyle, { marginBottom: 10, marginHorizontal: 20 }]}></View>


                            <FlatList
                                data={this.props.data}
                                extraData={this.state.selectedItem}
                                keyExtractor={item => item._id.toString()}
                                renderItem={
                                    ({ item }) =>
                                        <TouchableOpacity
                                            onPress={() => this.onPressHandler(item._id)}>
                                            <View style={{ flexDirection: 'row', marginVertical: 5, marginHorizontal: 20 }}>
                                                <Image
                                                    style={{ flex: 0.1, resizeMode: 'contain', alignSelf: "center", height: 20 }}
                                                    tintColor={this.state.selectedItem == item._id ? COLORS.red : COLORS.placehldrgrey}
                                                    source={require('../assets/redtick.png')}
                                                />
                                                <Text style={[styles.TextStyleClass, { flex: 0.9, color: COLORS.placehldrgrey }]}>{item.address1}</Text>
                                            </View>
                                        </TouchableOpacity>
                                }
                            />


                        </View >

                    </View>
                </ScrollView >
                <View style={StylesCommon.bottomButtonContainer}>
                    <TouchableOpacity style={[StylesCommon.btnSquareBottom]} onPress={() => this.onConfirmHandler()}  >
                        <Text style={[StylesCommon.boldTextLarge, { color: COLORS.white }]}>{translate('shipping_address_confirm')}</Text>
                    </TouchableOpacity>
                </View>

            </View>


        );


    }
}
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        flex: 1,
        textAlign: I18nManager.isRTL ? 'right' : 'left',
        height: 50,
        borderWidth: 0.5,
        borderColor: COLORS.black,
        borderRadius: 25,
        backgroundColor: "#FFFFFF",


        paddingHorizontal: 20
    },
    inputAndroid: {

        textAlign: I18nManager.isRTL ? 'right' : 'left',
        height: 50,
        borderWidth: 0.5,
        borderColor: COLORS.black,
        borderRadius: 25,
        backgroundColor: "#FFFFFF",


        paddingHorizontal: 20
    },
});
const styles = StyleSheet.create({

    mainContainer: {
        backgroundColor: COLORS.white,
        flex: 1,
        flexDirection: 'column',
        marginBottom: 50
    },
    TextInputStyleClass: {

        textAlign: I18nManager.isRTL ? 'right' : 'left',
        height: 50,
        borderWidth: 0.5,
        borderColor: COLORS.black,
        borderRadius: 25,
        backgroundColor: "#FFFFFF",
        marginHorizontal: 20,
        marginTop: 20,

        paddingHorizontal: 20


    },
    TextStyleClass: {
        textAlignVertical: 'center',
        height: 40,
        borderWidth: 0.5,
        borderColor: COLORS.black,
        borderRadius: 25,
        backgroundColor: COLORS.white,
        textAlign: 'left',
        padding: 10,
        marginLeft: 10,
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 10,
        paddingBottom: 10

    },
    lineStyle: {
        borderWidth: 0.5,
        color: COLORS.black,
        marginTop: 10,
        marginHorizontal: 20
    },
    ButtonStyleClass: {
        textAlign: 'center',
        height: 50,
        borderRadius: 25,
        backgroundColor: COLORS.Orange,
        marginHorizontal: 20,
        marginTop: 10,
        marginBottom: 5,
        textAlign: 'left',
        padding: 10

    },
    boldtext: {
        fontSize: 18,
        color: "white",
        textAlign: 'center',
        textAlignVertical: 'center',
        fontWeight: 'bold',
        height: '100%',
        width: '100%'

    },

});


const mapStateToProps = ({ shipping }) => {
    console.log(shipping.result)
    // RNTestLibModule.show("Hello World");
    return { data: shipping.data, result: shipping.result, loading: shipping.loading, cities: shipping.cities };
}


function mapDispatchToProps(dispatch) {
    return {
        ...bindActionCreators({ fetchShippingAddress, submitShippingAddress, fetchCitiesList }, dispatch)
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ShippingAddress);