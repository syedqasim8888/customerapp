import React  , { Component }from 'react';
import AppNavigation from './src/navigator/AppNavigation';
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import storeConfig from "./src/store";
 import NavigationService from "./src/navigator/NavigationService"
 
 
export default  class App extends  Component {
 
  render() {
    let storeDefaults = storeConfig();
    let Navigator = AppNavigation(this.props.token ? "MainStack" : "Authentication");
    return (
      <Provider store={storeDefaults.store}>
        <PersistGate loading={null} persistor={storeDefaults.persistor}>
          <Navigator  
          ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }} />
        </PersistGate>
      </Provider>
    );
  }
}

