
import React, { Component } from 'react';
import {
    I18nManager,
    Switch,
    ScrollView,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
} from 'react-native';

import * as Animatable from 'react-native-animatable';
import Collapsible from 'react-native-collapsible';
import Accordion from 'react-native-collapsible/Accordion';
import SplashScreen from 'react-native-splash-screen';
import Header from '../components/common/header';
import COLORS from '../stylehelper/Color';
import { getFAQS } from '../store/actions/FAQAction';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import AsyncStorage from '@react-native-community/async-storage';
import * as RNLocalize from 'react-native-localize';
import { setI18nConfig } from './../utils/i18n'
import { translate } from './../utils/i18n';
import { setBaseURL, cancelRequest, setCustomHeaders } from "../apis";

 


 class FAQ extends Component {
    state = {
        activeSections: [],
        collapsed: true,
        multipleSelect: false,
        isArabic:""
    };

    toggleExpanded = () => {
        this.setState({ collapsed: !this.state.collapsed });
    };

    setSections = sections => {
        this.setState({
            activeSections: sections.includes(undefined) ? [] : sections,
        });
    };
    constructor(props) {
        super(props);
        setBaseURL("http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1");
        
        setI18nConfig();
        //this.props.getFAQS(this.state.isArabic)

        this.getFaqAsync();
    }
    
    getFaqAsync = async () => {
        try {
          const language = await AsyncStorage.getItem("language");

          if(language=='ar'){
            this.setState({ isArabic: "1" });
            console.log('faq '+language)
            this.props.getFAQS(this.state.isArabic)
        }
        else if(language=='en'){
            this.props.getFAQS(this.state.isArabic)
        }     
          return retrievedItem;
        } catch (error) {
          console.log(error);
        }
        return
      }

    componentWillMount() {
        SplashScreen.hide();
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', this.handleLocalizationChange); 
      }

      
  componentWillUnmount() {
    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
  }

  handleLocalizationChange = () => {
     setI18nConfig();
  };


    renderHeader = (section, _, isActive) => {
        return (
            <Animatable.View
                duration={400}
                style={[styles.header, isActive ? styles.active : styles.inactive]}
                transition="backgroundColor"
            >
                <View style={[isActive ? { flex: 1, backgroundColor: COLORS.Orange } : { flex: 1, backgroundColor: '#F5FCFF' }]}>
                    <Text style={[styles.headerText, isActive ? styles.activeHeaderText : styles.inactiveHeaderText]}>{section.questions}</Text>
                </View>
            </Animatable.View>
        );
    };

    renderContent(section, _, isActive) {
        return (
            <Animatable.View
                duration={400}
                style={[styles.content, isActive ? styles.active : styles.inactive]}
                transition="backgroundColor"
            >
                <Animatable.Text animation={isActive ? 'bounceIn' : undefined}>
                    {section.answer}
                </Animatable.Text>
            </Animatable.View>
        );
    }

    render() {
        const { multipleSelect, activeSections } = this.state;

        return (
            <View style={styles.container}>
                <ScrollView  >
                    <Header marginHorizontal={20} headerText={'FAQS'} ></Header>

                    <Collapsible collapsed={this.state.collapsed} align="center">
                        <View style={styles.content}>
                            <Text>
                                Bacon ipsum dolor amet chuck turducken landjaeger tongue spare
                                ribs
              </Text>
                        </View>
                    </Collapsible>
                    <Accordion
                        activeSections={activeSections}
                        sections={this.props.data}
                        touchableComponent={TouchableOpacity}
                        expandMultiple={multipleSelect}
                        renderHeader={this.renderHeader}
                        renderContent={this.renderContent}
                        duration={400}
                        onChange={this.setSections}
                        containerStyle={{ marginTop: 10 }}
                    />
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',

    },
    title: {
        textAlign: 'center',
        fontSize: 22,
        fontWeight: '300',
        marginBottom: 20,
    },
    header: {
        backgroundColor: '#F5FCFF',
        marginHorizontal: 10,

    },
    headerText: {

        marginLeft: 10,
        fontSize: 16,
        marginTop: 10,
        marginBottom: 10,
        fontWeight: '500',
    },
    content: {
        padding: 20,
        backgroundColor: '#fff',
    },
    active: {
        backgroundColor: COLORS.white,
    },
    inactive: {
        backgroundColor: 'rgba(245,252,255,1)',
    }, activeHeaderText: {

        color: 'white'

    }, inactiveHeaderText: {
        color: 'black'
    },
    selectors: {
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    selector: {
        backgroundColor: '#F5FCFF',
        padding: 10,
    },
    activeSelector: {
        fontWeight: 'bold',
    },
    selectTitle: {
        fontSize: 14,
        fontWeight: '500',
        padding: 10,
    },
    multipleToggle: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginVertical: 30,
        alignItems: 'center',
    },
    multipleToggle__title: {
        fontSize: 16,
        marginRight: 8,
    },
});


const mapStateToProps = ({ faq }) => {
    
    // RNTestLibModule.show("Hello World");

    return { loading:faq.loading , data:faq.data };
  }
  
  
  function mapDispatchToProps(dispatch) {
    return {
      ...bindActionCreators({getFAQS }, dispatch)
    }
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(FAQ);