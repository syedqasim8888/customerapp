import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    Alert,
    ScrollView,
    FlatList,
    TextInput,
    Button,
    I18nManager 
} from 'react-native';
import { setBaseURL, cancelRequest, setCustomHeaders } from "../apis";
import SplashScreen from 'react-native-splash-screen';
import * as RNLocalize from 'react-native-localize';
import { setI18nConfig } from './../utils/i18n';
import { translate } from './../utils/i18n';
import { getWalletTransaction } from '../store/actions/WalletAction';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import AsyncStorage from '@react-native-community/async-storage';
class WalletTransaction extends Component {

    constructor(props) {
        super(props);
        //http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000
       // setI18nConfig();
       setBaseURL("http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1")
       this.retriveUserDetails('userobject');  
    }


    retriveUserDetails = async (key) => {
        try {
            const retrievedItem = await AsyncStorage.getItem('token');
            const object = await AsyncStorage.getItem(key);
            console.log('Token in home', retrievedItem)
            this.setState({
                name: JSON.parse(object).name,
                _id: JSON.parse(object)._id,
                email: JSON.parse(object).email
            });
            console.log('Name: ' + this.state.name + ' CustomerID: ' + this.state._id + " Email: " + this.state.email)
            setCustomHeaders([
                {
                    name: "Authorization",
                    value: 'Bearer ' + retrievedItem,
                }
            ]);
            const { getWalletTransaction } = this.props;
            getWalletTransaction( this.state._id);
            //todo add customer id here
            //fetchShippingAddress(this.state._id);
            return retrievedItem;
        } catch (error) {
            console.log(error);
        }
        return
    }

    componentWillMount() {
        SplashScreen.hide();
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', this.handleLocalizationChange);
      }
    
      componentWillUnmount() {
        cancelRequest();
        RNLocalize.removeEventListener('change', this.handleLocalizationChange);
      }

    clickEventListener() {
        Alert.alert("Success", "Product has been added to cart")
    }

    render() {
        console.log(this.props)
        return (

            <View style={[styles.mainContainer]} >

 
                <View style={styles.inputContainer}  >

                    <View style={{ alignItems: 'center', marginTop: 30 }}>
                        <Text style={styles.name}>{translate('wallet_transactions_title')}</Text>
                    </View>

                    <View style={[styles.lineStyle, { marginTop: 10 }]}>
                    </View>

                    

                    {this.renderFlatList()}

                </View>

                <View style={[{ flex: 0.15, height: 100, width: '100%', bottom: 0, position: 'absolute' }]}>

                    <View style={styles.bottom}>

                        <View style={[styles.lineStyle, { color: '#F55130' }]}>
                        </View>

                        <View style={[{ flexDirection: 'row', marginTop: 15, width: '100%' }]}>
                            <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.textblack, { textAlign: 'left', width: '50%', fontWeight: "bold" }]}>{translate('wallet_transactions_recharge_total')}</Text>
                            <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.textblack, { textAlign: 'right', width: '50%', fontWeight: "bold" }]}>{ I18nManager.isRTL ? this.props.totalrecharge + ' ' +translate('common_aed')  : translate('common_aed') + ' ' +this.props.totalrecharge}</Text>
                        </View>
                        <View style={[{ flexDirection: 'row', marginTop: 10, width: '100%', marginBottom: 4 }]}>
                            <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.textblack, { textAlign: 'left', width: '50%', color: '#F55130', fontWeight: "bold" }]}>{translate('wallet_transactions_happy_cash_total')}</Text>
                            <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.textblack, { textAlign: 'right', width: '50%', color: '#F55130', fontWeight: "bold" }]}>{ I18nManager.isRTL ? this.props.totalhappycash + ' ' +translate('common_aed')  : translate('common_aed') + ' ' +this.props.totalhappycash}</Text>
                        </View>


                    </View>
                </View>
            </View>

        );
    }

    renderFlatList() {
        return (


            <FlatList
                data={ this.props.transactions}
                renderItem={
                    ({ item }) =>

                        <View style={[styles.ItemviewContainer, { flexDirection: 'column', marginTop: 20 }]} >

                            <View style={[{ flexDirection: 'column', flex: 1, height: '100%', width: '100%' }]}>

                                <View style={[{ flexDirection: 'row', flex: 1, width: '100%' }]}>
                                    <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.textblack, { flex: 0.30 }]}>{translate('wallet_transactions_recharge')}-</Text>
                                    <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.textblack, { flex:I18nManager.isRTL ?0.30: 0.40, textAlign:'center' }]}>{item.transactionDate}</Text>
                                    <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.textblack, { flex:I18nManager.isRTL ?0.40: 0.30, alignSelf: 'flex-end', position: 'absolute', right: 0, fontWeight: 'bold' }]}>{ I18nManager.isRTL ? item.walletamt + ' ' +translate('common_aed')  : translate('common_aed') + ' ' +item.walletamt}</Text>
                                </View>

                                <View style={[{ flexDirection: 'row', flex: 1, width: '100%', marginTop: 20 }]}>
                                    <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.textblack, { flex: 0.30 }]}>{translate('wallet_transactions_happy_cash')}-</Text>
                                    <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.textblack, { flex:I18nManager.isRTL ?0.30: 0.40,textAlign:'center'  }]}>{item.transactionDate}</Text>
                                    <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.textblack, { flex:I18nManager.isRTL ?0.40: 0.30, alignSelf: 'flex-end', position: 'absolute', right: 0, fontWeight: 'bold', color: '#F55130' }]}>{ I18nManager.isRTL ? item.happyCash + ' ' +translate('common_aed')  : translate('common_aed') + ' ' +item.happyCash}</Text>
                                </View>
                            </View>
                        </View>
                }
            />



        );
    }

}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        flexDirection: 'column'
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        marginTop: 30,
    },
    productImg: {
        width: 200,
        height: 200,
    },
    name: {
        fontSize: 20,
        color: "black",
        textAlign: 'right',
        alignSelf: 'flex-start'

    }, inputContainer: {
        flex: 0.85,
        justifyContent: 'flex-start',
        flexDirection: 'column',
        marginHorizontal: 20
    },
    lineStyle: {
        borderWidth: 0.35,
        borderColor: '#f55130',
        width: '100%',
    },
    viewContainer: {
        borderWidth: 0.5,
        borderColor: '#000',
        borderRadius: 10,
        marginTop: 10,
        padding: 10,
    },
    ItemviewContainer: {
        flexDirection: 'row',
        borderWidth: 0.5,
        borderColor: '#f55130',
        borderRadius: 5,
        marginTop: 10,
        padding: 20,
    },
    orderHeading: {

        fontSize: 13,
        color: "green",
        fontWeight: 'bold',
        alignSelf: 'flex-start'
    },
    textblack: {

        fontSize: 12,
        color: "black",
    },
    greytext: {

        fontSize: 13,
        color: "grey",
        alignSelf: 'flex-start'
    },
    boldtext: {

        fontSize: 13,
        color: "black",
        alignSelf: 'center',
        justifyContent: 'center',
        marginLeft: 10,
        fontWeight: 'bold'
    },
    btnNew: {
        paddingVertical: 7,
        width: 70,
        borderRadius: 20,
        marginHorizontal: 3,
        borderColor: '#f55130',
        borderWidth: 0.5,
        alignSelf: 'center',
        position: "absolute",
        right: 10
    },
    btnSquareleft: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',


        backgroundColor: '#fffff0'
    },
    btnSquareright: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        height: '100%',

        backgroundColor: "#f55130"
    },
    textNew: {
        textAlign: 'center',
        fontSize: 12,
        color: '#f55130',
    },
    boldgreen: {

        fontSize: 12,
        color: '#f55130',
        fontWeight: 'bold',

    },
    separatorVertical: {
        width: 1,
        backgroundColor: "#f55130",


    },
    bottom: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        marginHorizontal: 20
    },
    star: {
        width: 40,
        height: 40,
    },
    btnColor: {
        height: 30,
        width: 30,
        borderRadius: 30,
        marginHorizontal: 3
    },
    btnSize: {
        height: 40,
        width: 40,
        borderRadius: 40,
        borderColor: '#778899',
        borderWidth: 1,
        marginHorizontal: 3,
        backgroundColor: 'white',

        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    starContainer: {
        justifyContent: 'center',
        marginHorizontal: 30,
        flexDirection: 'row',
        marginTop: 20
    },
    contentColors: {
        justifyContent: 'center',
        marginHorizontal: 30,
        flexDirection: 'row',
        marginTop: 20
    },
    contentSize: {
        justifyContent: 'center',
        marginHorizontal: 20,
        flexDirection: 'row',
        marginTop: 20
    },
    separator: {
        height: 1,
        backgroundColor: "#f55130",
        marginTop: 20,
        marginHorizontal: 30
    },
    shareButton: {
        marginTop: 10,
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
        backgroundColor: "#00BFFF",
    },
    shareButtonText: {
        color: "#FFFFFF",
        fontSize: 20,
    }, buttonContainer: {

        flexDirection: 'row',
        backgroundColor: '#f05424',
        marginBottom: 10,
        marginLeft: 20,
        marginRight: 20,
        height: 45,
        borderRadius: 30,
        marginTop: 30


    },
    loginText: {
        flex: 1,
        color: 'white',
        fontSize: 13,
        textAlign: 'center',
        alignSelf: 'center',
        fontWeight: 'bold'
    },
    bottomView: {
        width: '100%',
        justifyContent: 'center',
        marginHorizontal: 20,
        flexDirection: 'column',
        bottom: 0
    }
});     


const mapStateToProps = ({ wallet }) => {
    
    // RNTestLibModule.show("Hello World");
    return { totalhappycash:wallet.totalhappycash , totalrecharge  : wallet.totalrecharge, transactions: wallet.transactions};
  }
  
  
  function mapDispatchToProps(dispatch) {
    return {
      ...bindActionCreators({getWalletTransaction }, dispatch)
    }
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(WalletTransaction);