import React, { Component, useState } from 'react';

import {
  StyleSheet,
  Text,
  View,
  Alert,
  TextInput,
  Platform,
  PixelRatio,
  Dimensions,
  StatusBar,
  Image,
  ScrollView,
  TouchableOpacity,
  I18nManager,
  Keyboard
} from 'react-native';
import { bindActionCreators } from 'redux';
import SplashScreen from 'react-native-splash-screen';
import COLORS from '../stylehelper/Color';
import { connect } from "react-redux";
import { setBaseURL, cancelRequest, setCustomHeaders } from "../apis";
import { registerUser } from "../store/actions/SignUpAction";
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';
import Toast, { DURATION } from 'react-native-easy-toast'
import { setI18nConfig } from './../utils/i18n';
import { translate } from './../utils/i18n';
import * as RNLocalize from 'react-native-localize';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import Icon from 'react-native-vector-icons/Entypo';
import RNPickerSelect, { defaultStyles } from 'react-native-picker-select';
import CountryPicker, { getAllCountries } from 'react-native-country-picker-modal';
const { width } = Dimensions.get('window');
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from "moment"; // For dae Formatting
import { Colors } from 'react-native/Libraries/NewAppScreen';
const gender = [
  {
    label: 'Male',
    value: 'male',
  },
  {
    label: 'Female',
    value: 'female',
  }
];

const nationality =  [
  {
    "label": "Afghanistan",
    "value": "AF"
  },
  {
    "label": "Åland Islands",
    "value": "AX"
  },
  {
    "label": "Albania",
    "value": "AL"
  },
  {
    "label": "Algeria",
    "value": "DZ"
  },
  {
    "label": "American Samoa",
    "value": "AS"
  },
  {
    "label": "Andorra",
    "value": "AD"
  },
  {
    "label": "Angola",
    "value": "AO"
  },
  {
    "label": "Anguilla",
    "value": "AI"
  },
  {
    "label": "Antarctica",
    "value": "AQ"
  },
  {
    "label": "Antigua and Barbuda",
    "value": "AG"
  },
  {
    "label": "Argentina",
    "value": "AR"
  },
  {
    "label": "Armenia",
    "value": "AM"
  },
  {
    "label": "Aruba",
    "value": "AW"
  },
  {
    "label": "Australia",
    "value": "AU"
  },
  {
    "label": "Austria",
    "value": "AT"
  },
  {
    "label": "Azerbaijan",
    "value": "AZ"
  },
  {
    "label": "Bahamas",
    "value": "BS"
  },
  {
    "label": "Bahrain",
    "value": "BH"
  },
  {
    "label": "Bangladesh",
    "value": "BD"
  },
  {
    "label": "Barbados",
    "value": "BB"
  },
  {
    "label": "Belarus",
    "value": "BY"
  },
  {
    "label": "Belgium",
    "value": "BE"
  },
  {
    "label": "Belize",
    "value": "BZ"
  },
  {
    "label": "Benin",
    "value": "BJ"
  },
  {
    "label": "Bermuda",
    "value": "BM"
  },
  {
    "label": "Bhutan",
    "value": "BT"
  },
  {
    "label": "Bolivia, Plurinational State of",
    "value": "BO"
  },
  {
    "label": "Bonaire, Sint Eustatius and Saba",
    "value": "BQ"
  },
  {
    "label": "Bosnia and Herzegovina",
    "value": "BA"
  },
  {
    "label": "Botswana",
    "value": "BW"
  },
  {
    "label": "Bouvet Island",
    "value": "BV"
  },
  {
    "label": "Brazil",
    "value": "BR"
  },
  {
    "label": "British Indian Ocean Territory",
    "value": "IO"
  },
  {
    "label": "Brunei Darussalam",
    "value": "BN"
  },
  {
    "label": "Bulgaria",
    "value": "BG"
  },
  {
    "label": "Burkina Faso",
    "value": "BF"
  },
  {
    "label": "Burundi",
    "value": "BI"
  },
  {
    "label": "Cambodia",
    "value": "KH"
  },
  {
    "label": "Cameroon",
    "value": "CM"
  },
  {
    "label": "Canada",
    "value": "CA"
  },
  {
    "label": "Cape Verde",
    "value": "CV"
  },
  {
    "label": "Cayman Islands",
    "value": "KY"
  },
  {
    "label": "Central African Republic",
    "value": "CF"
  },
  {
    "label": "Chad",
    "value": "TD"
  },
  {
    "label": "Chile",
    "value": "CL"
  },
  {
    "label": "China",
    "value": "CN"
  },
  {
    "label": "Christmas Island",
    "value": "CX"
  },
  {
    "label": "Cocos (Keeling) Islands",
    "value": "CC"
  },
  {
    "label": "Colombia",
    "value": "CO"
  },
  {
    "label": "Comoros",
    "value": "KM"
  },
  {
    "label": "Congo",
    "value": "CG"
  },
  {
    "label": "Congo, the Democratic Republic of the",
    "value": "CD"
  },
  {
    "label": "Cook Islands",
    "value": "CK"
  },
  {
    "label": "Costa Rica",
    "value": "CR"
  },
  {
    "label": "Côte d'Ivoire",
    "value": "CI"
  },
  {
    "label": "Croatia",
    "value": "HR"
  },
  {
    "label": "Cuba",
    "value": "CU"
  },
  {
    "label": "Curaçao",
    "value": "CW"
  },
  {
    "label": "Cyprus",
    "value": "CY"
  },
  {
    "label": "Czech Republic",
    "value": "CZ"
  },
  {
    "label": "Denmark",
    "value": "DK"
  },
  {
    "label": "Djibouti",
    "value": "DJ"
  },
  {
    "label": "Dominica",
    "value": "DM"
  },
  {
    "label": "Dominican Republic",
    "value": "DO"
  },
  {
    "label": "Ecuador",
    "value": "EC"
  },
  {
    "label": "Egypt",
    "value": "EG"
  },
  {
    "label": "El Salvador",
    "value": "SV"
  },
  {
    "label": "Equatorial Guinea",
    "value": "GQ"
  },
  {
    "label": "Eritrea",
    "value": "ER"
  },
  {
    "label": "Estonia",
    "value": "EE"
  },
  {
    "label": "Ethiopia",
    "value": "ET"
  },
  {
    "label": "Falkland Islands (Malvinas)",
    "value": "FK"
  },
  {
    "label": "Faroe Islands",
    "value": "FO"
  },
  {
    "label": "Fiji",
    "value": "FJ"
  },
  {
    "label": "Finland",
    "value": "FI"
  },
  {
    "label": "France",
    "value": "FR"
  },
  {
    "label": "French Guiana",
    "value": "GF"
  },
  {
    "label": "French Polynesia",
    "value": "PF"
  },
  {
    "label": "French Southern Territories",
    "value": "TF"
  },
  {
    "label": "Gabon",
    "value": "GA"
  },
  {
    "label": "Gambia",
    "value": "GM"
  },
  {
    "label": "Georgia",
    "value": "GE"
  },
  {
    "label": "Germany",
    "value": "DE"
  },
  {
    "label": "Ghana",
    "value": "GH"
  },
  {
    "label": "Gibraltar",
    "value": "GI"
  },
  {
    "label": "Greece",
    "value": "GR"
  },
  {
    "label": "Greenland",
    "value": "GL"
  },
  {
    "label": "Grenada",
    "value": "GD"
  },
  {
    "label": "Guadeloupe",
    "value": "GP"
  },
  {
    "label": "Guam",
    "value": "GU"
  },
  {
    "label": "Guatemala",
    "value": "GT"
  },
  {
    "label": "Guernsey",
    "value": "GG"
  },
  {
    "label": "Guinea",
    "value": "GN"
  },
  {
    "label": "Guinea-Bissau",
    "value": "GW"
  },
  {
    "label": "Guyana",
    "value": "GY"
  },
  {
    "label": "Haiti",
    "value": "HT"
  },
  {
    "label": "Heard Island and McDonald Islands",
    "value": "HM"
  },
  {
    "label": "Holy See (Vatican City State)",
    "value": "VA"
  },
  {
    "label": "Honduras",
    "value": "HN"
  },
  {
    "label": "Hong Kong",
    "value": "HK"
  },
  {
    "label": "Hungary",
    "value": "HU"
  },
  {
    "label": "Iceland",
    "value": "IS"
  },
  {
    "label": "India",
    "value": "IN"
  },
  {
    "label": "Indonesia",
    "value": "ID"
  },
  {
    "label": "Iran, Islamic Republic of",
    "value": "IR"
  },
  {
    "label": "Iraq",
    "value": "IQ"
  },
  {
    "label": "Ireland",
    "value": "IE"
  },
  {
    "label": "Isle of Man",
    "value": "IM"
  },
  {
    "label": "Israel",
    "value": "IL"
  },
  {
    "label": "Italy",
    "value": "IT"
  },
  {
    "label": "Jamaica",
    "value": "JM"
  },
  {
    "label": "Japan",
    "value": "JP"
  },
  {
    "label": "Jersey",
    "value": "JE"
  },
  {
    "label": "Jordan",
    "value": "JO"
  },
  {
    "label": "Kazakhstan",
    "value": "KZ"
  },
  {
    "label": "Kenya",
    "value": "KE"
  },
  {
    "label": "Kiribati",
    "value": "KI"
  },
  {
    "label": "Korea, Democratic People's Republic of",
    "value": "KP"
  },
  {
    "label": "Korea, Republic of",
    "value": "KR"
  },
  {
    "label": "Kuwait",
    "value": "KW"
  },
  {
    "label": "Kyrgyzstan",
    "value": "KG"
  },
  {
    "label": "Lao People's Democratic Republic",
    "value": "LA"
  },
  {
    "label": "Latvia",
    "value": "LV"
  },
  {
    "label": "Lebanon",
    "value": "LB"
  },
  {
    "label": "Lesotho",
    "value": "LS"
  },
  {
    "label": "Liberia",
    "value": "LR"
  },
  {
    "label": "Libya",
    "value": "LY"
  },
  {
    "label": "Liechtenstein",
    "value": "LI"
  },
  {
    "label": "Lithuania",
    "value": "LT"
  },
  {
    "label": "Luxembourg",
    "value": "LU"
  },
  {
    "label": "Macao",
    "value": "MO"
  },
  {
    "label": "Macedonia, the Former Yugoslav Republic of",
    "value": "MK"
  },
  {
    "label": "Madagascar",
    "value": "MG"
  },
  {
    "label": "Malawi",
    "value": "MW"
  },
  {
    "label": "Malaysia",
    "value": "MY"
  },
  {
    "label": "Maldives",
    "value": "MV"
  },
  {
    "label": "Mali",
    "value": "ML"
  },
  {
    "label": "Malta",
    "value": "MT"
  },
  {
    "label": "Marshall Islands",
    "value": "MH"
  },
  {
    "label": "Martinique",
    "value": "MQ"
  },
  {
    "label": "Mauritania",
    "value": "MR"
  },
  {
    "label": "Mauritius",
    "value": "MU"
  },
  {
    "label": "Mayotte",
    "value": "YT"
  },
  {
    "label": "Mexico",
    "value": "MX"
  },
  {
    "label": "Micronesia, Federated States of",
    "value": "FM"
  },
  {
    "label": "Moldova, Republic of",
    "value": "MD"
  },
  {
    "label": "Monaco",
    "value": "MC"
  },
  {
    "label": "Mongolia",
    "value": "MN"
  },
  {
    "label": "Montenegro",
    "value": "ME"
  },
  {
    "label": "Montserrat",
    "value": "MS"
  },
  {
    "label": "Morocco",
    "value": "MA"
  },
  {
    "label": "Mozambique",
    "value": "MZ"
  },
  {
    "label": "Myanmar",
    "value": "MM"
  },
  {
    "label": "Namibia",
    "value": "NA"
  },
  {
    "label": "Nauru",
    "value": "NR"
  },
  {
    "label": "Nepal",
    "value": "NP"
  },
  {
    "label": "Netherlands",
    "value": "NL"
  },
  {
    "label": "New Caledonia",
    "value": "NC"
  },
  {
    "label": "New Zealand",
    "value": "NZ"
  },
  {
    "label": "Nicaragua",
    "value": "NI"
  },
  {
    "label": "Niger",
    "value": "NE"
  },
  {
    "label": "Nigeria",
    "value": "NG"
  },
  {
    "label": "Niue",
    "value": "NU"
  },
  {
    "label": "Norfolk Island",
    "value": "NF"
  },
  {
    "label": "Northern Mariana Islands",
    "value": "MP"
  },
  {
    "label": "Norway",
    "value": "NO"
  },
  {
    "label": "Oman",
    "value": "OM"
  },
  {
    "label": "Pakistan",
    "value": "PK"
  },
  {
    "label": "Palau",
    "value": "PW"
  },
  {
    "label": "Palestine, State of",
    "value": "PS"
  },
  {
    "label": "Panama",
    "value": "PA"
  },
  {
    "label": "Papua New Guinea",
    "value": "PG"
  },
  {
    "label": "Paraguay",
    "value": "PY"
  },
  {
    "label": "Peru",
    "value": "PE"
  },
  {
    "label": "Philippines",
    "value": "PH"
  },
  {
    "label": "Pitcairn",
    "value": "PN"
  },
  {
    "label": "Poland",
    "value": "PL"
  },
  {
    "label": "Portugal",
    "value": "PT"
  },
  {
    "label": "Puerto Rico",
    "value": "PR"
  },
  {
    "label": "Qatar",
    "value": "QA"
  },
  {
    "label": "Réunion",
    "value": "RE"
  },
  {
    "label": "Romania",
    "value": "RO"
  },
  {
    "label": "Russian Federation",
    "value": "RU"
  },
  {
    "label": "Rwanda",
    "value": "RW"
  },
  {
    "label": "Saint Barthélemy",
    "value": "BL"
  },
  {
    "label": "Saint Helena, Ascension and Tristan da Cunha",
    "value": "SH"
  },
  {
    "label": "Saint Kitts and Nevis",
    "value": "KN"
  },
  {
    "label": "Saint Lucia",
    "value": "LC"
  },
  {
    "label": "Saint Martin (French part)",
    "value": "MF"
  },
  {
    "label": "Saint Pierre and Miquelon",
    "value": "PM"
  },
  {
    "label": "Saint Vincent and the Grenadines",
    "value": "VC"
  },
  {
    "label": "Samoa",
    "value": "WS"
  },
  {
    "label": "San Marino",
    "value": "SM"
  },
  {
    "label": "Sao Tome and Principe",
    "value": "ST"
  },
  {
    "label": "Saudi Arabia",
    "value": "SA"
  },
  {
    "label": "Senegal",
    "value": "SN"
  },
  {
    "label": "Serbia",
    "value": "RS"
  },
  {
    "label": "Seychelles",
    "value": "SC"
  },
  {
    "label": "Sierra Leone",
    "value": "SL"
  },
  {
    "label": "Singapore",
    "value": "SG"
  },
  {
    "label": "Sint Maarten (Dutch part)",
    "value": "SX"
  },
  {
    "label": "Slovakia",
    "value": "SK"
  },
  {
    "label": "Slovenia",
    "value": "SI"
  },
  {
    "label": "Solomon Islands",
    "value": "SB"
  },
  {
    "label": "Somalia",
    "value": "SO"
  },
  {
    "label": "South Africa",
    "value": "ZA"
  },
  {
    "label": "South Georgia and the South Sandwich Islands",
    "value": "GS"
  },
  {
    "label": "South Sudan",
    "value": "SS"
  },
  {
    "label": "Spain",
    "value": "ES"
  },
  {
    "label": "Sri Lanka",
    "value": "LK"
  },
  {
    "label": "Sudan",
    "value": "SD"
  },
  {
    "label": "Surilabel",
    "value": "SR"
  },
  {
    "label": "Svalbard and Jan Mayen",
    "value": "SJ"
  },
  {
    "label": "Swaziland",
    "value": "SZ"
  },
  {
    "label": "Sweden",
    "value": "SE"
  },
  {
    "label": "Switzerland",
    "value": "CH"
  },
  {
    "label": "Syrian Arab Republic",
    "value": "SY"
  },
  {
    "label": "Taiwan, Province of China",
    "value": "TW"
  },
  {
    "label": "Tajikistan",
    "value": "TJ"
  },
  {
    "label": "Tanzania, United Republic of",
    "value": "TZ"
  },
  {
    "label": "Thailand",
    "value": "TH"
  },
  {
    "label": "Timor-Leste",
    "value": "TL"
  },
  {
    "label": "Togo",
    "value": "TG"
  },
  {
    "label": "Tokelau",
    "value": "TK"
  },
  {
    "label": "Tonga",
    "value": "TO"
  },
  {
    "label": "Trinidad and Tobago",
    "value": "TT"
  },
  {
    "label": "Tunisia",
    "value": "TN"
  },
  {
    "label": "Turkey",
    "value": "TR"
  },
  {
    "label": "Turkmenistan",
    "value": "TM"
  },
  {
    "label": "Turks and Caicos Islands",
    "value": "TC"
  },
  {
    "label": "Tuvalu",
    "value": "TV"
  },
  {
    "label": "Uganda",
    "value": "UG"
  },
  {
    "label": "Ukraine",
    "value": "UA"
  },
  {
    "label": "United Arab Emirates",
    "value": "AE"
  },
  {
    "label": "United Kingdom",
    "value": "GB"
  },
  {
    "label": "United States",
    "value": "US"
  },
  {
    "label": "United States Minor Outlying Islands",
    "value": "UM"
  },
  {
    "label": "Uruguay",
    "value": "UY"
  },
  {
    "label": "Uzbekistan",
    "value": "UZ"
  },
  {
    "label": "Vanuatu",
    "value": "VU"
  },
  {
    "label": "Venezuela, Bolivarian Republic of",
    "value": "VE"
  },
  {
    "label": "Viet Nam",
    "value": "VN"
  },
  {
    "label": "Virgin Islands, British",
    "value": "VG"
  },
  {
    "label": "Virgin Islands, U.S.",
    "value": "VI"
  },
  {
    "label": "Wallis and Futuna",
    "value": "WF"
  },
  {
    "label": "Western Sahara",
    "value": "EH"
  },
  {
    "label": "Yemen",
    "value": "YE"
  },
  {
    "label": "Zambia",
    "value": "ZM"
  },
  {
    "label": "Zimbabwe",
    "value": "ZW"
  }
]

const dialingcode = [
  {
    label: '+971',
    value: '+971',
  },
  {
    label: '+966',
    value: '+966',
  }
]

class SignUp extends Component {

  constructor(props) {
    super(props)

    setBaseURL("http://ec2-3-16-76-216.us-east-2.compute.amazonaws.com:3000/api/v1");
    this.state = {
      name: '',
      lastName: '',
      email: '',
      password: '',
      wallet: '',
      nationality: undefined,
      birthDate: undefined,
      mobile: '',
      gender: undefined,
      TRNNumber: '',
      checkspinner: true,
      date: new Date(),
      mode: 'date',
      show: false,
      hidedatepicker: false
    }

    const checkdatepicker='';
    //  setI18nConfig();
  }

  // onCreate
  componentWillMount() {
    SplashScreen.hide();
  }

  componentDidMount() {
    RNLocalize.addEventListener('change', this.handleLocalizationChange);
  }

  componentWillUnmount() {
    cancelRequest();
    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
  }


  setDate = (event, date) => {
    date = date || this.state.date;

    this.setState({
      show: Platform.OS === 'ios' ? true : false,
      date,
      birthDate: date,
    });
  }

  setDate2 = () => {

    this.setState({
      show: Platform.OS === 'ios' ? true : false,
      date,
      birthDate: date,
    }); 
  }

  show = mode => {

    this.setState({
      show: true,
      mode,
    });
  }

  hide = mode => {

    this.setState({
      show: false,
      mode,
    });
  }

  hideDate = ()=>{
    this.setState({
      hidedatepicker: false,
    });
    this.hide('date');

  }

  datepicker = () => {

    if(this.state.hidedatepicker){
      console.log('xyz'+this.state.hidedatepicker)
      this.setState({
        hidedatepicker: false,
      });
      this.hide('date');

    }
    else{
      console.log('acx'+this.state.hidedatepicker)
      this.setState({
        hidedatepicker: true,
      });
      this.show('date');
    }

  }


  handleLocalizationChange = () => {
    setI18nConfig();
    this.forceUpdate();
  };




  componentWillReceiveProps(newProps) {
    console.log(newProps)
    if (!newProps.loading) {
      if (newProps.error) {
        alert("Register Error" + newProps.message);
      } else {
        (async () => {
          await this._storePassword();
        })();

      }
    }

  }




  _storePassword = async () => {
    try {
      if (this.state.password && this.state.email) {
        // Alert.alert('Saved', 'Successful'+this.props.message.email);
        await AsyncStorage.setItem('password', this.state.password);
        await AsyncStorage.setItem('email', this.state.email);
        const password = await AsyncStorage.getItem('password');
        const email = await AsyncStorage.getItem('email')
        await AsyncStorage.setItem('logintype', "device");

        // Alert.alert('Saved', 'Suc passav '+password + ' Sav Email: '+email );
        this.props.navigation.navigate("OTP");

      }
      else {
        // Alert.alert('Saved', 'Error in async ');

      }


    } catch (error) {
      // Error saving data
      console.log(error)
    }
  };

  componentDidMount() {
    cancelRequest();
  }






  CheckTextInput = () => {
    //Handler for the Submit onPress
    if (this.state.name != '') {
      if (this.state.email != '') {
        //Check for the Name TextInput

        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (reg.test(this.state.email) === true) {

          if (this.state.lastName != '') {

            if (this.state.password != '') {

              if (this.state.nationality != '') {

                if (this.state.birthDate != '') {

                  if (this.state.mobile != '') {

                    if (this.state.gender != '') {



                      //Check for the Email TextInput
                      this.props.registerUser({
                        name: this.state.name,
                        email: this.state.email,
                        lastName: this.state.lastName,
                        password: this.state.password,
                        wallet: this.state.wallet,
                        nationality: this.state.nationality,
                        birthDate: moment(this.state.date).format('MM-DD-YYYY'),
                        mobile: '+971' + this.state.mobile,
                        gender: this.state.gender,
                        TRNNumber: this.state.TRNNumber,
                        devicekey: "",
                        devicename: "",
                        logintype: "device",
                        google_id: "",
                        fb_id: ""

                      });

                    } else {
                      this.refs.toast.show('please enter gender',200);
                    }

                  } else {
                    this.refs.toast.show('please enter mobile no',200);
                  }

                } else {
                  this.refs.toast.show('please enter birthdate',200);
                }

              } else {
                this.refs.toast.show('please enter nationality',200);
              }

            } else {
              this.refs.toast.show('please enter password',200);
            }

          } else {
            this.refs.toast.show('please enter lastname',200);
          }
        }
        else {
          this.refs.toast.show('email is not valid',200);
        }
      }
      else {
        this.refs.toast.show('please enter email',200);
      }
    }
    else {
      this.refs.toast.show('please enter name',200);

    }
  };








  render() {


    const placeholder_gender = {
      label: translate('signup_Gender'),
      value: null,
      color: '#9EA0A4',
    };
    const placeholder_nationality = {
      label: translate('signup_Nationality'),
      value: null,
      color: '#9EA0A4',
    };
    return (

      <View style={{ flex: 1, backgroundColor: COLORS.Orange }}>

        <Spinner
          visible={this.props.loading}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />

        <ScrollView  >

          <View style={styles.container}>

            <Image
              source={require('../assets/logo.png')}
              resizeMode={"contain"}
              style={{ flex: 0, width: 150, height: 50, alignSelf: 'center', marginTop: 20 }}
            />

            <View
              style={[styles.TextContainer]}>
              <Text style={[styles.signinText, { alignSelf: 'stretch' }]}>{translate('signup_title')}</Text>
            </View>


            <TextInput

              style={[styles.inputContainer, styles.inputs]}
              placeholderTextColor={'white'}
              placeholder={translate('signup_First_Name')}
              onChangeText={(name) => this.setState({ name })}
              underlineColorAndroid='transparent'
              label={"Field 1"}
              blurOnSubmit={false}
              onSubmitEditing={() => this.sirnamefamilynameRef.focus()}
              selectionColor={'green'}
            />

            <TextInput
              style={[styles.inputContainer, styles.inputs]}
              placeholderTextColor={'white'}
              placeholder={translate('signup_Surname_Family_Name')}
              ref={ref => this.sirnamefamilynameRef = ref}
              blurOnSubmit={false}
              onSubmitEditing={() => this.genderRef.togglePicker()}
              underlineColorAndroid='transparent'
              onChangeText={(lastName) => this.setState({ lastName })}
              selectionColor={'green'}
            />
            <RNPickerSelect
              placeholder={placeholder_gender}
              placeholderTextColor='white'
              
              items={gender}
              onValueChange={value => {
                this.setState({
                  gender: value,
                });

              }}
              useNativeAndroidPickerStyle={false}
              InputAccessoryView={() => null}
              style={pickerSelectStyles}
              ref={ref => this.genderRef = ref}
              value={this.state.gender}
            />



            <TextInput
              style={[styles.inputContainer, styles.inputs]} placeholderTextColor={'white'}
              placeholder={translate('signup_Email')}
              keyboardType="email-address"
              onChangeText={(email) => this.setState({ email })}
              underlineColorAndroid='transparent'
              ref={ref => this.emailRef = ref}
              blurOnSubmit={false}
             
              selectionColor={'green'}
            />

            <RNPickerSelect
              placeholder={placeholder_nationality}
              placeholderTextColor='white'
          
              items={nationality}
              onValueChange={value => {
                this.setState({
                  nationality: value,
                });
                this.birthdateRef.focus()
              }}
              useNativeAndroidPickerStyle={false}
              InputAccessoryView={() => null}
              style={pickerSelectStyles}
              ref={ref => this.nationalityRef = ref}
              value={this.state.nationality}
            />

            <TouchableOpacity onPress={this.datepicker}>
              <TextInput
                style={[styles.inputContainer, styles.inputs]} placeholderTextColor={'white'}

                value={this.state.birthDate ? moment(this.state.date).format('MM-DD-YYYY') : translate('signup_DateOfBirth')}
                underlineColorAndroid='transparent'

                onChangeText={(birthDate) => this.setState({ birthDate })}
                ref={ref => this.birthdateRef = ref}
                blurOnSubmit={false}
                pointerEvents="none"
                editable={false}
                onSubmitEditing={() => this.mobileRef.focus()}
              />
            </TouchableOpacity>


            <View style={{ flex: 1, flexDirection: 'row' }}>

              <TextInput
                style={[styles.inputContainerLeftRound, styles.inputs, { flex: 0.2 }]} placeholderTextColor={'white'}
                placeholder={'+971'}
                keyboardType="phone-pad"
                underlineColorAndroid='transparent'
                onChangeText={(mobile) => this.setState({ mobile })}
                ref={ref => this.mobileRef = ref}
                blurOnSubmit={false}
                editable={false}
                onSubmitEditing={() => this.passwordRef.focus()}
              />
              <TextInput
                style={[styles.inputContaineRightRound, styles.inputs, { flex: 0.8 }]} placeholderTextColor={'white'}
                placeholder={translate('signup_Phone_number')}
                keyboardType="phone-pad"
                underlineColorAndroid='transparent'
                onChangeText={(mobile) => this.setState({ mobile })}
                ref={ref => this.mobileRef = ref}
                blurOnSubmit={false}
 
                onSubmitEditing={() => this.passwordRef.focus()}
              />
            </View>


            <TextInput
              style={[styles.inputContainer, styles.inputs]} placeholderTextColor={'white'}
              placeholder={translate('signup_Password')}
              keyboardType="default"
              secureTextEntry={true}
              underlineColorAndroid='transparent'
              onChangeText={(password) => this.setState({ password })}
              ref={ref => this.passwordRef = ref}
              blurOnSubmit={false}
              onSubmitEditing={() => this.TRNNumber.focus()}
            />

            <TextInput
              style={[styles.inputContainer, styles.inputs]} placeholderTextColor={'white'}
              placeholder={translate('signup_TRNNO')}
              keyboardType="default"
              underlineColorAndroid='transparent'
              onChangeText={(TRNNumber) => this.setState({ TRNNumber })}
              ref={ref => this.TRNNumber = ref}
              blurOnSubmit={false}
              onSubmitEditing={Keyboard.dismiss}
            />


            <Toast
              ref="toast"
              style={{ backgroundColor:COLORS.lightblack, borderRadius: 30}}
              position='bottom'
              positionValue={200}
              textStyle={{ color: COLORS.white }}
            />


            <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]}

              onPress={() => {
                this.CheckTextInput()
              }}>
              <Text style={[styles.loginText]}>{translate('signup_SUBMIT')}</Text>
            </TouchableOpacity>


          </View>

        </ScrollView>

        <View style={{ flexDirection: 'column', justifyContent: 'center', bottom: 0, height: 60, backgroundColor: 'rgba(255,255,255,0.2)', width: '100%' }}>
              <Text style={{ color: COLORS.white, alignSelf: 'center', textAlignVertical: 'center' }}>{translate('signup_agree')}</Text>
              <Text style={{ color: COLORS.white, alignSelf: 'center', textAlignVertical: 'center' }}>{translate('signup_termsofservice')}</Text>
        </View>

        {this.state.show && <DateTimePicker value={this.state.date}
          mode={this.state.mode}
          is24Hour={true}
          display="default"
          maximumDate={new Date()}
          onChange={this.setDate}
         />
          
        }
      </View>

    )
  }
}

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: 'rgba(255,255,255,0.2)',
    textAlign: I18nManager.isRTL ? "center" : "left",
    borderRadius: 30,

    marginLeft: 12,
    marginRight: 12,
    marginBottom: 10,
    flexDirection: 'row',
    color: 'white',
    paddingLeft: 30,
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {

    backgroundColor: 'rgba(255,255,255,0.2)',
    textAlign: I18nManager.isRTL ? "center" : "left",
    borderRadius: 30,

    marginLeft: 12,
    marginRight: 12,
    marginBottom: 10,

    color: 'white',
    paddingLeft: 30,

    paddingRight: 30, // to ensure the text is never behind the icon
  },
});

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#FFF'
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: COLORS.Orange,
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: 'rgba(255,255,255,0.2)',
    textAlign: I18nManager.isRTL ? "center" : "left",
    borderRadius: 30,

    marginLeft: 12,
    marginRight: 12,
    marginBottom: 10,
    flexDirection: 'row'
  },
  inputs: {

    paddingLeft: 30,
    paddingRight: 30,
    borderBottomColor: '#FFFFFF',
    flex: 0,
    paddingTop: 10,
    paddingBottom: 10,
    color: 'white',
    fontSize: 16
  },
  inputContainerLeftRound: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: 'rgba(255,255,255,0.2)',
    textAlign: I18nManager.isRTL ? "center" : "left",
    borderBottomLeftRadius: 30,
    borderTopLeftRadius: 30,

    marginLeft: 12,
    marginRight: 2,
    marginBottom: 10,
    flexDirection: 'row'
  }, inputContaineRightRound: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: 'rgba(255,255,255,0.2)',
    textAlign: I18nManager.isRTL ? "center" : "left",
    borderBottomRightRadius: 30,
    borderTopRightRadius: 30,


    marginRight: 12,
    marginBottom: 10,
    flexDirection: 'row'
  },
  inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 15,
    justifyContent: 'center'
  },
  buttonContainer: {

    flexDirection: 'row',
    marginBottom: 10,
    marginLeft: 15,
    marginRight: 15,
    paddingTop: 10,
    paddingBottom: 10,
    borderRadius: 30,
    borderWidth: 0.5,
    borderColor: '#fff'
  },
  loginButton: {
    backgroundColor: "#F55130",
  },
  loginText: {
    flex: 1,
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
    alignSelf: 'center'

  },

  signinText: {
    fontSize: 22,
    color: "white",
    textAlign: 'left',
    marginLeft: 30,
    marginRight: 30
    // <-- the magic
  },
  TextContainer: {
    width: '100%',
    height: 40
  },
  instructions: {
    fontSize: 12,
    textAlign: 'center',
    color: '#888',
    marginBottom: 5
  },
  data: {
    padding: 15,
    marginTop: 10,
    backgroundColor: '#ddd',
    borderColor: '#888',
    borderWidth: 1 / PixelRatio.get(),
    color: '#777'
  },
  countryPickerContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',


    padding: 15,
  },
  buttonRoundSmall: {
    padding: 12,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 30,
    width: width / 2 - 30,
    backgroundColor: COLORS.white
  },
});


const mapStateToProps = ({ signup, loading }) => {

  console.log(signup)
  //Toast.show(signup.message.verificationcode+"");
  // console.warn("Data:"+signup.message.email)
  //Toast.show("call");
  //console.log( 'Map State to props--------- ',signup.message,signup.data)
  return {
    message: signup.message,
    error: signup.error,
    loading: signup.loading,
  };
};


function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({ registerUser }, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps)
  (SignUp);