
import React, { Component } from 'react';
import { removeFromCart,  increaseQuantity  ,decreaseQuantity} from "../store/actions/ProductAction";
import {  getCartTotal } from "../store/reducer/ProductReducer";
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import {cancelRequest} from '../apis';
import SplashScreen from 'react-native-splash-screen';
import { I18nManager, StyleSheet, Text,View, TouchableOpacity, Image, Alert, Dimensions, FlatList, ScrollView,}
    from 'react-native';
    
import   * as RNLocalize  from 'react-native-localize';
import    {setI18nConfig} from './../utils/i18n';
import      {translate } from './../utils/i18n';
import   translationGetters from './../utils/i18n';
import COLORS from '../stylehelper/Color';

 
const width = Dimensions.get('window').width;
  class DriverDetails extends Component {

    componentWillMount() {
        SplashScreen.hide();
      }
    
    constructor(props) {
        super(props);
        setI18nConfig();
    }

    componentDidMount() {
        RNLocalize.addEventListener('change', this.handleLocalizationChange);
      }
    
      componentWillUnmount() {
        cancelRequest();
        RNLocalize.removeEventListener('change', this.handleLocalizationChange);
      }
   
    clickEventListener() {
        Alert.alert("Success", "Product has beed added to cart")
    }

    render() {
        return (

            <View style={[styles.mainContaner]} >

                <View style={[styles.subcontainer1]}>

                    <Text style={styles.name}>{translate('driver_details_title')}</Text>

                    <View style={[styles.lineStyle, { marginTop: 5 }]} />

                    <View style={{flexDirection:'row',marginTop:20}}>

                    <Image source={{ uri: 'https://reactnativecode.com/wp-content/uploads/2017/10/Guitar.jpg' }}
                    style={{
                             alignSelf: 'center',
                             width: 80,
                             height: 80,
                             overflow: "hidden",
                             borderRadius: 100 / 2,
                             flex:0.3}} />

                    <View style={{flex:0.7,flexDirection:'column',padding:15}}>

                    <Text style={{fontSize:20,color:COLORS.Orange}}>Ravi Sanchala</Text>

                    <View style={{ flexDirection:'row'}}>
                    <Text style={{fontSize:16,width:'50%'}}>{translate('driver_details_id')}</Text>
                    <Text style={{fontSize:16,width:'50%'}}>123456</Text>
                    </View>
                    
                    <View style={{  flexDirection:'row'}}>
                    <Text style={{fontSize:16,width:'50%'}}>{translate('driver_details_nationality')}</Text>
                    <Text style={{fontSize:16,width:'50%'}}>India</Text>
                    </View>

                    <View style={{ flexDirection:'row'}}>
                    <Text style={{fontSize:15,width:'50%'}}>{translate('driver_details_mobileno')}</Text>
                    <Text style={{fontSize:15,width:'50%'}}>+97150xxxxxxx</Text>
                    </View>

                    </View>
                    </View>


                    <View style={[styles.squareContainer, {  marginTop: 20 }]}>
                        
                        <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.estimatedtimetoarrive]} >{translate('driver_call_Estimated_time_to_arrive')}</Text>
                        
                        <Text 
                              style={{ flex: 0.20, fontSize:13, alignSelf: "center",color:COLORS.Orange }}
                        >14 mins</Text>

                    </View>   

                    <View >

                    </View>


                </View>
   

                <View style={[{ flexDirection: 'column', flex: 0, width: '100%', bottom: 0 }]}>
                    <View style={styles.bottom}>
                        <TouchableOpacity style={[styles.bottomButton, { backgroundColor: '#74D5F7' }]}   onPress={() => this.props.navigation.navigate('DriverTrack')} >
                            <Text style={[styles.boldtext]}>{translate('driver_view_on_map')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.bottomButton, { backgroundColor: "#F55130" }]}  >
                            <Text style={[styles.boldtext]}>{translate('driver_call_driver')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>


            </View>

        );


    }
}

const styles = StyleSheet.create({

    squareContainer: {
        flexDirection: 'row',
        borderRadius: 10,
        borderColor: 'black',
        padding: 15,
        borderWidth: 0.5
    },
    estimatedtimetoarrive: {
        flex: 0.80,
        fontWeight: 'bold',
        textAlignVertical: 'center'
    },
    mainContaner: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white'
    }, subcontainer1: {
        flex: 1,
        flexDirection: 'column',
        marginHorizontal: 20,
        marginTop: 20
    },
    itemContainer: {
        flexDirection: 'row',
        flex: 1,
        marginTop: 10,
        marginBottom: 10
    }, itemImage: {
        width: 100,
        height: 100,
        marginTop:5,

        alignSelf: "center",

    }, itemMiddleContainer: {
        flexDirection: 'column',
        flex: 1,
        height: '100%',
        marginTop: 5,
        marginBottom: 5
    }, itemLastContaner: {
        flexDirection: 'column',
        flex: 1,
        position: 'absolute',
        height: '100%',
        right: 0,
        justifyContent: 'space-between',
        marginBottom: 10,
        marginTop: 5,
    }, textIncrement: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#F55130',
        alignSelf: 'center',
        padding: 5
    }, quantityContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        borderRadius: 5,
        borderColor: '#f55130',
        borderWidth: 0.5,
        width: 100
    },

    flcontainer: {
        flex: 1,
        paddingTop: 22,
        backgroundColor: '#fff',
    },

    bottomButton: {
        flex: 0.5,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15,
    },

    bottom: {
        flex: 0,
        flexDirection: 'row',
        marginTop: 10
    },
    item: {
    },
    container: {
        flex: 1,
        marginTop: 20,
    },
    productImg: {
        width: 200,
        height: 200,
    },
    name: {
        fontSize: 20,
        color: "black",
        textAlign: 'right',
        alignSelf: 'flex-start',

    },
    lineStyle: {
        borderWidth: 0.5,
        borderColor: '#f55130',
        marginTop: 5
    },

    text: {
        fontSize: 0.03 * width,
        color: "black",
    },
    greytext: {
        marginTop: -3,
        fontSize: 13,
        color: "grey",
        alignSelf: 'flex-start'
    },
    boldtext: {
        fontSize: 0.04 * width,
        color: "white",
        alignSelf: 'center',
        justifyContent: 'center',

        fontWeight: 'bold'
    },

});     
const mapStateToProps = ({ products, loading }) => {
 
    return { cart: products.cart, error: products.error, loading: products.loading ,productCount:products.productCount ,total:getCartTotal(products) };
  }
  
  
  function mapDispatchToProps(dispatch) {
    return {
      ...bindActionCreators({ removeFromCart,increaseQuantity  ,decreaseQuantity}, dispatch)
    }
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(DriverDetails);
  