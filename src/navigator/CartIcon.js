
import React , { Component  }from 'react';
import { Platform } from 'react-native';
import {Dimensions, View, TouchableOpacity, Text, TextInput, StyleSheet, Image, StatusBar } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { connect } from "react-redux";
import { NavigationActions } from 'react-navigation';

import { Badge } from "react-native-elements";

const screen = Dimensions.get('window')


class CartIcon extends Component {

    constructor(props) {
        super(props);
      
    }
    
    render = () => {
        return (
    <View style={{ flexDirection: 'row', position: 'absolute' , right: Platform.OS === 'ios' ? -50 : 15}} >

    <TouchableOpacity style={{ alignSelf: 'center' }} onPress={this.props.onPress}>
        <View>
            <Icon
                name='shopping-cart'
                size={25}
                color="white"
                style={{ margin: 2 }}
            />

            <Badge
                status="success"
                value={ this.props.productCount}
                textStyle={styles.badgeText}
                badgeStyle={styles.badge}
                containerStyle={{ position: 'absolute', top: -4, right: -4 }}
            />

        </View>
    </TouchableOpacity>
    <TouchableOpacity >
        <Image
            source={require('../assets/wallet.png')}
            resizeMode="contain"
            style={{ margin: 2, marginLeft: 15, width: 25, alignSelf: 'center' }}
        />
    </TouchableOpacity>
</View>
  );
};
}

const styles = StyleSheet.create({
    headerLeftIconStyle: {
        marginLeft: 15,
    },
    searchInputContainer: {
        flex: 1,
        borderWidth: 1,
        borderRadius: 20,
        borderColor: '#999',
        flexDirection: 'row',
        backgroundColor: 'white',
    },
    searchInputIconStyle: {

        marginLeft: 10
    }, badgeText: {
        fontSize: 11,
        paddingHorizontal: 0
    }, badge: {
        backgroundColor: 'black',
        borderWidth: 0
    },

});
const mapStateToProps = ({ products, loading }) => {
 
    return { productCount:products.productCount};
  }
  

export default connect(
    mapStateToProps,
    null
  )(CartIcon); 
 