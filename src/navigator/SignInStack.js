import { createStackNavigator } from 'react-navigation';
import SignInScreen from '../screens/Login';
import SignUp from '../screens/SignUp';
import ForgotPassword from '../screens/ForgotPassword';
import OTP from '../screens/OTP';
import EnterPhoneno from '../screens/EnterPhoneno';



export default createStackNavigator(
  {
    SignInScreen: { screen: SignInScreen },
    SignUp: { screen: SignUp },
    ForgotPassword: { screen: ForgotPassword },
    OTP: { screen: OTP },
    EnterPhoneno: { screen: EnterPhoneno },
  } ,    { headerMode: 'none' }
);
