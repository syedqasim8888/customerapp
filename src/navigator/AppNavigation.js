import React from 'react';
import { StackNavigator, DrawerNavigator, createSwitchNavigator } from 'react-navigation';
import { View, TouchableOpacity, Text, TextInput, StyleSheet, Image, I18nManager, } from 'react-native';
import SignInStack from './SignInStack';
import Icon from 'react-native-vector-icons/FontAwesome5';
import DrawerContent from './DrawerContent';
import HomeScreen from '../screens/Home';
import Cart from '../screens/Cart';
import CheckOut from '../screens/CheckOut';
import ShippingAddress from '../screens/ShippingAddress';
import Contactus2 from '../screens/Contactus2';
import WalletTransaction from '../screens/WalletTransaction';
import Wallet from '../screens/Wallet';
import Thankyou from '../screens/Thankyou';
import SelectTime from '../screens/SelectTime';
import Congratulation from '../screens/Congratulation';
import AboutCompany from '../screens/AboutCompany';
import SignUp from '../screens/SignUp';
import AddMoney from '../screens/AddMoney';
import Orders from '../screens/Orders';
import CartIcon from './CartIcon';
import OTP from '../screens/OTP';
import ProductDetails from '../screens/ProductDetails';
import PaymentScreen from '../screens/PaymentScreen';
import ForgotPassword from '../screens/ForgotPassword';
import DriverDetail from '../screens/DriverDetail';
import Settings from '../screens/Settings';
import CustomerProfile from '../screens/CustomerProfile';
import I18t from 'i18n-js';
import OrderDetails from '../screens/OrderDetail';
import FAQ from '../screens/FAQ'
import DriverTrack from '../screens/DriverTrack'
import Notification from '../screens/Notification';



const navigationOptions = navigator => ({
    
    headerStyle: {
        backgroundColor: '#f55130',
    },
    headerLeft: (
        <TouchableOpacity
            onPress={() => {
                navigator.navigation.toggleDrawer();
            }}>

            <Image
                source={require('../assets/menu.png')}
                resizeMode="contain"
                style={[styles.headerLeftIconStyle, { height: 30, width: 30 }]}
            />

        </TouchableOpacity>
    ),
    headerTitle: (

        <View style={{ flexDirection: 'row', flex: 1}} >
            <Image
                source={require('../assets/logo.png')}
                width={20}
                height={20}
                resizeMode="contain"
                style={{  width: 60, height: 40, alignSelf: 'center', flex: 1 }}
            />
            <CartIcon 
            onPress={() => navigator.navigation.navigate('Cart')} />

        </View>

    ),
    drawerLockMode: 'locked-open',
})


const HomeScreen_StackNavigator = StackNavigator({
    //All the screen from the HomeScreen will be indexed here

    HomeScreen: {
        screen: HomeScreen,
        navigationOptions: navigationOptions
    },
    ProductDetails: {
        screen: ProductDetails,
        navigationOptions: navigationOptions

    },
    Cart: {
        screen: Cart,
        navigationOptions: navigationOptions
    },
    SelectTime: {
        screen: SelectTime,
        navigationOptions: navigationOptions
    }, ShippingAddress: {
        screen: ShippingAddress,
        navigationOptions: navigationOptions
    },

    CheckOut: {
        screen: CheckOut,
        navigationOptions: navigationOptions
    },
  
    PaymentScreen: {
        screen: PaymentScreen,
        navigationOptions: {
            header: null,
        },
    }

},
    {
        transitionConfig: () => ({
            transitionSpec: {
                duration: 0,
            },
        }),
    }

);

const Wallet_StackNavigator = StackNavigator({
    Wallet: {
        screen: Wallet,
        navigationOptions: navigationOptions
    }, WalletTransaction: {
        screen: WalletTransaction,
        navigationOptions: navigationOptions
    },
    AddMoney: {
        screen: AddMoney,
        navigationOptions: navigationOptions
    }, PaymentScreen: {
        screen: PaymentScreen,
        navigationOptions: {
            header: null,
        },
    },
    Thankyou: {
        screen: Thankyou,
        navigationOptions: navigationOptions
    }
})
const AboutUs_StackNavigator = StackNavigator({

    AboutCompany: {
        screen: AboutCompany,
        navigationOptions: navigationOptions
    },
})
const CustomerProfile_StackNavigator = StackNavigator({

    CustomerProfile: {
        screen: CustomerProfile,
        navigationOptions: navigationOptions
    },
    // here
})
const OrderDetails_StackNavigaor = StackNavigator({
    //All the screen from the OrderDetails_StackNavigaor will be indexed here
    Orders: {
        screen: Orders,
        navigationOptions: navigationOptions
    },
    OrderDetails: {
        screen: OrderDetails,
        navigationOptions: navigationOptions
    },
    DriverDetail: {
        screen: DriverDetail,
        navigationOptions: navigationOptions
    },
    DriverTrack:{
        screen: DriverTrack,
        navigationOptions: navigationOptions
    }   

});

const FAQ_STACK_NAVIGATOR = StackNavigator({
    FAQ: {
        screen: FAQ,
        navigationOptions: navigationOptions
    }
    
})

const NOTIFICATION_STACK_NAVIGATOR = StackNavigator({
    Notification: {
        screen: Notification,
        navigationOptions: navigationOptions
    }
    
})

const DrawerNavigation = DrawerNavigator(
    {
        HomeScreen: {
            screen: HomeScreen_StackNavigator,
        },
        OrderDetail: {
            screen: OrderDetails_StackNavigaor
        },
        Contactus2: { screen: Contactus2 },
        //Notification: { screen: Notification },
        Wallet: { screen: Wallet_StackNavigator },
        AboutCompany: { screen: AboutUs_StackNavigator },
        CustomerProfile: { screen: CustomerProfile_StackNavigator },
       
        //TODO will replace notification with contactus2
       // Notification: { screen: NOTIFICATION_STACK_NAVIGATOR },
        Settings: { screen: Settings },
        FAQ: { screen: FAQ_STACK_NAVIGATOR }


    },
    {
        contentComponent: DrawerContent,
        drawerPosition: I18nManager.isRTL ? 'right' : 'left',
        drawerWidth: 300,

    }
);

/* 
// if any one want to hide status bar also when drawer opens

const defaultGetStateForAction = DrawerNavigation.router.getStateForAction;


DrawerNavigation.router.getStateForAction = (action, state) => {
  switch (action.type) {
    case 'Navigation/OPEN_DRAWER':
    case 'Navigation/DRAWER_OPENED':

    
      //StatusBar.setHidden(true, 'slide');
      break;
      
    case 'Navigation/CLOSE_DRAWER':
    case 'Navigation/DRAWER_CLOSED':
     // StatusBar.setHidden(false, 'slide');
      break;
    }

  return defaultGetStateForAction(action, state);
};*/

const MainStack = StackNavigator(
    {
        DrawerNavigation: DrawerNavigation,
        Congratulation: { screen: Congratulation }
    },
    {
        headerMode: 'none'
        /*  navigationOptions: navigator => ({
              headerStyle: {
                  backgroundColor: '#f55130',
              },
              headerLeft: (
                  <TouchableOpacity
                      onPress={() => {
                          navigator.navigation.toggleDrawer();
                      }}
                 >
                      <Icon
                          name="bars"
                          size={40}
                          color="white"
                          style={styles.headerLeftIconStyle}
                      />
                  </TouchableOpacity>
              ),
              headerTitle: (   
                  <Icon
                      name="home"
                      size={20}
                      color="#aaa"
                      style={styles.searchInputIconStyle}
                  />
              ),
              drawerLockMode: 'locked-open',
          }),*/
    }
);

let TransitionConfig = () => {

    console.log('IN APP NAVIGATION' + I18t.locale + I18nManager.isRTL)
    return {
        screenInterpolator: ({ position, scene }) => {
            const opacity = position.interpolate({
                inputRange: [scene.index - 1, scene.index],
                outputRange: [0, 1],
            });
            return {
                opacity: opacity,
            };
        },
    };
};



const styles = StyleSheet.create({
    headerLeftIconStyle: {
        marginLeft: 15,
    },
    searchInputContainer: {
        flex: 1,
        borderWidth: 1,
        borderRadius: 20,
        borderColor: '#999',
        flexDirection: 'row',
        backgroundColor: 'white',
    },
    searchInputIconStyle: {

        marginLeft: 10
    }, badgeText: {
        fontSize: 11,
        paddingHorizontal: 0
    }, badge: {
        backgroundColor: 'black',
        borderWidth: 0
    },

});

//products
/*   const mapStateToProps = state => ({
   cartList: state.products.cartList,
   cartCount: state.products.cartCount,
 });
 
 export default connect(mapStateToProps)(CheckoutPage);*/

export default initialRouteName => createSwitchNavigator(
    {
        SignInStack: { screen: SignInStack },
        MainStack: { screen: MainStack },
    },
    {
        initialRouteName,
        headerMode: 'none',
        transitionConfig: TransitionConfig,
    }
);