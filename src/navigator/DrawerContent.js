import React, { Component } from 'react';
import { StackActions, NavigationActions } from 'react-navigation';

import { Linking, I18nManager, Platform, ScrollView, TouchableOpacity, Text, View, Image, PixelRatio, Share } from 'react-native';

import COLORS from '../assets/colors'

import * as RNLocalize from 'react-native-localize';
import { setI18nConfig } from './../utils/i18n';
import { translate } from './../utils/i18n';

import AsyncStorage from '@react-native-community/async-storage';
import { GoogleSignin } from '@react-native-community/google-signin';



class DrawerContent extends Component {

  onShare = async () => {
    try {

      const result = await Share.share({
        message:
        "Hi, Now you can order water online with Better Life APP download our app below \n https://play.google.com/store/apps/details?id=com.betterlife.customerapp&hl=en ",
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  onlogout = async () => {

    const isSignedIn = await GoogleSignin.isSignedIn();

    if (isSignedIn) {
      this.signOut();
    }

    AsyncStorage.clear();

    this.props.navigation.navigate('SignInStack');
  };


  signOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      // this.setState({ user: null }); // Remember to remove the user from your app's state as well


    } catch (error) {
      console.error(error);
    }
  };

  navigateToScreen = route => () => {
    const navigate = NavigationActions.navigate({
      routeName: route,
    });
    this.props.navigation.dispatch(navigate);
  };
  constructor(props) {
    super(props);
    setI18nConfig();
    this.state = {
      name: '',
      channels: [
        { key: 0, screen: 'HomeScreen', title: translate('menu_home'), icon: require('../assets/home.png') },
        { key: 1, screen: 'RankingScreen', title: translate('menu_register'), icon: require('../assets/register.png') },
        { key: 2, screen: 'Orders', title: translate('menu_order'), icon: require('../assets/order.png') },
        { key: 3, screen: 'Wallet', title: translate('menu_wallet'), icon: require('../assets/wallet.png') },
        { key: 4, screen: 'AboutCompany', title: translate('menu_about'), icon: require('../assets/about.png') },
        { key: 5, screen: 'Contactus2', title: translate('menu_contactus'), icon: require('../assets/call.png') },
        //{ key: 5, screen: 'Notification', title: translate('menu_contactus'), icon: require('../assets/call.png') },
        { key: 6, screen: 'FAQ', title: translate('menu_faq'), icon: require('../assets/faq.png') },
        { key: 9, screen: 'Settings', title: translate('menu_settings'), icon: require('../assets/about.png') },
      ]

    };

    this.onPressHandler = this.onPressHandler.bind(this);
    this.navigateToScreen = this.navigateToScreen.bind(this);
    this.retriveUserObject('userobject');

  }
  retriveUserObject = async (key) => {
    try {
      const retrievedItem = await AsyncStorage.getItem(key);
      const email = JSON.parse(retrievedItem).email;
      const name = JSON.parse(retrievedItem).name;
      console.log('Token in home', retrievedItem)

      this.setState({
        name: name,
      });

      return retrievedItem;
    } catch (error) {
      console.log(error);
    }
    return
  }



  componentDidMount() {
    RNLocalize.addEventListener('change', this.handleLocalizationChange);
  }
  componentWillUnmount() {

    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
  }

  onPressHandler() {

    const url = "whatsapp://send?text=${'Better Life'}&phone=${+971549984300}";


    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
        alert('Whatsapp not installled')
        console.log('Can\'t handle url: ' + url);
      } else {
        Linking.openURL(url)
          .catch(err => {
            console.warn('openURL error', err);
          });
      }
    }).catch(err => console.warn('An unexpected error happened', err));

  }

  renderChannelButtons() {

    return this.state.channels.map(({ key, screen, title, icon }) => (

      <View key={key} style={{ flex: 1, justifyContent: 'center' }}>
        <TouchableOpacity
          onPress={this.navigateToScreen(screen)}
        >

          <View style={{ flexDirection: 'row' }}>
            <Image
              source={(icon)}
              width={20}
              height={20}
              resizeMode={"contain"}
              style={{ margin: 12, width: 20, alignSelf: 'center' }}
            />
            <Text style={{ color: 'white', alignSelf: 'center' }}>{title}</Text>

          </View>

        </TouchableOpacity>
        <View style={[styles.lineStyle, { marginBottom: 5 }]} />
      </View>


    ));
  }

  render() {

    return (
      <View style={styles.containerStyle}>

        <TouchableOpacity onPress={this.navigateToScreen('CustomerProfile')}>
          <Image source={{ uri: 'https://reactnativecode.com/wp-content/uploads/2017/10/Guitar.jpg' }}
            style={styles.sideMenuProfileIcon} />
          <View style={{ alignSelf: 'center' }}>
            <Text style={{ color: 'white', marginTop: 8, fontSize: 17 }} >{this.state.name}</Text>
          </View>
        </TouchableOpacity>
        <View style={{ alignSelf: 'center' }}>

          <TouchableOpacity onPress={() => this.onlogout()}   >

            <Text style={{ color: 'black', marginTop: -1, fontSize: 15 }} >{translate('menu_logout')}</Text>
          </TouchableOpacity>
        </View>

        <ScrollView>
          <View style={[styles.lineStyle, { marginTop: 17, marginBottom: 5 }]} />
          {this.renderChannelButtons()}


          <View style={{ flex: 1, justifyContent: 'center' }}>
            <TouchableOpacity
              onPress={() => this.onShare()} >
              <View style={{ flexDirection: 'row' }}>
                <Image
                  source={require('../assets/share.png')}
                  width={20}
                  height={20}
                  resizeMode={"contain"}
                  style={{ margin: 12, width: 20, alignSelf: 'center' }} />
                <Text style={{ color: 'white', alignSelf: 'center' }}>{translate('menu_invite_friend')}</Text>
              </View>
            </TouchableOpacity>
            <View style={[styles.lineStyle, { marginBottom: 5 }]} />
          </View>

          <View style={{ flex: 1, justifyContent: 'center' }}>
            <TouchableOpacity
              onPress={() => this.onPressHandler()}
            >

              <View style={{ flexDirection: 'row' }}>
                <Image
                  source={(require('../assets/whatsapp.png'))}
                  width={20}
                  height={20}
                  resizeMode={"contain"}
                  style={{ margin: 12, width: 20, alignSelf: 'center' }} />
                <Text style={{ color: 'white', alignSelf: 'center' }}>{translate('menu_whatsapp')}</Text>
              </View>

            </TouchableOpacity>
            <View style={[styles.lineStyle, { marginBottom: 5 }]} />
          </View>



          <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', flex: 1, marginHorizontal: 30 }}>

            <TouchableOpacity onPress={() => { Linking.openURL('https://www.facebook.com/betterlifeae/') }} >
              <View style={styles.CircleShapeView}>
                <Image
                  source={require('../assets/f.png')}
                  resizeMode={"contain"}
                  style={{ width: 30, alignSelf: 'center' }}
                />
              </View>
            </TouchableOpacity >
            <TouchableOpacity onPress={() => { Linking.openURL('https://www.linkedin.com/company/happy-life-llc/') }}>
              <View style={styles.CircleShapeView}>
                <Image
                  source={require('../assets/linked.png')}
                  resizeMode={"contain"}
                  style={{ width: 25, alignSelf: 'center' }}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => { Linking.openURL('https://www.instagram.com/betterlifeae/') }}>
              <View style={styles.CircleShapeView}>
                <Image
                  source={require('../assets/insta.png')}
                  resizeMode={"contain"}
                  style={{ width: 20, alignSelf: 'center' }}
                />
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>


      </View>

    );
  }
}

const styles = {
  containerStyle: {

    paddingTop: (Platform.OS === 'ios') ? 30 : 20,
    flex: 1,
    width: '100%',
    backgroundColor: COLORS.bgColor,
  },
  sideMenuProfileIcon:
  {
    alignSelf: 'center',

    width: 100,
    height: 100,
    overflow: "hidden",
    borderRadius: 100 / 2
  },

  lineStyle: {
    borderWidth: 0.5,
    borderColor: '#f48968',
    marginTop: 5,
    marginHorizontal: 15
  },
  CircleShapeView: {
    width: 35,
    height: 35,
    borderRadius: 35 / 2,
    borderColor: 'white',
    backgroundColor: 'white',
    alignSelf: 'center',
    borderWidth: 2,
    marginBottom: 20,
    marginTop: 20,
    justifyContent: 'center',

  },
};

export default DrawerContent;
