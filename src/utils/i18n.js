import React from 'react';
import I18t from 'i18n-js';

import * as RNLocalize from 'react-native-localize';
import memoize from 'lodash.memoize';

import { I18nManager, SafeAreaView, StyleSheet, Text ,AsyncStorage} from 'react-native';

export const translationGetters = {
  // lazy requires (metro bundler does not support symlinks)
  en: () => require('./locales/en.json'),
  ar: () => require('./locales/ar.json'),
};

export const translate = memoize(
  (key, config) => I18t.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key),
);

export const setI18nConfig = async () => {
  // fallback if no available language fits
  let language = await AsyncStorage.getItem("language");
  rtl = false;
  if (language == null) {
    I18nManager.allowRTL(false);
    I18nManager.forceRTL(false);
    language = 'en'
  } else if (language == 'ar') {
    console.log(language)
    I18nManager.allowRTL(true);
    I18nManager.forceRTL(true);
  } else if (language == 'en') {
    console.log(language)
    I18nManager.allowRTL(false);
    I18nManager.forceRTL(false);
  }
  const { languageTag, isRTL } = { languageTag: language, isRTL: false };

  // const {languageTag, isRTL} =
  //RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) ||
  //   fallback;
 
  translate.cache.clear();
 
  I18t.translations = { [languageTag]: translationGetters[language]() };
  I18t.locale = languageTag;
  console.log( 'locale ' + I18t.locale );
};

