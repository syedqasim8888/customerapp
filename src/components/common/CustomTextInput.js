import React, { Component } from 'react';
import { TextInput,View,Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  buttonRound: {
    backgroundColor: 'transparent',
    paddingTop: 12,
    paddingRight: 12,
    paddingBottom: 12,
    paddingLeft: 12,
    borderColor: '#fff',
    borderWidth: 1,
    alignSelf: 'center',
    borderRadius: 25,
    marginTop: 12,
    marginLeft: 10,
    marginRight: 10,
    width: width - 30,
  },
  buttonRoundText: {
    color: '#fff',
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default class CustomTextInput extends Component {
  render = () => {
    return (
        <TouchableOpacity
        activeOpacity={1}
        onPress={()=>this.input.focus()}>
        <View pointerEvents="none">
              <TextInput
                 ref = {(input) => this.input = input}
              />
        </View>
   </TouchableOpacity>
    );
  };
}
