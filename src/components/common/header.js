import React from 'react';
import { Text, View } from 'react-native';

const Header = (props) => {
    const { textStyle, viewStyle } = styles;

    return (
        <View style={[styles.viewContainer,{ marginHorizontal: props.marginHorizontal}]} >
            <Text style={styles.name}>{props.headerText}</Text>
            <View style={[styles.lineStyle ]}></View>
        </View>
    );
};

const styles = {
    viewContainer:{ 
        
        flexDirection: 'column',
         marginTop: 20,
  
        },
    name: {
        fontSize: 20,
        color: "black",
        alignSelf: 'flex-start'
    
      },
      lineStyle: {
        borderWidth: 0.35,
        borderColor: '#f55130',
        marginTop: 5,
      },
};

export default Header;