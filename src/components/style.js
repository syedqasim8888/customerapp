'use strict';
import COLORS from '../assets/colors';
var React = require('react-native');

var {
  StyleSheet,
} = React;

module.exports = StyleSheet.create({

  alwaysred: {
    backgroundColor: 'red',
    height: 100,
    width: 100,
  },
  lineSepHorizontal: {
    borderWidth: 0.5,
    borderColor: COLORS.bgColor,
  },

});