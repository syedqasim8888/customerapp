import {
  FETCH_FAQ_PENDING ,
  FETCH_FAQ_SUCCSESS,
  FETCH_FAQ_FAILURE ,
  } from "./types";
  import { fetch, POST ,GET} from "../../apis";
 
  
  
  
  
  export const getFAQS = ( isArabic) => {
    return async dispatch => {
      dispatch({
        type: FETCH_FAQ_PENDING
      });
      let parameters = { isArabic };
      fetch(GET, "getfaqs", parameters)
        .then(response => {
          console.log(response);
          dispatch({
            type: FETCH_FAQ_SUCCSESS,
            payload: {
              data: response.data
            }
          });
        })
        .catch(err => {
          dispatch({
            type: FETCH_FAQ_FAILURE,
            payload: {
              message: err
            }
          });
        });
    };
  };
  
  
 