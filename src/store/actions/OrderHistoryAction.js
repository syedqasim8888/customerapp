import {  ORDER_HISTORY_PENDING , 
    ORDER_HISTORY_SUCCESS,
    ORDER_HISTORY_FAILURE } from "./types";
import { fetch, GET ,POST} from "../../apis";



export const  fetchAllOrders  = (  customerid  ) => {
    return async dispatch => {
        dispatch({
          type: ORDER_HISTORY_PENDING
        });
        // here static shipping ID 
     
      let parameters = {}
        fetch(GET, "productorders/getProductByCustomer/"+customerid ,parameters  )
          .then(response => {
            console.log( 'SHIPPPING ADDRESS ' +response);
            dispatch({
              type: ORDER_HISTORY_SUCCESS,
              payload: {
                data: response.data,
                error :false
              }
            });
          })
          .catch(err => {
            dispatch({
              type: ORDER_HISTORY_FAILURE,
              payload: {
                message: err.message,
                error :err.success
              }
            });
          });
      };

}; 