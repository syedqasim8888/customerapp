import { CONTACTUS_FALIURE,CONTACTUS_SUCCSESS,CONTACTUS_PENDING } from "./types";
import { fetch, POST } from "../../apis";

export const ContactUs = ({  name, email,phonenumber,message,customerid  }) => {
  return async dispatch => {
    dispatch({
      type: CONTACTUS_PENDING
    });
    let parameters = { name, email,phonenumber,message,customerid };
    fetch(POST, "contactusform", parameters)
      .then(response => {
        console.log(  response.data._id);              
        dispatch({
          type: CONTACTUS_SUCCSESS,
          payload: {
            userdata: response.data,
          }
        });
      })
      .catch(err => {
        dispatch({
          type: CONTACTUS_FALIURE,
          payload: {
            message: err,
          }
        });
      });
  };
};