import {GMAIL_SIGNUP,GMAIL_SIGNUP_FAILURE,GMAIL_SIGNUP_SUCCESS,TOKEN,TOKEN_FAILURE,TOKEN_SUCCESS,
LOGIN, LOGIN_SUCCESS,LOGIN_FAILURE, FB_SIGNUP,FB_SIGNUP_SUCCESS, FB_SIGNUP_FAILURE } from "./types";
import { fetch, POST } from "../../apis";

export const loginUser = ({ email, password ,devicekey,devicename }) => {
  return async dispatch => {
    dispatch({
      type: LOGIN
    });
    let parameters = { email, password,devicekey,devicename };
    fetch(POST, "users/loginuser", parameters)
      .then(response => {
        console.log(  response.data.wallet);
              
        dispatch({
          type: LOGIN_SUCCESS,
          payload: {
            userdata: response.data,
          }
        });
      })
      .catch(err => {
        dispatch({
          type: LOGIN_FAILURE,
          payload: {
            message: err,
          }
        });
      });
  };
};

export const fetchToken = ({email,password}) => {
  return async dispatch => {
    dispatch({
      type: TOKEN
    });
    //console.warn("email: "+email+"password: "+password)
    let parameters = {email, password};
    fetch(POST, "authenticate", parameters)
      .then(response => {  
     dispatch({
          type: TOKEN_SUCCESS,
          payload: {
            savetoken: response.data,
            success: response.success
          }
        });
      })
      .catch(err => {
        dispatch({
          type: TOKEN_FAILURE,
          payload: {
            message: err
          }
        });
      });
  };
};

export const registerUserGmail = ({ name, lastName, email, password, wallet,
   nationality, birthDate, mobile, gender, TRNNumber,devicekey,
   devicename,logintype,googleid,fb_id  }) => {
  return async dispatch => {
    dispatch({
      type: GMAIL_SIGNUP
    });

    let parameters = { name, lastName, email, password, wallet,gender, nationality, birthDate, mobile, TRNNumber,devicekey,devicename,logintype,googleid,fb_id };
    fetch(POST, "register", parameters)
      .then(response => {
        console.log('IN SIGNUP GMAIL then', response);
        dispatch({
          type: GMAIL_SIGNUP_SUCCESS,
          payload: {
            userdata_gmail: response.data,
            success: response.success,
            varified:response.varified
          }
        });
      })
      .catch(err => {
        dispatch({
          type: GMAIL_SIGNUP_FAILURE,
          payload: {
            message: err
          }
        });
      });
  };
};
/*
    name: this.state.name,
                                      email: this.state.email,
                                      lastName: "",
                                      password: json.id,
                                      wallet: "",
                                      nationality: "",
                                      birthDate: "",
                                      mobile: "",
                                      gender: "",
                                      TRNNumber: "",
                                      devicekey: "",
                                      devicename: "",
                                      logintype: "facebook",
                                      googleid: "",
                                      fb_id: json.id
*/
export const registerUserFacebook = ({ name, lastName, email, password, wallet,
  nationality, birthDate, mobile, gender, TRNNumber,devicekey,
  devicename,logintype,googleid,fb_id  }) => {
 return async dispatch => {
   dispatch({
     type: FB_SIGNUP
   });

   let parameters = { name, lastName, email, password, wallet,gender, nationality, birthDate, mobile, TRNNumber,devicekey,devicename,logintype,googleid,fb_id };
   fetch(POST, "register", parameters)
     .then(response => {
       console.log('IN SIGNUP FACEBOOK then', response);
       dispatch({
         type: FB_SIGNUP_SUCCESS,
         payload: {
           userdata_facebook: response.data,
           success: response.success,
           varified:response.varified
         }
       });
     })
     .catch(err => {
       dispatch({
         type: FB_SIGNUP_FAILURE,
         payload: {
           message: err
         }
       });
     });
 };
};