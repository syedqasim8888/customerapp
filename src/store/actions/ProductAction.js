import {
  FETCH_PRODUCTS_PENDING, FETCH_PRODUCTS_SUCCSESS,
  FETCH_PRODUCTS_FALIURE, ADD_TO_CART, REMOVE_FROM_CART, INCREASE_QTY, DECREMENT_QTY,
  PLACE_ORDER_FAILURE, PLACE_ORDER_SUCCSESS, PLACE_ORDER_PENDING, CLEAR_CART,
  FETCH_CATEGORY_PENDING, FETCH_CATEGORY_SUCCSESS, FETCH_CATEGORY_FAILURE,
  GET_WALLET_BALANCE_CHECKOUT_PENDING,
GET_WALLET_BALANCE_CHECKOUT_SUCCESS,
GET_WALLET_BALANCE_CHECKOUT_FAILURE
} from "./types";
import { fetch, GET, POST } from "../../apis";


export const increaseQuantity = (id, qty) => {
  return dispatch => {
    dispatch({
      type: INCREASE_QTY,
      payload: {
        id: id,
        qty: qty

      }
    });
  }

}

export const decreaseQuantity = (id, qty) => {
  return dispatch => {
    dispatch({
      type: DECREMENT_QTY,
      payload: {
        id: id,
        qty: qty

      }
    });
  }

}

export const removeFromCart = (id) => {
  return dispatch => {
    dispatch({
      type: REMOVE_FROM_CART,
      payload: {
        id: id,

      }
    });
  }
}

export const clearCart = (id) => {
  return dispatch => {
    dispatch({
      type: CLEAR_CART,
      payload: {
        id: id,

      }
    });
  }
}



export const addtoCart = (id, qty) => {

  return dispatch => {
    dispatch({
      type: ADD_TO_CART,
      payload: {
        id: id,
        qty: qty
      }
    });
  }

};



export const fetchProducts = () => {
  return async dispatch => {
    dispatch({
      type: FETCH_PRODUCTS_PENDING
    });
    let parameters = {};
    fetch(GET, "products", parameters)
      .then(response => {
        console.log(response);
        dispatch({
          type: FETCH_PRODUCTS_SUCCSESS,
          payload: {
            products: response.data
          }
        });
      })
      .catch(err => {
        dispatch({
          type: FETCH_PRODUCTS_FALIURE,
          payload: {
            error: err
          }
        });
      });
  };
};


// Place Order 
/*
{
        "uniqueorderid": "100006",
        "customerid": "5d564209ab95e07a445d2cb6",
        "shippingid": "5d56437fc91a1e919098caab",
        "tax": "5d563fe6ab95e07a445d2cb3",
        "deliverytype": "Free",
        "deliverycost": null,
        "orderstatus": "open",
        "paymentmode": "COD",
        "qty":7,
        "paymentrefrence":"",
        "promoCode":"",
        "total":32.50,
        "shippingtimeIisOneTime":true,
                "shippingtimeIisReccuring":false,
                "shippingtimetimeSlot":"9am - 11am",
                "shippingDate": "14/10/2019",
        "products": [
                {
                        "productid": "5d4e987cec0536679414e88d",
                        "categoryid": "5d735461dba6fbee94440b35",
                        "qty": 7,
                        "price": 32.30,
                        "currencytypes": "AED",
                        "discount": null
                },
                {
                        "productid": "5d7359d09e4f6e71f458d922",
                        "categoryid": "5d735461dba6fbee94440b35",
                        "qty": 7,
                        "price": 32.30,
                        "currencytypes": "AED",a
                        "discount": null
                }
        ]
}
*/

export const placeOrder = ({ uniqueorderid, customerid, shippingid, tax, deliverytype, deliverycost, orderstatus, paymentmode,
  qty, paymentrefrence, promoCode, total, shippingtimeIisOneTime, shippingtimeIisReccuring, shippingtimetimeSlot, shippingDate,
  products ,reccuringPattern}) => {
  return async dispatch => {
        return new Promise(function (resolve, reject) {
 
    dispatch({
      type: PLACE_ORDER_PENDING
    });
    let parameters = {
      uniqueorderid, customerid, shippingid, tax, deliverytype, deliverycost, orderstatus, paymentmode,
      qty, paymentrefrence, promoCode, total, shippingtimeIisOneTime, shippingtimeIisReccuring, shippingtimetimeSlot, shippingDate,
      products,reccuringPattern
    };
    fetch(POST, "productorders/addmultipleproductorder/", parameters)
      .then(response => {
        console.log(response);
        dispatch({
          type: PLACE_ORDER_SUCCSESS,
          payload: {
            data: response.data
          }
        });
        resolve(response);
      })
      .catch(err => {
        console.log(err)
    
        dispatch({
          type: PLACE_ORDER_FAILURE,
          payload: {
            message: err.message,
            error: err.success
          }
        });
        reject(err)
      });
  
    })  };
};


export const fetchAllCategory = () => {
  return async dispatch => {
    dispatch({
      type: FETCH_CATEGORY_PENDING
    });
    let parameters = {};
    fetch(GET, "categories", parameters)
      .then(response => {
        console.log(response);
        dispatch({
          type: FETCH_CATEGORY_SUCCSESS,
          payload: {
            categories: response.data
          }
        });
      })
      .catch(err => {
        dispatch({
          type: FETCH_CATEGORY_FAILURE,
          payload: {
            error: err
          }
        });
        reject(error.response);
      });
  };
};





/// GET CURRENT   WALLET  BALANCE 

export function getWalletBalance(customerid) {
  return async function (dispatch) {
    return new Promise(function (resolve, reject) {
      {
        dispatch({
          type: GET_WALLET_BALANCE_CHECKOUT_PENDING
        });
        let parameters = {};
        fetch(GET, "userwallet/getWalletBalanceByUserId/" + customerid, parameters)
          .then(response => {
            console.log(response);
            dispatch({
              type: GET_WALLET_BALANCE_CHECKOUT_SUCCESS,
              payload: {
                data: response
              }
            });
            resolve(response);
          })
          .catch(err => {
            dispatch({
              type: GET_WALLET_BALANCE_CHECKOUT_FAILURE,
              payload: {
                message: err
              }
            });
            reject(error.response);
          });



      }
    })
  }
}
