import {  SHIPPING_ADDRESS_FETCH_PENDING  , SHIPPING_ADDRESS_FETCH_SUCCSESS , SHIPPING_ADDRESS_FETCH_FAILURE 
     , SHIPPING_ADDRESS_SUBMIT_PENDING , SHIPPING_ADDRESS_SUBMIT_SUCCSESS, SHIPPING_ADDRESS_SUBMIT_FAILURE,
    
     FETCH_CITY_PENDING ,
     FETCH_CITY_SUCCSESS,
     FETCH_CITY_FAILURE 
    } from "./types";
import { fetch, GET ,POST} from "../../apis";

export const  fetchShippingAddress  = (  customerid  ) => {
    return async dispatch => {
        dispatch({
          type: SHIPPING_ADDRESS_FETCH_PENDING
        });
        // here static shipping ID 
     
      let parameters = {}
        fetch(GET, "shippings/getShipingByUserId/"+customerid ,parameters  )
          .then(response => {
            console.log( 'SHIPPPING ADDRESS ' +response);
            dispatch({
              type: SHIPPING_ADDRESS_FETCH_SUCCSESS,
              payload: {
                data: response.data,
                error :false
              }
            });
          })
          .catch(err => {
            dispatch({
              type: SHIPPING_ADDRESS_FETCH_FAILURE,
              payload: {
                message: err.message,
                error :err.success
              }
            });
          });
      };

}; 


export const fetchCitiesList = (     ) => {
  return async dispatch => {
      dispatch({
        type: FETCH_CITY_PENDING
      });
      // here static shipping ID 
   
    let parameters = {}
      fetch(GET, "productorders/productOrderCities" ,parameters  )
        .then(response => {
          console.log( 'FETCH_CITY_SUCCSESS' +response);
          dispatch({
            type: FETCH_CITY_SUCCSESS,
            payload: {
              data: response.data,
              error :false
            }
          });
        })
        .catch(err => {
          dispatch({
            type: FETCH_CITY_FAILURE,
            payload: {
              message: err.message,
              error :err.success
            }
          });
        });
    };

}; 
/*

{
        "name": "XYZ Customera",
        "customerid": "5d85dc3de174d82dc8253992",
        "phonenumber": "97144368888",
        "email": "sheldycoop1990a@gmail.com",
"buildingName":"",
"street":"",
"area":"",
"landmark":"",
        "city": "Dubai"

}

{
	"name": "XYZ Customer",
	"customerid": "5d564209ab95e07a445d2cb6",
	"phonenumber": "97144368888",
  "email": "test@gmail.com",
  
	"address1": "Sheikh Mohammed bin Rashid Blvd",
	"address2": " Dubai - United Arab Emirates",
    	"city": "Dubai"
}
*/
export const submitShippingAddress = ({ address1,address2, phonenumber, flatNumber, buildingName,street,area,landmark,city,name ,customerid ,email}) => {
  return async dispatch => {
  return new Promise(function (resolve, reject) {
  
  
      dispatch({
        type: SHIPPING_ADDRESS_SUBMIT_PENDING
      });
      let parameters = { address1,address2,  phonenumber, flatNumber, buildingName,street,area,landmark,city,name ,customerid ,email};
      fetch(POST, "shippings/addshipping/", parameters)
        .then(response => {
          console.log(response);
          
          dispatch({
            type: SHIPPING_ADDRESS_SUBMIT_SUCCSESS,
            payload: {
              data: response.data
            }
          });
          resolve(response.data)
        })
        .catch(err => {
          dispatch({
            type: SHIPPING_ADDRESS_SUBMIT_FAILURE,
            payload: {
              message: err.message,
              error :err.success
            }
          });
          reject(err)
        });
      })  };
    };
 