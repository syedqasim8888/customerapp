import {
  GET_PAYMENT_URL,
  GET_PAYMENT_URL_SUCCESS,
  GET_PAYMENT_URL_FAILURE,
  PAYMENT_TELR_RESPONSE_PENDING,
  PAYMENT_TELR_RESPONSE_FAILURE,
  PAYMENT_TELR_RESPONSE_SUCCESS,
  PLACE_ORDER_PAYMENT_PENDING,
  PLACE_ORDER_PAYMENT_SUCCSESS,
  PLACE_ORDER_PAYMENT_FAILURE,

  RECHARGE_WALLET_PENDING,
  RECHARGE_WALLET_SUCCSESS,
  RECHARGE_WALLET_FAILURE,
  GET_PAYMENT_URL_FROM_API ,        
  GET_PAYMENT_URL_FROM_API_SUCCESS ,
  GET_PAYMENT_URL_FROM_API_FAILURE 
} from "./types";
import { fetch, POST } from "../../apis";

const queryString = require('query-string');


export const getPaymentUrlfromAPI =({  customerid,  amount }) =>{
  
  return async dispatch => {
    
    dispatch({
      type: GET_PAYMENT_URL_FROM_API
    });
    let parameters = {
  customerid,  amount 
    };
    fetch(POST, "getTelrUrl", parameters)
      .then(response => {
   
        console.log(response);
        dispatch({
          type: GET_PAYMENT_URL_FROM_API_SUCCESS,
          payload: {
            data: response
          }
        });
      })
      .catch(err => {
        console.log(err)
        dispatch({
          type: GET_PAYMENT_URL_FROM_API_FAILURE,
          payload: {
            message: err.message,
            error: err.success
          }
        });
      });
  };
}


export const getPaymentURL = ({
  ivp_method, ivp_store, ivp_authkey, ivp_cart, ivp_test, ivp_amount, ivp_currency, ivp_desc, return_auth, return_can, return_decl
  , bill_fname, bill_sname, bill_addr1, bill_city, bill_country, bill_email }) => {
  return async dispatch => {
    dispatch({
      type: GET_PAYMENT_URL
    });
    let parameters = {
      ivp_method, ivp_store, ivp_authkey, ivp_cart, ivp_test, ivp_amount, ivp_currency, ivp_desc, return_auth, return_can, return_decl, bill_fname, bill_sname, bill_addr1, bill_city, bill_country, bill_email
    };
    fetch(POST, "order.json", queryString.stringify(parameters))
      .then(response => {
        console.log(response);
        dispatch({
          type: GET_PAYMENT_URL_SUCCESS,
          payload: {
            data: response
          }
        });
      })
      .catch(err => {
        dispatch({
          type: GET_PAYMENT_URL_FAILURE,
          payload: {
            message: err
          }
        });
      });
  };
};





export const getTELRResponse = ({ typeOfTransaction, telrRefrance, customerid }) => {
  return async dispatch => {
    dispatch({
      type: PAYMENT_TELR_RESPONSE_PENDING
    });
    let parameters = { typeOfTransaction, telrRefrance, customerid };
    fetch(POST, "checkTelrTransaction", parameters)
      .then(response => {
        console.log('Message :'+response.message);
        if(response.message == "fail"){
          dispatch({
            type: PAYMENT_TELR_RESPONSE_FAILURE,
            payload: {
              message: response.error
            }
          });
        }
        else if (response.message == "success"){
        dispatch({
          type: PAYMENT_TELR_RESPONSE_SUCCESS,
          payload: {
            data: response
          }
        });
      }
      else {
        
      }
      })
      .catch(err => {
        dispatch({
          type: PAYMENT_TELR_RESPONSE_FAILURE,
          payload: {
            message: err
          }
        });
      });
  };
};



export const placeOrder = ({ uniqueorderid, customerid, shippingid, tax, deliverytype, deliverycost, orderstatus, paymentmode,
  qty, paymentrefrence, promoCode, total, shippingtimeIisOneTime, shippingtimeIisReccuring, shippingtimetimeSlot, shippingDate,
  products ,reccuringPattern}) => {
    
  return async dispatch => {
    dispatch({
      type: PLACE_ORDER_PAYMENT_PENDING
    });
    let parameters = {
      uniqueorderid, customerid, shippingid, tax, deliverytype, deliverycost, orderstatus, paymentmode,
      qty, paymentrefrence, promoCode, total, shippingtimeIisOneTime, shippingtimeIisReccuring, shippingtimetimeSlot, shippingDate,
      products,reccuringPattern
    };
    fetch(POST, "productorders/addmultipleproductorder/", parameters)
      .then(response => {
   
        console.log(response);
        dispatch({
          type: PLACE_ORDER_PAYMENT_SUCCSESS,
          payload: {
            data: response.data
          }
        });
      })
      .catch(err => {
        console.log(err)
        dispatch({
          type: PLACE_ORDER_PAYMENT_FAILURE,
          payload: {
            message: err.message,
            error: err.success
          }
        });
      });
  };
};

 
export const rechargeWallet = ({ customerid, telrTransactionId, totalAmountTopUp, promoCode, entrytype }) => {
  return async dispatch => {
    dispatch({
      type: RECHARGE_WALLET_PENDING
    });
    let parameters = { customerid, telrTransactionId, totalAmountTopUp, promoCode, entrytype };
    fetch(POST, "userwallet/rechargeWallet", parameters)
      .then(response => {
        console.log(response);
        dispatch({
          type: RECHARGE_WALLET_SUCCSESS,
          payload: {
            data: response.data
          }
        });
      })
      .catch(err => {
        console.log(err)
        dispatch({
          type: RECHARGE_WALLET_FAILURE,
          payload: {
            message: err.message,
            error: err.success
          }
        });
      });
  };
};
