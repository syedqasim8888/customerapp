import { TOKEN, TOKEN_SUCCESS, TOKEN_FAILURE, SIGNUP, SIGNUP_FAILURE, SIGNUP_SUCCESS, OTP_DEFAULT, OTP_VERIFICATION_FAILURE, OTP_VERIFICATION_SUCCESS } from "./types";
import { fetch, POST } from "../../apis";

export const checkOTP = (manual, recived) => {

  return dispatch => {
    dispatch({
      type: OTP_DEFAULT,
      payload: {
        manual: manual,
        recived: recived
      }
    });
  }

};

export const fetchToken = ({email,password}) => {
  return async dispatch => {
    dispatch({
      type: TOKEN
    });
   // console.warn("email: "+email+"password: "+password)
    let parameters = {email, password};
    fetch(POST, "authenticate", parameters)
      .then(response => {
     dispatch({
          type: TOKEN_SUCCESS,
          payload: {
            savetoken: response.data,
            success: response.success
          }
        });
      })
      .catch(err => {
        dispatch({
          type: TOKEN_FAILURE,
          payload: {
            message: err
          }
        });
      });
  };
};

export const registerUser = ({ name, lastName, email, password, wallet, nationality, birthDate, mobile, gender, TRNNumber,devicekey,devicename,logintype,googleid,fb_id }) => {
  return async dispatch => {
    dispatch({
      type: SIGNUP
    });

    let parameters = { name, lastName, email, password, wallet, nationality, birthDate, mobile, gender, TRNNumber,devicekey,devicename,logintype,googleid,fb_id };
    fetch(POST, "register", parameters)
      .then(response => {
        console.log('IN SIGNUP then', response);
        dispatch({
          type: SIGNUP_SUCCESS,
          payload: {
            userregister: response.data,
            success: response.success
          }
        });
      })
      .catch(err => {
        dispatch({
          type: SIGNUP_FAILURE,
          payload: {
            message: err
          }
        });
      });
  };
};