import { 
    GET_WALLET_BALANCE_PENDING,
    GET_WALLET_BALANCE_SUCCESS,
    GET_WALLET_BALANCE_FAILURE,
    GET_WALLET_TRANSACTION_PENDING,
GET_WALLET_TRANSACTION_SUCCESS,
GET_WALLET_TRANSACTION_FAILURE
  } from "./types";
  import { fetch, POST, GET } from "../../apis";


  export const getWalletBalance = ( customerid) => {
    return async dispatch => {
      dispatch({
        type: GET_WALLET_BALANCE_PENDING
      });
      let parameters = {   };
      fetch(GET, "userwallet/getWalletBalanceByUserId/" +customerid,     parameters  )
        .then(response => {
          console.log(response);
          dispatch({
            type: GET_WALLET_BALANCE_SUCCESS,
            payload: {
              data: response
            }
          });
        })
        .catch(err => {
          dispatch({
            type: GET_WALLET_BALANCE_FAILURE,
            payload: {
              message: err
            }
          });
        });
    };
    };



    export const getWalletTransaction = ( customerid) => {
      return async dispatch => {
        dispatch({
          type: GET_WALLET_TRANSACTION_PENDING
        });
        let parameters = {   };
        fetch(GET, "userwallet/getWalletTransactionByUserId/" +customerid,     parameters  )
          .then(response => {
            console.log(response);
            dispatch({
              type: GET_WALLET_TRANSACTION_SUCCESS,
              payload: {
                data: response
              }
            });
          })
          .catch(err => {
            dispatch({
              type: GET_WALLET_TRANSACTION_FAILURE,
              payload: {
                message: err
              }
            });
          });
      };
      };