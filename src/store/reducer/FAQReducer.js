import {
    FETCH_FAQ_PENDING ,
    FETCH_FAQ_SUCCSESS,
    FETCH_FAQ_FAILURE ,
} from "../actions/types";


import { REHYDRATE } from 'redux-persist';
const initialState = {


    message: null,
    data: [],
    result: [],
    success:null,
    error:null
 
};

export default (state = initialState, action) => {
    switch (action.type) {

        case REHYDRATE:
            return  state
             


        case FETCH_FAQ_PENDING:
            console.log("GET_WALLET_BALANCE_PENDING  ", action);
            return {
                ...state,
                loading: true,
                success:null

            };


        case FETCH_FAQ_SUCCSESS:

            console.log("GET_WALLET_BALANCE_SUCCESS", action);

            return {
                ...state,
                loading: false,
                data: action.payload.data,
                success:"success"
             
            };

        case FETCH_FAQ_FAILURE:
            console.log("GET_WALLET_BALANCE_FAILURE", action.payload.error);
            return {
                ...state,
                loading: false,
                error: action.payload.error,
                message: action.payload.message,
                success:"failure"
            };




 

        default:
            console.log("Wallet Default : ", action);
            return state;
    }
};

