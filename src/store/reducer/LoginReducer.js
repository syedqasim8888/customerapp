import {FB_SIGNUP,FB_SIGNUP_SUCCESS,FB_SIGNUP_FAILURE,GMAIL_SIGNUP,GMAIL_SIGNUP_SUCCESS,GMAIL_SIGNUP_FAILURE,TOKEN,TOKEN_FAILURE,TOKEN_SUCCESS,LOGIN,LOGIN_SUCCESS,LOGIN_FAILURE} from "../actions/types";
import { REHYDRATE } from 'redux-persist';

const initialState = {
  name: "",
  message: "",
  email: "",
  pwd: "",
  loading: false,
  loadingotp: false,
  userdata:[],
  user_gmaildata:[],
  user_facebookdata:[],
  tokenstring:null,
  err:false,
  errtoken:false,
  varified:false,
  casetype:0,
  case:null,
  gmailsuccsess:false,
  facebooksuccsess:false,
  loginsuccess:false
};
export default (state = initialState, action) => {
  switch (action.type) {

    case REHYDRATE:
        return {...state,
          varified:false,
          loading: false,
          casetype:0,
          case:null,
          gmailsuccsess:false,
          loginsuccess:false
      };
 

    case LOGIN:
    console.log("LOGIN  ", action);
      return {
        ...state,
        loading: true,
        err: false,
        case:'LOGIN',
        loginsuccess:false
      };  

    case LOGIN_SUCCESS:
    console.log("LOGIN Success", action);
      return {
        ...state,
        loading: false,
        userdata: action.payload.userdata,
        err: false,
        casetype:1,
        case:'LOGIN',
        loginsuccess:true

      };

    case LOGIN_FAILURE:
      console.log("LOGIN FAILURE", action);
      return {
        ...state,
        loading: false,
        err: true,
        case:'LOGIN',
        loginsuccess:false
        
      };

      case TOKEN:
        console.log("SIGNUP", action);
        return {
          ...state,
          loading: true,
          errtoken:false,
          case:'TOKEN'

        };
  
      case TOKEN_SUCCESS:
        console.log("TOKEN SUCCESS", action);
        return {
          ...state,
          loading: false,
          tokenstring: action.payload.savetoken,
          errtoken:false,
          casetype:1,
          case:'TOKEN'
        };
  
      case TOKEN_FAILURE:
        console.log("TOKEN FAILURE", action);
        return {
          ...state,
          loading: false,
          errtoken:true,
          case:'TOKEN'
        };

      case GMAIL_SIGNUP:
      console.log("SIGNUP_GMAIL", action);
      return {
        ...state,
        loading: true,
        err: false,
        varified:false,
        case:'GMAIL',
        gmailsuccsess:false
      };

    case GMAIL_SIGNUP_SUCCESS:
      console.log("SIGNUP_GMAIL SUCCESS", action.payload.varified);
      return {
        ...state,
        loading: false,
        user_gmaildata: action.payload.userdata_gmail,
        varified: action.payload.varified,
        err: false,
        casetype:2,
        case:'GMAIL',
        gmailsuccsess:true
      };

    case GMAIL_SIGNUP_FAILURE:
      console.log("SIGNUP_GMAIL FAILURE", action);
      return {
        ...state,
        loading: false,
        message: action.payload.message.message,
        varified:false,
        err: true,
        case:'GMAIL',
        gmailsuccsess:false
      };

      case FB_SIGNUP:
          console.log("SIGNUP_FACEBOOK", action);
          return {
            ...state,
            loading: true,
            err: false,
            varified:false,
            case:'FACEBOOK',
            facebooksuccsess:false
          };
    
        case FB_SIGNUP_SUCCESS:
          console.log("SIGNUP_FACEBOOK SUCCESS", action.payload.varified);
          return {
            ...state,
            loading: false,
            user_facebookdata: action.payload.userdata_facebook,
            varified: action.payload.varified,
            err: false,
            casetype:2,
            case:'FACEBOOK',
            facebooksuccsess:true
          };
    
        case FB_SIGNUP_FAILURE:
          console.log("SIGNUP_FACEBOOK FAILURE", action);
          return {
            ...state,
            loading: false,
            message: action.payload.message.message,
            varified:false,
            err: true,
            case:'FACEBOOK',
            facebooksuccsess:false
          };

    default:
    console.log("Default state", action);
        return {...state,
          varified:false,
          casetype:0,
          case:null,
          gmailsuccsess:false,
          loginsuccess:false,
          facebooksuccsess:false,
          loading: false,
      };
  }
};