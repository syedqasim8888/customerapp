import {
  FETCH_PRODUCTS_PENDING, FETCH_PRODUCTS_SUCCSESS,
  PLACE_ORDER_FAILURE, PLACE_ORDER_SUCCSESS, PLACE_ORDER_PENDING, CLEAR_CART,
  FETCH_CATEGORY_PENDING, FETCH_CATEGORY_SUCCSESS, FETCH_CATEGORY_FAILURE,
  FETCH_PRODUCTS_FALIURE, ADD_TO_CART, REMOVE_FROM_CART, INCREASE_QTY, DECREMENT_QTY,
  GET_WALLET_BALANCE_CHECKOUT_PENDING,
  GET_WALLET_BALANCE_CHECKOUT_SUCCESS,
  GET_WALLET_BALANCE_CHECKOUT_FAILURE
} from "../actions/types";
import { REHYDRATE } from 'redux-persist';

const initialState = {

  productCount: null,
  cart: [],
  products: [],
  error: null,
  orderData: [],
  orderMsg: null,
  status: null,
  orderLoading: null,
  loading: false,
  categoryList: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case REHYDRATE:
      return {
        ...state,
        status: null
      };
    case FETCH_PRODUCTS_PENDING:
      console.log("LOGIN  ", action);
      return {
        ...state,
        loading: true,
        message: ""
      };

    case FETCH_PRODUCTS_SUCCSESS:
      console.log("Product FAliure", action.payload.products);
      console.log("Product Success", action);
      return {
        ...state,
        loading: false,
        products: action.payload.products,
        status: null,

      };

    case FETCH_PRODUCTS_FALIURE:
      console.log("Product FAliure", action.payload.error);
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    case ADD_TO_CART:

      let addedItem = state.products.find(item => item._id === action.payload.id)
      let isCartNotEmpty = state.cart && state.cart.length > 0


      let existed_item = isCartNotEmpty ? state.cart.find(item => action.payload.id === item._id) : false;
      if (existed_item) {
        return {
          ...state,
          productCount: state.cart.length
        }
      }






      return {
        ...state,
        cart: isCartNotEmpty ? [...state.cart, addedItem] : [addedItem],
        productCount: isCartNotEmpty ? state.cart.length + 1 : 1
      };
    case REMOVE_FROM_CART:

      let itemToRemove = state.cart.find(item => action.payload.id === item._id)
      let new_items = state.cart.filter(item => action.payload.id !== item._id)
      let isnewCartNotEmpty = new_items && new_items.length > 0



      //calculating the total


      console.log(itemToRemove)
      return {
        ...state,
        cart: new_items,
        productCount: isnewCartNotEmpty ? new_items.length : null

      }

    case INCREASE_QTY:


      var foundIndex = state.products.findIndex(x => x._id == action.payload.id);
      var foundIndexIncreaseCart = state.cart.findIndex(x => x._id == action.payload.id);

      return {
        ...state,
        products: [
          ...state.products.slice(0, foundIndex),
          Object.assign({}, state.products[foundIndex], { quantity: action.payload.qty + 1 }),
          ...state.products.slice(foundIndex + 1)
        ],
        cart: foundIndexIncreaseCart === -1 ? state.cart : [
          ...state.cart.slice(0, foundIndexIncreaseCart),
          Object.assign({}, state.cart[foundIndexIncreaseCart], { quantity: action.payload.qty + 1 }),
          ...state.cart.slice(foundIndexIncreaseCart + 1)
        ]

      }
    case DECREMENT_QTY:

      if (action.payload.qty == 1)
        return { ...state }

      var foundIndexDecrease = state.products.findIndex(x => x._id == action.payload.id);
      var foundIndexDecreaseCart = state.cart.findIndex(x => x._id == action.payload.id);

      return {
        ...state,
        products: [
          ...state.products.slice(0, foundIndexDecrease),
          Object.assign({}, state.products[foundIndexDecrease], { quantity: action.payload.qty - 1 }),
          ...state.products.slice(foundIndexDecrease + 1)
        ],
        cart: foundIndexDecreaseCart === -1 ? state.cart : [
          ...state.cart.slice(0, foundIndexDecreaseCart),
          Object.assign({}, state.cart[foundIndexDecreaseCart], { quantity: action.payload.qty - 1 }),
          ...state.cart.slice(foundIndexDecreaseCart + 1)
        ]

      }

    case PLACE_ORDER_FAILURE:
      return {
        ...state,
        orderMsg: action.payload.message,
        orderLoading: false,
        status: 'FAILURE',
      }


    case PLACE_ORDER_SUCCSESS:
      return {
        ...state,
        productCount: null,
        cart: [],
        orderMsg: action.payload.message,
        orderData: action.payload.data,
        status: 'SUCCESS',
        orderLoading: false

      }


    case PLACE_ORDER_PENDING:

      return {
        ...state,
        orderLoading: true,
        status: null,

      }
    case CLEAR_CART:
      return {
        ...state,
        isFetching: false,
        message: '',
        cart: [],
        productCount: null
      }

    case FETCH_CATEGORY_PENDING:
      return {
        ...state,
        loading: true,
        message: ""
      };


    case FETCH_CATEGORY_SUCCSESS:
      console.log("FETCH_CATEGORY_SUCCSESS", action.payload.categories);

      return {
        ...state,
        loading: false,
        categoryList: [ { _id:'0', name:'ALL'} ,  ...action.payload.categories],
        status: null,

      };


    case FETCH_CATEGORY_FAILURE:
      console.log("FETCH_CATEGORY_FAILURE", action.payload.error);

      return {
        ...state,
        loading: false,
        error: action.payload.error,
        status: null,

      };


      case GET_WALLET_BALANCE_CHECKOUT_PENDING:
      console.log("GET_WALLET_BALANCE_CHECKOUT_PENDING  ", action);
      return {
        ...state,
        loading: true,

      };
  
      
      case    GET_WALLET_BALANCE_CHECKOUT_SUCCESS:
      console.log("GET_WALLET_BALANCE_CHECKOUT_SUCCESS", action);
       
      return {
        ...state,
        loading: false,
        data: action.payload.data,
        amount: action.payload.data.data
      };      

      case     GET_WALLET_BALANCE_CHECKOUT_FAILURE:
      console.log("GET_WALLET_BALANCE_CHECKOUT_FAILURE", action.payload.error);
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        message :action.payload.message
      };
    default:
      console.log("Product Default", action);
      return {
        ...state,
        status: null
      };
  }
};
export const getCartTotal = (state) => {

  let total = 0;
  state.cart.map(item => (total += (parseFloat(item.price) * parseFloat(item.quantity))).toFixed(2))


  return total
}
export const getCartItemQuantity = (state) => {

  let totalQuantity = 0;
  state.cart.map(item => (totalQuantity += parseFloat(item.quantity)))


  return totalQuantity
}


export const getProducts = state => state.products;
export const getProductsPending = state => state.loading;
export const getProductsError = state => state.error;