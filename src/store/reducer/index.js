import { combineReducers } from "redux";
import LoginReducer from "./LoginReducer";
import SignUpReducer from "./SignUpReducer";
import ProductReducer from './ProductReducer'
import PaymentReducer from './PaymentReducer'
import ShippingReducer from './ShippingReducer'
import ContactUsReducer from './ContactUsReducer'
import FAQReducer from './FAQReducer'

import OrderHistoryReducer from './OrderHistoryReducer'
import WalletReducer from './WalletReducer'
export default combineReducers({
  login: LoginReducer,
  products:ProductReducer,
  signup:SignUpReducer,
  payment:PaymentReducer,
  shipping:ShippingReducer,
  contactus:ContactUsReducer,
  ordershistory:OrderHistoryReducer,
  wallet:WalletReducer,
  faq:FAQReducer

});

// state.login
//        -> signup->(data,name,)
//       