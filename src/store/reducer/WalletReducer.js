import  {     GET_WALLET_BALANCE_PENDING,
    GET_WALLET_BALANCE_SUCCESS,
    GET_WALLET_BALANCE_FAILURE ,
    GET_WALLET_TRANSACTION_PENDING,
    GET_WALLET_TRANSACTION_SUCCESS,
    GET_WALLET_TRANSACTION_FAILURE
  
  }  from "../actions/types";


  import { REHYDRATE } from 'redux-persist';
  const initialState = {
  
 
    message:null,
    data: [],
    result: [], 
    error: null,
    loading: false,
    amount:0,
    totalhappycash:'',
    totalrecharge:'',
    transactions:[]
  };
  
  export default (state = initialState, action) => {
    switch (action.type) {

      case REHYDRATE:
      return {...state,
        amount:''
    };


      case GET_WALLET_BALANCE_PENDING:
        console.log("GET_WALLET_BALANCE_PENDING  ", action);
        return {
          ...state,
          loading: true,
 
        };
  
         
      case GET_WALLET_BALANCE_SUCCESS:
        
        console.log("GET_WALLET_BALANCE_SUCCESS", action);
       
        return {
          ...state,
          loading: false,
          data: action.payload.data,
          amount: action.payload.data.data
        };      
  
      case GET_WALLET_BALANCE_FAILURE:
        console.log("GET_WALLET_BALANCE_FAILURE", action.payload.error);
        return {
          ...state,
          loading: false,
          error: action.payload.error,
          message :action.payload.message
        };
     


        
      case GET_WALLET_TRANSACTION_PENDING:
      console.log("GET_WALLET_TRANSACTION_PENDING  ", action);
      return {
        ...state,
        loading: true,

      };

       
    case GET_WALLET_TRANSACTION_SUCCESS:
      
      console.log("GET_WALLET_TRANSACTION_SUCCESS", action.payload.data.data [0].totalHapyCash  );
      let isDataNotEmpty = action.payload.data.data && action.payload.data.data.length  > 0
      console.log("GET_WALLET_TRANSACTION_SUCCESS isDATAEMPTY",isDataNotEmpty );
      return {
        ...state,
        loading: false,
        data: action.payload.data,
 
        totalhappycash:isDataNotEmpty? action.payload.data.data [0].totalHapyCash : '',
        totalrecharge:isDataNotEmpty? action.payload.data.data [0].totalBlc : '',
        transactions: isDataNotEmpty? action.payload.data.data [0].walletData : [],
          
      };      

    case GET_WALLET_TRANSACTION_FAILURE:
      console.log("GET_WALLET_TRANSACTION_FAILURE", action.payload.error);
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        message :action.payload.message
      };
   
  
      default:
        console.log("Wallet Default : ", action);
        return state;
    }
  };
 
