import { REHYDRATE } from 'redux-persist';
import {
  GET_PAYMENT_URL,
  GET_PAYMENT_URL_SUCCESS,
  GET_PAYMENT_URL_FAILURE,
  PAYMENT_TELR_RESPONSE_PENDING,
  PAYMENT_TELR_RESPONSE_FAILURE,
  PAYMENT_TELR_RESPONSE_SUCCESS,
  PLACE_ORDER_PAYMENT_PENDING,
  PLACE_ORDER_PAYMENT_SUCCSESS,
  PLACE_ORDER_PAYMENT_FAILURE,


  RECHARGE_WALLET_PENDING,
  RECHARGE_WALLET_SUCCSESS,
  RECHARGE_WALLET_FAILURE,

  GET_PAYMENT_URL_FROM_API ,        
  GET_PAYMENT_URL_FROM_API_SUCCESS ,
  GET_PAYMENT_URL_FROM_API_FAILURE 
} from "../actions/types";

const initialState = {
  name: "",
  message: "",
  data: [],
  url: null,
  ref: null,
  loading: false,
  telrdata: [],
  telrmessage: null,
  ordermessage: null,
  walletmessage:null
};
const PaymentReducer = (state = initialState, action) => {
  switch (action.type) {

    case REHYDRATE:
      return state;

      case GET_PAYMENT_URL_FROM_API:
      console.log("GET_PAYMENT_URL_FROM_API", action);
      return {
        ...state,
        loading: true,
        message: ""
      };

    case GET_PAYMENT_URL_FROM_API_SUCCESS:
      console.log("GET_PAYMENT_URL_FROM_API_SUCCESS", action);

      return {
        ...state,
        loading: false,
        data: action.payload.data.order,
        url: action.payload.data.url,
        ref: action.payload.data.ref,
      };

    case GET_PAYMENT_URL_FROM_API_FAILURE:
      console.log("GET_PAYMENT_URL_FROM_API_FAILURE", action);

      return {
        ...state,
        loading: false,
        message: action.payload.message
      };






    case GET_PAYMENT_URL:
      console.log("Payment Action Default", action);
      return {
        ...state,
        loading: true,
        message: ""
      };

    case GET_PAYMENT_URL_SUCCESS:
      console.log("Payment Action Success", action);

      return {
        ...state,
        loading: false,
        data: action.payload.data.order,
        url: action.payload.data.order.url,
        ref: action.payload.data.order.ref,
      };

    case GET_PAYMENT_URL_FAILURE:
      console.log("Payment Action Failur", action);

      return {
        ...state,
        loading: false,
        message: action.payload.message
      };

    case PAYMENT_TELR_RESPONSE_PENDING:
      console.log("PAYMENT_TELR_RESPONSE_PENDING", action);

      return {
        ...state,
        loading: true,
        telrmessage: null,

      };



    case PAYMENT_TELR_RESPONSE_SUCCESS:
      console.log("PAYMENT_TELR_RESPONSE_SUCCESS", action);

      return {
        ...state,
        loading: false,
        telrmessage: "success",
        telrdata: action.payload.data,
        message: action.payload.message
      };

    case PAYMENT_TELR_RESPONSE_FAILURE:
      console.log("PAYMENT_TELR_RESPONSE_FAILURE", action);

      return {
        ...state,
        loading: false,
        telrmessage: "failure",
        message: action.payload.message
      };


    case PLACE_ORDER_PAYMENT_PENDING:
      console.log("PLACE_ORDER_PAYMENT_PENDING", action);

      return {
        ...state,
        loading: true,
        ordermessage: null,
      };


    case PLACE_ORDER_PAYMENT_SUCCSESS:
      console.log("PLACE_ORDER_PAYMENT_SUCCSESS", action);

      return {
        ...state,
        loading: false,
        ordermessage: "success",
        message: action.payload.message
      };

    case PLACE_ORDER_PAYMENT_FAILURE:
      console.log("PLACE_ORDER_PAYMENT_FAILURE", action);

      return {
        ...state,
        loading: false,
        ordermessage: "failure",
        message: action.payload.message
      };



    case RECHARGE_WALLET_PENDING:
      console.log("RECHARGE_WALLET_PENDING", action);

      return {
        ...state,
        loading: true,
  
        walletmessage:null
      };


    case RECHARGE_WALLET_SUCCSESS:
      console.log("RECHARGE_WALLET_SUCCSESS", action);

      return {
        ...state,
        loading: false,
        walletmessage: "success",
        message: action.payload.message
      };

    case RECHARGE_WALLET_FAILURE:
      console.log("RECHARGE_WALLET_FAILURE", action);

      return {
        ...state,
        loading: false,
        walletmessage:"failure",
        message: action.payload.message
      };





    default:
      console.log("Default state", action);
      return state;
  }
};

export default PaymentReducer; 