import {TOKEN,TOKEN_FAILURE,TOKEN_SUCCESS, SIGNUP, SIGNUP_SUCCESS, OTP_DEFAULT, 
  SIGNUP_FAILURE, OTP_VERIFICATION_SUCCESS, OTP_VERIFICATION_FAILURE } from "../actions/types";
import { REHYDRATE } from 'redux-persist';

const initialState = {
  data: null,
  loading: false,
  loadingotp: false,
  error: null,
  otpverify: false,
  name: "",
  message: "",
  verificationcode: null,
  tokenstring: "",
  mobsuccsess:false,
  succsess:false

};

export default (state = initialState, action) => {
  switch (action.type) {
    case REHYDRATE:
      return state;
      

    case SIGNUP:
      console.log("SIGNUP", action);
      return {
        ...state,
        loading: true,
        otpverify: false,
        error: null,
        mobsuccsess:false,
        succsess:false
      };

    case SIGNUP_SUCCESS:
      console.log("SIGNUP SUCCESS", action);
      return {
        ...state,
        loading: false,
        message: action.payload.userregister,
        verificationcode: action.payload.userregister.verificationcode,
        error: null,
        succsess:true

      };

    case SIGNUP_FAILURE:
      console.log("SIGNUP FAILURE", action);
      return {
        ...state,
        loading: false,
        message: action.payload.message.message,
        error: true,
        succsess:false
      };

    case OTP_VERIFICATION_SUCCESS:
      console.log("VERIFY SUCCESS", action);
      return {
        ...state,
        otpverify: true
      };

    case OTP_VERIFICATION_FAILURE:
      console.log("VERIFY FAILURE", action);
      return {
        ...state,
        otpverify: false
      };

    case OTP_DEFAULT:
      console.log("Default ", action);
      if (action.payload.manual && action.payload.recived) {

        if (action.payload.manual == action.payload.recived) {
          // console.warn('Match IN OTP_DEFAULT ' + action.payload.manual ,action.payload.recived );
          return {
            ...state,
            otpverify: true
          };
        }
      } else {
        // console.warn('FALSE IN OTP_DEFAULT ' + action.payload.manual ,action.payload.recived );
        return {
          ...state,
          otpverify: false
        };
      }

    case TOKEN:
      console.log("SIGNUP", action);
      return {
        ...state,
        loadingotp: true,
      };

    case TOKEN_SUCCESS:
      console.log("TOKEN SUCCESS", action);

      return {
        ...state,
        loadingotp: false,
        tokenstring: action.payload.savetoken,
        tokensuccess: action.payload.success,
      };

    case TOKEN_FAILURE:
      console.log("TOKEN FAILURE", action);
      return {
        ...state,
        loadingotp: false,
        tokenstring: action.payload.savetoken,
      };

    default:
      console.log("Default state", action);
      return state;
  }
};