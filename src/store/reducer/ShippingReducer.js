import {
  SHIPPING_ADDRESS_FETCH_PENDING, SHIPPING_ADDRESS_FETCH_SUCCSESS, SHIPPING_ADDRESS_FETCH_FAILURE
  , SHIPPING_ADDRESS_SUBMIT_PENDING, SHIPPING_ADDRESS_SUBMIT_SUCCSESS, SHIPPING_ADDRESS_SUBMIT_FAILURE,
  FETCH_CITY_PENDING,
  FETCH_CITY_SUCCSESS,
  FETCH_CITY_FAILURE

} from "../actions/types";
import NavigationService from "../../navigator/NavigationService";

const initialState = {


  message: null,
  data: [],
  result: [],
  error: null,
  loading: false,
  cities: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SHIPPING_ADDRESS_FETCH_PENDING:
      console.log("LOGIN  ", action);
      return {
        ...state,
        loading: false,
        message: ""
      };


    case SHIPPING_ADDRESS_FETCH_SUCCSESS:
      console.log("SHIPPING_ADDRESS_FETCH_SUCCSESS  ", action.payload.data);
      console.log("SHIPPING_ADDRESS_FETCH_SUCCSESS", action);

      return {
        ...state,
        loading: false,
        data: action.payload.data,
      };

    case SHIPPING_ADDRESS_FETCH_FAILURE:
      console.log("Shipping Address Fetch FAliure", action.payload.error);
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        message: action.payload.message
      };
    case SHIPPING_ADDRESS_SUBMIT_PENDING:
      return {
        ...state,
        loading: true,


      };




    case FETCH_CITY_PENDING:
      console.log("LOGIN  ", action);
      return {
        ...state,
        loading: true,
        message: ""
      };


    case FETCH_CITY_SUCCSESS:
      console.log("FETCH_CITY_SUCCSESS  ", action.payload.data);
      console.log("FETCH_CITY_SUCCSESS", action);

      return {
        ...state,
        loading: false,
        cities: action.payload.data,
      };

    case FETCH_CITY_FAILURE:
      console.log("Shipping Address Fetch FAliure", action.payload.error);
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        message: action.payload.message
      };
    case SHIPPING_ADDRESS_SUBMIT_PENDING:
      return {
        ...state,
        loading: true,


      };


    case SHIPPING_ADDRESS_SUBMIT_SUCCSESS:

      return {
        ...state,
        loading: false,
        result: action.payload.data

      };

    case SHIPPING_ADDRESS_SUBMIT_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        message: action.payload.message

      };


    default:
      console.log("Shipping Address Default", action);
      return state;
  }
};
