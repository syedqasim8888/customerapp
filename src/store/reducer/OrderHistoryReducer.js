import  {    ORDER_HISTORY_PENDING , 
    ORDER_HISTORY_SUCCESS,
    ORDER_HISTORY_FAILURE }  from "../actions/types";


    const initialState = {
        message:null,
        data: [],
        result: [], 
        error: null,
        loading: false
      };


      export default (state = initialState, action) => {
        switch (action.type) {
          case ORDER_HISTORY_PENDING:
            console.log("LOGIN  ", action);
            return {
              ...state,
              loading: false,
              message: ""
            };
      
             
          case ORDER_HISTORY_SUCCESS:
            console.log("ORDER_HISTORY_SUCCESS  ", action.payload.data);
            console.log("ORDER_HISTORY_SUCCESS", action);
           
            return {
              ...state,
              loading: false,
              data: action.payload.data,
            };      
      
          case ORDER_HISTORY_FAILURE:
            console.log("ORDER_HISTORY_FAILURE", action.payload.error);
            return {
              ...state,
              loading: false,
              error: action.payload.error,
              message :action.payload.message
            };
      
    
           
          default:
            console.log("Order History  Default", action);
            return state;
        }
      };
     
 