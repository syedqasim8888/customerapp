import {CONTACTUS_SUCCSESS,CONTACTUS_FALIURE, CONTACTUS_PENDING} from "../actions/types";
import { REHYDRATE } from 'redux-persist';

const initialState = {
  name: "",
  message: "",
  email: "",
  pwd: "",
  loading: false,
  contactusdata:[],
  err:false,
  contactussuccess:false

};
export default (state = initialState, action) => {
  switch (action.type) {

    case REHYDRATE:
        return {...state,
          loading: false,
          contactussuccess:false
      };
 

    case CONTACTUS_PENDING:
    console.log("CONTACT", action);
      return {
        ...state,
        loading: true,
        err: false,
        contactussuccess:false
      };  

    case CONTACTUS_SUCCSESS:
    console.log("CONTACT SUCCESS", action);
      return {
        ...state,
        loading: false,
        contactusdata: action.payload.userdata,
        err: false,
        contactussuccess:true

      };

    case CONTACTUS_FALIURE:
      console.log("CONTACT FAILURE", action);
      return {
        ...state,
        loading: false,
        err: true,
        contactussuccess:false
        
      };

      default:
        console.log("Default state", action);
            return {...state,
              loading: false,
              contactusdata: null,
              err: false,
              contactussuccess:false
          };
  }
};