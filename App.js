import React, { Component } from 'react';
import AppNavigation from './src/navigator/AppNavigation';
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import storeConfig from "./src/store";
import NavigationService from "./src/navigator/NavigationService"
import AsyncStorage from '@react-native-community/async-storage';
import {Alert,View,Text
} from 'react-native';

 
import * as RNLocalize from 'react-native-localize';
import { setI18nConfig } from './src/utils/i18n';
import { translate } from './src/utils/i18n';

export default  class App extends  Component {
  
  constructor(props) {
    super(props)
    this.state = {
      islogin: false,
      Navigator:0
    }
    setI18nConfig();
    this.Gettinglogin();
    
  }
  componentWillUnmount() {
    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
  }
  async  componentDidMount() {
    RNLocalize.addEventListener('change', this.handleLocalizationChange);
  }

  Gettinglogin = async () => {

    const a = await AsyncStorage.getItem("islogin");

    if(a==='1'){

      this.setState({
        islogin: true
      })
   

      console.log("islogin: "+this.state.islogin);
    }
    this.setState(  {Navigator:AppNavigation(this.state.islogin?"MainStack":"SignInStack")})
  }
 
  render() {
    let storeDefaults = storeConfig();
    if(this.state.Navigator)
    return (
      <Provider store={storeDefaults.store}>
        <PersistGate loading={null} persistor={storeDefaults.persistor}>
          <this.state.Navigator  
          ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }} />
        </PersistGate>
      </Provider>
    );
    return (<View><Text>Loading</Text></View>)
  }
}

